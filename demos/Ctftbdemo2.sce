//function Ctftbdemo();
//-----------------------------------
// ANSI C Time Frequency Toolbox
//-----------------------------------
//       Scilab demo file
//-----------------------------------
//close all
mode(-1)
show_optimal=%f;
clc;  f=scf(); fig_id=get(gcf(),"figure_id"); clf; 
f.color_map = jetcolormap(64);
mode(-1);lines(0);
disp('---------------------------------------------')
disp(' Welcome to the ANSI C Time frequency toolbox')
disp(' demonstration file')
disp('---------------------------------------------')

mode(-1);
demopath = get_absolute_file_path("Ctftbdemo2.sce");
n = x_choose([' Bat shirp';'Woman speaking the word greasy';...
              'Linus pronouncing Linux.';'Glockenspiel test signal';...
              'Click-evoked otoacoustic emmision.';'a train passing close by with a doppler shift';...
              'Piano';'Clarinette'],'Return');
if n==0;
  n=1;
end

disp('--------------------------------------------')
disp('Load signal and do signal pre prosessing. ')
disp('The signal must be downsampled, otherwise it will not possible to display the spectrum using grayplot. ')
disp('In order to make the signal analytic the signal will be hilbert - transformed.')
disp('---------------------------------------------')
N_kern=1024;
select n
case 1
  mode(1);
  loadmatfile(demopath+"/data/bat.asc");
  sig=hilbert(bat);
  t=1:length(sig);
  N_kern=length(sig);
  show_optimal=%t;
  mode(-1);
case 2
  mode(1);
  sig=wavread(demopath+"/data/greasy.wav");
  sig=intdec(sig,1/4);
  sig=hilbert(sig);
  t=1:length(sig);
  N_kern=1024;
  mode(-1);
case 3
  mode(1);
  sig=wavread(demopath+"/data/linus.wav");
  sig=intdec(sig,1/20);
  sig=hilbert(sig);
  t=1:length(sig);
  N_kern=1024;
  mode(-1);
case 4
  mode(1);
  sig=wavread(demopath+"/data/gspi.wav");
  sig=intdec(sig,1/100);
  sig=hilbert(sig);
  t=1:length(sig);
  mode(-1);
case 5
  mode(1);
  loadmatfile(demopath+"/data/otoclick.asc");
  sig=intdec(otoclick,1/2);
  sig=hilbert(sig);
  t=1:length(sig);
  mode(-1);
case 6
  mode(1);
  sig=wavread(demopath+"/data/traindoppler.wav");
  sig=intdec(sig,1/100);
  sig=hilbert(sig);
  t=1:length(sig);
  mode(-1);
case 7
  mode(1);
  sig=wavread(demopath+"/data/Piano2.wav");
  sig=intdec(sig,1/10);
  sig=hilbert(sig);
  t=1:length(sig);
  mode(-1);
case 8
  mode(1);
  sig=wavread(demopath+"/data/Clar.wav");
  sig=intdec(sig,1/20);
  sig=hilbert(sig);
  t=1:length(sig);
  mode(-1);
end

mode(1);
mode(-1)
halt('Press return to continue');
disp('--------------------------------------------')
disp(' this signal looks like this')
disp('---------------------------------------------')
scf(fig_id);clf(fig_id);
plot(t,real(sig),'b',t,imag(sig),'r');
xlabel('Time (points)')
ylabel('Amplitude')
title('Analyzed signal (blue=real part, red=imag part)')
//a=gca();a.data_bounds=([0 t($) -2.5 2.5]);
a=gca();a.tight_limits ='on';


halt('Press return to continue');
mode(-1)
disp('--------------------------------------------')
disp(' Let''s have a look to a spectrogram')
disp('---------------------------------------------')
[TFR,T,F]=Ctfrsp(sig,t,128,Cwindow(31,'hamming'));
scf(fig_id);clf(fig_id);
grayplot(T,F,TFR');
a=gca();a.tight_limits ='on';
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Spectrogram');


halt('Press return to continue');

mode(-1)
disp('--------------------------------------------')
disp(' What about a  Reassigned spectrogram ?')
disp('---------------------------------------------')
TFR=Ctfrrsp(sig,t,128,Cwindow(31,'hamming'));
scf(fig_id);clf(fig_id);
grayplot(T,F,TFR');
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Reassigned Spectrogram');



halt('Press return to continue');
mode(-1)
scf(fig_id);clf(fig_id);
disp('--------------------------------------------')
disp(' Or a Choi-Williams Representation ?')
disp('--------------------------------------------')
[TFR,T,F]=Ctfrcw(sig,t,128,Cwindow(31,'hamming'));

grayplot(T,F,TFR');
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Choi-Williams representation');



halt('Press return to continue');
mode(-1)
disp('--------------------------------------------')
disp(' We could try a lot more TFR kernels, ')
disp(' such as the following: ')
disp('--------------------------------------------')

scf(fig_id);clf(fig_id);;
[TFR,T,F]=Ctfrbj(sig,t,128);
subplot(2,3,1);
grayplot(T,F,TFR');
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Born-Jordan')

[TFR,T,F]=Ctfrbud(sig,t,128);
subplot(2,3,2);
grayplot(T,F,TFR');
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Butterworth')

[TFR,T,F]=Ctfrgrd(sig,t,128);
subplot(2,3,3);
grayplot(T,F,TFR');
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Generalized rectangular')

[TFR,T,F]=Ctfrmh(sig,t,128);
subplot(2,3,4);
grayplot(T,F,TFR');
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title(' Margenau-Hill')

[TFR,T,F]=Ctfrwv(sig,t,128);
subplot(2,3,5);
grayplot(T,F,TFR');
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Wigner-Ville')

[TFR,T,F]=Ctfrpwv(sig,t,128);
subplot(2,3,6);
grayplot(T,F,TFR');
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Pseudo Wigner-Ville')
mode(1)

halt('Press return to continue');

mode(-1)
if show_optimal then
  mode(1)
  disp(' Now, why not try do adapt the TFR kernel')
  disp(' to the signals. This is done in 4 steps')
  disp('--------------------------------------------')
  disp(' 1 - Compute the ambiguity function of the signal')
  disp('--------------------------------------------')
  scf(fig_id);clf(fig_id);
  [AF,TAU,XI]=Cambifunb(sig,-N_kern/2+1:N_kern/2,N_kern);
  //imagesc(TAU,XI,abs(AF));
  grayplot(TAU,XI,abs(AF)');
  //axis xy
  a=gca();a.tight_limits ='on';
  xlabel('Time lag (points)');
  ylabel('Normalized frequency lag');
  title('Ambiguity function (module)')


  halt('Press return to continue');
  scf(fig_id);clf(fig_id);
  disp('--------------------------------------------')
  disp(' 2 - Choose a kernel shape among the possible')
  disp(' choices (see Ctfrker). We choose a Generalized')
  disp(' Marginals Choi-Williams kernel with one branch')
  disp('--------------------------------------------')

  Kernel=Ctfrker(N_kern,N_kern,'gmcwk',[1 0.2]);
  //imagesc(TAU,XI,kernel);
  grayplot(TAU,XI,Kernel');
  //axis xy
  a=gca();a.tight_limits ='on';
  xlabel('Time lag (points)');
  ylabel('Normalized frequency lag');
  title('Initial one branch kernel')


  halt('Press return to continue');
  scf(fig_id);clf(fig_id);
  disp('---------------------------------------------')
  disp(' 3 - Optimize the parameters of this kernel')
  disp(' to fit the analyzed signal')
  disp('---------------------------------------------')
  d_min=1e100;
  sigma=0.1;
  theta_min=0;
  for theta=linspace(0,2*%pi,30);
    Kernel=Ctfrker(N_kern,N_kern,'gmcwk',[sigma theta]);
    d=Ctfrdist(abs(AF),Kernel,'kullback');
    if (d<d_min),
      d_min=d;
      theta_min=theta;
    end
  end

  disp('---------------------------------------------')
  disp(' 4 - Display the optimal kernel')
  disp('---------------------------------------------')
  Kernel=Ctfrker(N_kern,N_kern,'gmcwk',[sigma theta_min]);
  //imagesc(TAU,XI,kernel);
  grayplot(TAU,XI,Kernel');
  //axis xy
  a=gca();a.tight_limits ='on';
  xlabel('Time lag (points)');
  ylabel('Normalized frequency lag');
  title('One branch kernel')


  halt('Press return to continue');
  scf(fig_id);clf(fig_id);
  disp('---------------------------------------------')
  disp(' 4 - here is the optimal TFR for this signal')
  disp('---------------------------------------------')

  TFR=Caf2tfr(AF,Kernel);
  //imagesc(t,linspace(0,0.5,128),TFR);
  grayplot(1:N_kern,linspace(0,0.5,N_kern),TFR');
  //axis xy
  a=gca();a.tight_limits ='on';
  xlabel('Time(points)');
  ylabel('Normalized frequency');
  title('Optimal One branch kernel TFR')

  disp('---------------------------------------------')
  disp('Kernel optimization is simple to implement !!')
  disp('---------------------------------------------')
end;
