<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->

<refentry xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook"
          xml:lang="en"
          xml:id="ambifunb">


  <refnamediv>
    <refname>ambifunb</refname><refpurpose>Narrow-band ambiguity function.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [NAF,TAU,XI]=ambifunb(X)
      [NAF,TAU,XI]=ambifunb(X,TAU)
      [NAF,TAU,XI]=ambifunb(X,TAU,N)
      [NAF,TAU,XI]=ambifunb(X,TAU,N,TRACE)
      [NAF,TAU,XI]=ambifunb(...,'plot')
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>

      <varlistentry><term>X:</term>
      <listitem>
        <para>a real or complex Nx elements real (auto-Untenberger) or
        a Nx by 2 array signal (cross-Untenberger).</para>
      </listitem>
      </varlistentry>

      <varlistentry><term>TAU   :</term>
      <listitem>
        <para> real vector of lag values     (default is -Nx/2:Nx/2).</para>
      </listitem>
      </varlistentry>

      <varlistentry>
        <term>N     :</term>
        <listitem>
          <para>a positive integer: the number of frequency bins (default is Nx).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>TRACE :</term>
        <listitem>
           <para>A boolean (or a real scalar) if true (or nonzero),the
          progression of the algorithm is shown (default : %f).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>'plot':</term>
        <listitem>
          <para>if input contains the string 'plot', the output values will be plotted</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>NAF   :</term>
        <listitem>
          <para> doppler-lag representation, with the doppler bins
          stored in the rows and the time-lags stored in the
          columns. When called without output arguments, ambifunb
          displays the squared modulus of the ambiguity function by
          means of contour.</para>
        </listitem>
      </varlistentry>
      <varlistentry><term>XI:</term>
      <listitem>
        <para> row vector of doppler values.</para>
      </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      ambifunb computes the narrow-band ambiguity function of a signal
      X, or the cross-ambiguity function between two signals.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    sig=anabpsk(256,8);
    ambifunb(sig,'plot');
    ]]></programlisting>

    <scilab:image><![CDATA[
    sig=anabpsk(256,8);
    ambifunb(sig,'plot');
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="ambifuwb">ambifuwb</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>O. Lemoine</member>
      <member>F. Auger - August 1995.</member>
    </simplelist>
  </refsection>
</refentry>
