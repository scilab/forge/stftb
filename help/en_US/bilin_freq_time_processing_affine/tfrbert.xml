<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1995 Rice University - CNRS (France).
-->

<refentry xml:id="tfrbert" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>tfrbert</refname><refpurpose>Unitary Bertrand time-frequency distribution.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [TFR,T,F]=tfrbert(X)
      [TFR,T,F]=tfrbert(X,T)
      [TFR,T,F]=tfrbert(X,T,FMIN,FMAX)
      [TFR,T,F]=tfrbert(X,T,FMIN,FMAX,N)
      [TFR,T,F]=tfrbert(X,T,FMIN,FMAX,N,TRACE)
      [TFR,T,F]=tfrbert(...,'plot')
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term> X :</term> 
        <listitem>
          <para>It can be:</para>
          <para>a vector of size Nx: the signal (in time)to be analyzed .</para>
          <para>or a 2 by Nx matrix  for the cross-unitary Bertrand distribution)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> T :</term> 
        <listitem>
          <para>a real vector: the time instant(s) on which the TFR is
          evaluated (default : 1:Nx).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> FMIN,FMAX :</term> 
        <listitem>
          <para>
            respectively lower and upper frequency bounds of the analyzed
            signal. These parameters fix the equivalent frequency bandwidth
            (expressed in Hz). When unspecified, you have to enter them at the
            command line from the plot of the spectrum. FMIN and FMAX must be
          &gt;0 and &lt;=0.5.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> N :</term> 
        <listitem>
          <para> number of analyzed
          voices (default : automatically
          determined).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> TRACE :</term> 
        <listitem>
          <para> if nonzero,
          the progression of the algorithm is shown (default :
          0).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>'plot':</term> 
        <listitem>
          <para> if one input
          parameter is 'plot', tfrbert runs tfrqview. and TFR will be
          plotted</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> TFR :</term>
        <listitem>
          <para> time-frequency
          matrix containing the coefficients of the distribution
          (x-coordinate corresponds to uniformly sampled time, and
          y-coordinate corresponds to a geometrically sampled
          frequency). First row of TFR corresponds to the lowest
          frequency.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> F :</term>
        <listitem>
          <para> vector of
          normalized frequencies (geometrically sampled from FMIN to
          FMAX).</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      tfrbert generates the auto- or cross- unitary Bertrand distribution.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <para> Interactive use </para>
    <programlisting role="example"><![CDATA[
    sig=altes(64,0.1,0.45); tfrbert(sig,'plot');
    ]]></programlisting>
    <para>Non interactive use</para>

    <programlisting role="example"><![CDATA[
    N=128;
    sig=altes(N,0.1,0.45); 
    T=1:N;
    [tfr,t,f]=tfrbert(sig,T,0.1,0.35,64);
    clf;gcf().color_map= jetcolormap(128);
    grayplot(t,f,tfr');
    ]]></programlisting>
    <scilab:image><![CDATA[
    N=128;
    sig=altes(N,0.1,0.45); 
    T=1:N;
    [tfr,t,f]=tfrbert(sig,T,0.1,0.35,64);
    clf;gcf().color_map= jetcolormap(128);
    grayplot(t,f,tfr');
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="tfrscalo">tfrscalo</link></member>
      <member><link linkend="tfrunter">tfrunter</link></member>
      <member><link linkend="tfrdfla">tfrdfla</link></member>
      <member><link linkend="tfrspaw">tfrspaw</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010	</member>
      <member>	P. Goncalves, October 95 - O. Lemoine, June 1996.</member>
    </simplelist>
  </refsection>
</refentry>
