<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xml:id="tfrspaw" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>tfrspaw</refname><refpurpose>Smoothed Pseudo Affine
    Wigner time-frequency distributions.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [TFR,T,F]=tfrspaw(X)
      [TFR,T,F]=tfrspaw(X,T)
      [TFR,T,F]=tfrspaw(X,T,K)
      [TFR,T,F]=tfrspaw(X,T,K,NH0)
      [TFR,T,F]=tfrspaw(X,T,K,NH0,NG0)
      [TFR,T,F]=tfrspaw(X,T,K,NH0,NG0,FMIN,FMAX)
      [TFR,T,F]=tfrspaw(X,T,K,NH0,NG0,FMIN,FMAX,N)
      [TFR,T,F]=tfrspaw(X,T,K,NH0,NG0,FMIN,FMAX,N,TRACE)
      [TFR,T,F]=tfrspaw(...,'plot')
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>X :</term>
        <listitem>
          <para> A Nx elements vector (signal) or a Nx by 2 array signal
          (cross-Smoothed Pseudo Affine Wigner distribution.).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> T:</term> 
        <listitem>
          <para> a real Nt vector with elements in [1 Nx] : time
          instant(s) on which the TFR is evaluated (default:
          1:NX).</para>
        </listitem>
      </varlistentry>
      
      
      <varlistentry>
        <term> K :</term> 
        <listitem>
          <para> label of the K-Bertrand distribution. The
          distribution with parameterization function
          <literal>lambdak(u,K) =
          (K(exp(-u)-1)/(exp(-Ku)-1))^(1/(K-1))</literal> is computed
          (default is 0).</para>
          <itemizedlist>
            <listitem>
              <para>K=-1: Smoothed pseudo (active) Unterberger
              distribution</para>
            </listitem>
            <listitem>
              <para>K=0: Smoothed pseudo Bertrand distribution</para>
            </listitem>
            <listitem>
              <para>K=1/2: Smoothed pseudo D-Flandrin
              distribution</para>
            </listitem>
            <listitem>
              <para> K=2: Affine smoothed pseudo Wigner-Ville
              distribution.</para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
      
      <varlistentry>
        <term> NH0 :</term> 
        <listitem>
          <para> half length of
          the analyzing wavelet at coarsest scale.  A Morlet wavelet is
          used. NH0 controles the frequency smoothing of the smoothed pseudo
          Affine Wigner distribution. (default is
          sqrt(Nx)).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> NG0 :</term> 
        <listitem>
          <para> half length of the time smoothing window. If NG0 is
          set to zero the time smoothing window is a rectangular one
          and the length is automatically determined.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> FMIN:</term> 
        <listitem>
          <para>
            a positive scalar in ]0 0.5], the normalized lower
            frequency bound in (Hz) of the analyzed signal. When
            unspecified, you have to enter it at the command line from
          the plot of the spectrum. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> FMAX :</term> 
        <listitem>
          <para> a positive scalar in ]0 0.5], the normalized upper
          frequency bound (in Hz) of the analyzed signal.  When
          unspecified, you have to enter it at the command line from
          the plot of the spectrum.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>N :</term> 
        <listitem>
          <para>positive integer: number of analyzed voices.  When
          unspecified, you have to enter it at the command line from
          the plot of the spectrum.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> TRACE :</term> 
        <listitem>
          <para>A boolean (or a real scalar) if true (or nonzero),the
          progression of the algorithm is shown (default : %f).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>'plot':</term> 
        <listitem>
          <para> if one input parameter is 'plot', <link
          linkend="tfrqview">tfrqview</link> is called and the
          time-frequency representation will be plotted.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> TFR :</term> 
        <listitem>
          <para> N by Nt real array: the time-frequency
          matrix containing the coefficients of the decomposition (abscissa
          correspond to uniformly sampled time, and ordinates correspond to a
          geometrically sampled frequency). First row of TFR corresponds to
          the lowest frequency.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> F :</term> 
        <listitem>
          <para> vector of
          normalized frequencies (geometrically sampled from FMIN to
          FMAX).
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      tfrspaw generates the auto- or cross- Smoothed Pseudo Affine
      Wigner distributions.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <para> Interactive use </para>
    <programlisting role="example"><![CDATA[
    N=128;
    sig=altes(N,0.1,0.45); 
    tfrspaw(sig,"plot");
    ]]></programlisting>

    <para>Non interactive use</para>

    <programlisting role="example"><![CDATA[
    N=128;
    sig=altes(N,0.1,0.45); 
    T=1:N;
    K=0;

    [tfr,t,f]=tfrspaw(sig,1:N,0,2*sqrt(N),0,0.1,0.35,32);
    clf;gcf().color_map= jetcolormap(128);
    grayplot(t,f,tfr');
    ]]></programlisting>
    <scilab:image><![CDATA[
    N=128;
    sig=altes(N,0.1,0.45); 
    [tfr,t,f]=tfrspaw(sig,1:N,0,2*sqrt(N),0,0.1,0.35,32);
    clf;gcf().color_map= jetcolormap(128);
    grayplot(t,f,tfr');
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	P. Goncalves, October 95</member>
      <member>O. Lemoine, June 1996.</member>
      <member>	Copyright (c) 1995 Rice University - CNRS (France) 1996.</member>
    </simplelist>
  </refsection>
</refentry>
