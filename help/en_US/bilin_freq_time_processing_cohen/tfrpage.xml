<?xml version="1.0" encoding="UTF-8"?>

<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France)
-->


<refentry xml:id="tfrpage" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>tfrpage</refname><refpurpose>Page time-frequency distribution.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [TFR,T,F]=tfrpage(X)
      [TFR,T,F]=tfrpage(X,T)
      [TFR,T,F]=tfrpage(X,T,N)
      [TFR,T,F]=tfrpage(X,T,N,TRACE)
      [TFR,T,F]=tfrpage(...,'plot')
    </synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>X :</term>
        <listitem>
          <para> A Nx elements vector (auto-PAGE)  or a Nx by 2 array signal (cross-PAGE).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> T:</term> 
        <listitem>
          <para>a real Nt vector with elements in [1 Nx] : time instant(s)
          (default: 1:NX).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> N:</term> 
        <listitem><para> a positive integer: the number of frequency bins
        (default:NX). For faster computation N should be a power of
        2.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> TRACE :</term> 
        <listitem>
          <para>A boolean (or a real scalar) if true (or nonzero),the
          progression of the algorithm is shown (default : %f).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>'plot':</term> 
        <listitem>
          <para> if one input parameter is
        'plot', <link linkend="tfrqview">tfrqview</link> is called and
        the time-frequency representation will be plotted.</para>
      </listitem>
      </varlistentry>
      <varlistentry>
        <term> TFR :</term> 
        <listitem>
          <para> A real N by Nt array: the time-frequency representation.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> F :</term> 
        <listitem>
          <para> A N vector of normalized frequencies.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      tfrpage computes the Page distribution of a discrete-time signal X, or
      the cross Page representation between two signals.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <para> Interactive use </para>
    <programlisting role="example"><![CDATA[
    N=128;
    sig=fmlin(N,0.1,0.4); 
    tfrpage(sig,'plot');
    ]]></programlisting>
    <para>Non interactive use</para>

    <programlisting role="example"><![CDATA[
    N=128;
    sig=fmlin(N,0.1,0.4); 
    [TFR,T,F]=tfrpage(sig);
    clf;gcf().color_map= jetcolormap(128);
    grayplot(T,F(1:N/2),TFR(1:N/2,:)');
    ]]></programlisting>

    <scilab:image><![CDATA[
    N=128;
    sig=fmlin(N,0.1,0.4); 
    [TFR,T,F]=tfrpage(sig);
    clf;gcf().color_map= jetcolormap(128);
    grayplot(T,F(1:N/2),TFR(1:N/2,:)');
    ]]></scilab:image>

  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	F. Auger, May-August 1994, July 1995.</member>

    </simplelist>
  </refsection>
</refentry>
