<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->

<refentry xml:id="parafrep" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>parafrep</refname><refpurpose>parametric frequency representation of a signal.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [spec,freqs]=parafrep(Rx)
      [spec,freqs]=parafrep(Rx,N)
      [spec,freqs]=parafrep(Rx,N,method)
      [spec,freqs]=parafrep(...,'plot')
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>Rx     :</term>
        <listitem>
          <para>A  (p+1) by (p+1) array of double: the correlation matrix.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>N      :</term>
        <listitem>
          <para>a positive integer: the number of frequency bins.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>method :</term>
        <listitem>
          <para>a character string with possible values: 'AR', 'PERIODOGRAM', 'CAPON', 'CAPNORM', 'LAGUNAS', or 'GENLAG'.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>'plot':</term> 
        <listitem><para> if one input parameter is 'plot', the
        frequency representation will be plotted.</para>
      </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    noise=rand(1000,1); signal=filter([1 0 0],[1 1 1],noise);
    clf;
    subplot(221);parafrep(correlmx(signal,2,'hermitian'),128,'AR',"plot");title('AR (2)');
    subplot(222);parafrep(correlmx(signal,4,'hermitian'),128,'Capon',"plot");title('Capon (4)');
    subplot(223);parafrep(correlmx(signal,2,'hermitian'),128,'lagunas',"plot");title('Lagunas (2)');
    subplot(224);parafrep(correlmx(signal,40,'hermitian'),128,'periodogram',"plot");title('periodogram (40)');
    ]]></programlisting>

    <scilab:image><![CDATA[
    noise=rand(1000,1); signal=filter([1 0 0],[1 1 1],noise);
    clf;
    subplot(221);parafrep(correlmx(signal,2,'hermitian'),128,'AR',"plot");title('AR (2)');
    subplot(222);parafrep(correlmx(signal,4,'hermitian'),128,'Capon',"plot");title('Capon (4)');
    subplot(223);parafrep(correlmx(signal,2,'hermitian'),128,'lagunas',"plot");title('Lagunas (2)');
    subplot(224);parafrep(correlmx(signal,40,'hermitian'),128,'periodogram',"plot");title('periodogram (40)');

    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>F. Auger, july 1998, april 99.</member>
    </simplelist>
  </refsection>
</refentry>
