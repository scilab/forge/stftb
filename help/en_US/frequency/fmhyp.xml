<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1995 Rice University
-->


<refentry xml:id="fmhyp" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>fmhyp</refname><refpurpose>Signal with hyperbolic
    frequency modulation.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [X,IFLAW]=fmhyp(N,P1,P2)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>N  :</term>
        <listitem>
          <para>a positive integer:  number of points in time</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>P1 :</term>
        <listitem>
          <para>a real 2 elements vector:</para>
          <para> if the number of input arguments is 2, P1 is a vector
          containing the two coefficients [F0 C] for an hyperbolic
          instantaneous frequency .</para>

          <para> if the number of input arguments is 3, P1 is a
          time-frequency point of the form [t,f]. where t is in
          seconds and f is a normalized frequency (between 0 and
          0.5). </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>P2 :</term>
        <listitem>
          <para>a real 2 elements vector:</para>
          <para>P2 is a time-frequency point of the form [t,f]. where
          t is in seconds and f is a normalized frequency (between 0
          and 0.5). </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>X  :</term>
        <listitem>
          <para> real column vector: the modulated signal time
          samples. Sampling frequency is set to 1.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>IFLAW :</term>
        <listitem>
          <para>real column vector: instantaneous frequency law</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      fmhyp generates a signal with hyperbolic frequency modulation :
      X(t) = exp(i.2.pi(F0.t + C/log|t|)).  When called with 3 input
      arguments F0 and C are derived from P1 and P2 such that the
      frequency modulation law fits the points P1 and P2.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    [X,IFLAW]=fmhyp(128,[1 .5],[32 0.1]);
    clf;
    subplot(211); plot(real(X));xtitle(_("Signal real part"))
    subplot(212); plot(IFLAW);xtitle(_("Instantaneous frequency law"))
    ]]></programlisting>
    <scilab:image><![CDATA[
    [X,IFLAW]=fmhyp(128,[1 .5],[32 0.1]);
    clf;
    subplot(211); plot(real(X));xtitle(_("Signal real part"))
    subplot(212); plot(IFLAW);xtitle(_("Instantaneous frequency law"))
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="fmlin">fmlin</link></member>
      <member><link linkend="fmsin">fmsin</link></member>
      <member><link linkend="fmpar">fmpar</link></member>
      <member><link linkend="fmconst">fmconst</link></member>
      <member><link linkend="fmodany">fmodany</link></member>
      <member><link linkend="fmpower">fmpower</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>P. Goncalves - October 1995, O. Lemoine - November 1995</member>
    </simplelist>
  </refsection>
</refentry>
