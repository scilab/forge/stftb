<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1995 Rice University - CNRS (France) 1996.
-->


<refentry version="5.0-subset Scilab" xml:id="renyi" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  

  <refnamediv>
    <refname>renyi</refname><refpurpose>Measure Renyi information.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      R=renyi(TFR)
      R=renyi(TFR,T)
      R=renyi(TFR,T,F)
      R=renyi(TFR,T,F,ALPHA)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>	TFR :</term>
        <listitem>
          <para> M by N array: the 2-D density function (or mass
          function). Eventually TFR can be a time-frequency
          representation, in which case its first row must correspond
          to the lower frequencies.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	T :</term>
        <listitem>
          <para> a real vector of size N: the abscissa vector parametrizing the TFR matrix. T can be a    non-uniform sampled vector (eventually a time vector)(default : (1:N)).	</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	F :</term>
        <listitem>
          <para>a real vector of size M: the ordinate vector parametrizing the TFR matrix. F can be a    non-uniform sampled vector (eventually a frequency vector)	(default : (1:M)).	</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	ALPHA :</term>
        <listitem>
          <para>a positive scalar: the rank of the Renyi measure (default : 3).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	R :</term>
        <listitem>
          <para> the alpha-rank Renyi measure (in bits if TFR is a time-frequency matrix) :   R=log2[Sum[TFR(Fi,Ti)^ALPHA dFi.dTi]/(1-ALPHA)]</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      renyi measures the Renyi information relative
      to a 2-D density function TFR (which can be eventually a TF
      representation).
    </para>
    <para>The Renyi entropy furnishes measures for estimating signal
    information and complexity in the time–frequency plane. When
    applied to a TFR from the Cohen’s or the affine classes, the Renyi
    entropies conform to the notion of complexity that we use
    when inspecting time–frequency images. </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    s=atoms(64,[32,.3,16,1]); [TFR,T,F]=tfrsp(s); R=renyi(TFR,T,F,3)
    
    s=atoms(64,[16,.2,10,1;40,.4,12,1]); [TFR,T,F]=tfrsp(s);
    R=renyi(TFR,T,F,3)
    ]]></programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	P. Goncalves, October 95</member>
    </simplelist>
  </refsection>
</refentry>
