<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->


<refentry xml:id="tfrrgab" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>tfrrgab</refname><refpurpose>Reassigned Gabor spectrogram time-frequency distribution.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [TFR,RTFR,HAT] = tfrrgab(X)
      [TFR,RTFR,HAT] = tfrrgab(X,T)
      [TFR,RTFR,HAT] = tfrrgab(X,T,N)
      [TFR,RTFR,HAT] = tfrrgab(X,T,N,NH)
      [TFR,RTFR,HAT] = tfrrgab(X,T,N,NH,TRACE)
      [TFR,RTFR,HAT] = tfrrgab(X,T,N,NH,TRACE,K)
      [TFR,RTFR,HAT] = tfrrgab(...,'plot')
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>X :</term>
        <listitem>
          <para> A Nx elements vector: the signal.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> T:</term> 
        <listitem>
          <para> a real Nt vector with elements in [1 Nx] : time instant(s)
          (default: 1:NX).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> N:</term> 
        <listitem><para> a positive integer: the number of frequency bins
        (default:NX). For faster computation N should be a power of
        2.</para>
        </listitem>
      </varlistentry>


      <varlistentry>
        <term>	NH    :</term>
        <listitem>
          <para> an odd  positive integer: the length of the gaussian window (default : the first odd number greater than N/4))</para>
        </listitem>
      </varlistentry>
      <varlistentry>

        <term>	TRACE :</term>
        <listitem>
          <para>A boolean (or a real scalar) if true (or nonzero),the
          progression of the algorithm is shown (default : %f).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	K     :</term>
        <listitem>
          <para>a real sclar: the value at both extremities (default 0.001)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>'plot':</term>
        
        <listitem>
          <para> if last input parameter value is 'plot', <link
          linkend="tfrqview">tfrqview</link> is called and the
          time-frequency representation will be plotted.</para>
        </listitem>         
      </varlistentry>
      <varlistentry>
        <term>	TFR  :</term>
        <listitem>
          <para>A real N by Nt array: the time-frequency representation.</para> 
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	RTFR  :</term>
        <listitem>
          <para>A real N by Nt array: the reassigned time-frequency representation.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	HAT   :</term>
        <listitem>
        <para>>A complex N by Nt array: the reassignment vectors.</para></listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      tfrrgab computes the Gabor spectrogram and its reassigned version.
      This particular window (a Gaussian window) allows a 20 % faster
      algorithm than the tfrrsp function.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    N=128;
    sig=fmlin(N,0.1,0.4);
    t=1:128;
    Nh=19;
    [tfr,rtfr,hat]=tfrrgab(sig,t,N,Nh);
    clf;gcf().color_map= jetcolormap(128);
    subplot(121)
    grayplot(t,linspace(0,0.25,N/2),tfr(1:N/2,:)')
    title("TFR")
    subplot(122)
    grayplot(t,linspace(0,0.25,N/2),tfr(1:N/2,:)')
    title("RTFR")
    
    ]]></programlisting>
    <scilab:image><![CDATA[
    N=128;
    sig=fmlin(N,0.1,0.4);
    t=1:128;
    Nh=19;
    [tfr,rtfr,hat]=tfrrgab(sig,t,N,Nh);
    clf;gcf().color_map= jetcolormap(128);
    subplot(121)
    grayplot(t,linspace(0,0.25,N/2),tfr(1:N/2,:)')
    title("TFR")
    subplot(122)
    grayplot(t,linspace(0,0.25,N/2),tfr(1:N/2,:)')
    title("RTFR") 
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	F. Auger, May-July 1994, July 1995.</member>
    </simplelist>
  </refsection>
</refentry>
