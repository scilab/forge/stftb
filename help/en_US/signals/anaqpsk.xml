<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->

<refentry xml:id="anaqpsk" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>anaqpsk</refname><refpurpose>Quaternary Phase Shift
    Keying (QPSK) signal.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [Y,PM]=anaqpsk(N)
      [Y,PM]=anaqpsk(N,NCOMP)
      [Y,PM]=anaqpsk(N,NCOMP,F0)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    
    <variablelist>
      <varlistentry>
        <term>	N: </term>
        <listitem>
          <para> a positive integer: the signal length.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	NCOMP :</term>
        <listitem>
          <para> number of points of each component (default: N/5)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>F0:</term>
        <listitem>
          <para>a real scalar in [0 0.5]: then normalized frequency
          (default: 0.25)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	Y:</term>
        <listitem>
          <para>a complex column vector of length N: the signal</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>	PM0   :</term>
        <listitem>
          <para> initial phase of each component	   (optional).</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      anaqpsk returns a complex phase modulated signal
      of normalized frequency F0, whose phase changes every NCOMP point according
      to a discrete uniform law, between the values (0, %pi/2, pi, 3*%pi/2).
      Such signal is only 'quasi'-analytic.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    rand("seed",0);
    [signal,pm0]=anaqpsk(512,64,0.05);
    clf; 
    subplot(211); plot(real(signal)); 
    subplot(212); plot(pm0);gca().data_bounds(1,2)=-0.1;
    ]]></programlisting>
    <scilab:image><![CDATA[
    rand("seed",0);
    [signal,pm0]=anaqpsk(512,64,0.05);
    clf; 
    subplot(211); plot(real(signal));xtitle(_("Signal real part"));
    subplot(212); plot(pm0);gca().data_bounds(:,2)=[-1.1 1.1];
    xtitle(_("Initial phases"));
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="anafsk">anafsk</link></member>
      <member><link linkend="anabpsk">anabpsk</link></member>
      <member><link linkend="anaask">anaask</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	O. Lemoine - October 1995</member>
      <member>	Copyright (c) 1996 by CNRS (France).</member>
    </simplelist>
  </refsection>
</refentry>
