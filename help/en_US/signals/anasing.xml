<?xml version="1.0" encoding="UTF-8"?>

<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->

<refentry xml:id="anasing" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>anasing</refname><refpurpose>Lipschitz singularity.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      X=anasing(N)
      X=anasing(N,T0)
      X=anasing(N,T0,H)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>	N  :</term>
        <listitem>
          <para>a positive integer: the number of points in time</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	T0 :</term>
        <listitem>
          <para>an integer value in ]0 N] : the index of time
          localization of the singularity (default :
          round(N/2)).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	H  :</term>
        <listitem>
          <para>a real scalar: the strenght of the Lipschitz
          singularity (positive or negative) (default : 0.0)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	X  :</term>
        <listitem>
          <para>a complex row vector:  the signal samples</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      anasing generates the N-points Lipschitz singularity
      centered around T=T0 : X(T) = |T-T0|^H.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    x=anasing(128); 
    clf(); plot(real(x));
    ]]></programlisting>
    <scilab:image><![CDATA[
    x=anasing(128); clf(); plot(real(x));
    ]]></scilab:image>
 
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="anastep">anastep</link></member>
      <member><link linkend="anapulse">anapulse</link></member>
      <member><link linkend="anabpsk">anabpsk</link></member>
      <member><link linkend="doppler">doppler</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	P. Goncalves - September 1995</member>
      <member>	Copyright (c) 1995 Rice University</member>
    </simplelist>
  </refsection>
</refentry>
