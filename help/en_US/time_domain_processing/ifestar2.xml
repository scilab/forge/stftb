<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->



<refentry xml:id="ifestar2" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>ifestar2</refname><refpurpose>Instantaneous frequency estimation using AR2 modelisation.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [FNORM,T2,RATIO]=ifestar2(X,T)
      [FNORM,T2,RATIO]=ifestar2(X)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>	X     :</term>
        <listitem>
          <para>A real vector of size N: the signal to be analyzed.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	T     :</term>
        <listitem>
          <para>A vector with integer elements >= 4: Time instants (default : 4:N).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	FNORM :</term>
        <listitem>
          <para>a real column vector: the (normalized) instantaneous frequency.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	T2    :</term>
        <listitem>
          <para>a real column vector: the time instants coresponding
          to FNORM. Since the algorithm can not always give a value,
          T2 may be different of T.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>RATIO :</term>
        <listitem>
          <para>a scalar in [0 1]: the proportion of instants where the algorithm yields an estimation</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      ifestar2 computes an estimate of the
      instantaneous frequency of the real signal X at time
      instant(s) T. The result FNORM lies between 0.0 and 0.5. This
      estimate is based only on the 4 last signal points, and has
      therefore an approximate delay of 2.5 points.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <para>First example</para>
    <programlisting role="example"><![CDATA[
    [x,if1]=fmlin(50,0.05,0.3,5); 
    [if2,t]=ifestar2(real(x));
    clf;plot(t,if1(t),t,if2);
    ]]></programlisting>
    <scilab:image><![CDATA[
    [x,if1]=fmlin(50,0.05,0.3,5); 
    [if2,t]=ifestar2(real(x));
    clf;plot(t,if1(t),t,if2);
    ]]></scilab:image>
    <para>Second example</para>
    <programlisting role="example"><![CDATA[
    N=1100; 
    [deter,if1]=fmconst(N,0.05); 
    deter=real(deter);
    noise=rand(N,1,'normal'); NbSNR=101; SNR=linspace(0,100,NbSNR)';
    for iSNR=1:NbSNR,
    sig=sigmerge(deter,noise,SNR(iSNR));
    [if2,t,ratio(iSNR)]=ifestar2(sig);
    EQM(iSNR,1)=norm(if1(t)-if2)^2 / length(t) ;
    end;

    clf(); 
    subplot(211); plot(SNR,EQM); gca().log_flags="nl";xgrid;
    xlabel('SNR'); ylabel('EQM');
    subplot(212); plot(SNR,ratio); xgrid;
    xlabel('SNR'); ylabel('ratio');
    ]]></programlisting>
    <scilab:image><![CDATA[
   
    ]]></scilab:image>
     N=1100; 
    [deter,if1]=fmconst(N,0.05); 
    deter=real(deter);
    noise=rand(N,1,'normal'); NbSNR=101; SNR=linspace(0,100,NbSNR)';
    for iSNR=1:NbSNR,
    sig=sigmerge(deter,noise,SNR(iSNR));
    [if2,t,ratio(iSNR)]=ifestar2(sig);
    EQM(iSNR,1)=norm(if1(t)-if2)^2 / length(t) ;
    end;

    clf(); 
    subplot(211); plot(SNR,EQM); gca().log_flags="nl";xgrid;
    xlabel('SNR'); ylabel('EQM');
    subplot(212); plot(SNR,ratio); xgrid;
    xlabel('SNR'); ylabel('ratio');
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="instfreq">instfreq</link></member>
      <member><link linkend="kaytth">kaytth</link></member>
      <member><link linkend="sgrpdlay">sgrpdlay</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	F. Auger, April 1996.</member>
    </simplelist>
  </refsection>
</refentry>
