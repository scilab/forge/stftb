function [x,t]=altes(N,fmin,fmax,alpha);
// Altes signal in time domain.
// Calling Sequence
//  x=altes(N)
//  x=altes(N,fmin)
//  x=altes(N,fmin,fmax)
//  x=altes(N,fmin,fmax,alpha)
// Parameters
//  N     : number of points in time   
//  fmin  : lower frequency bound (value of the hyperbolic instantaneous frequency law at the sample N), in normalized frequency (default is .05).
//  fmax  : upper frequency bound (value of the hyperbolic instantaneous frequency law at the first sample), in normalized frequency (default is 0.5).
//  alpha : attenuation factor of the envelope (default is 300).
//  x     : time row vector containing the Altes signal samples.
//
// Description
//  altes generates the Altes signal in the time domain.
//
// Examples
//  x=altes(128,0.1,0.45); plot(x);
//  //plots an Altes signal of 128 points whose normalized frequency goes from 0.45 down to 0.1.
// See also 
//   klauder
//   anasing
//   anapulse
//   anastep
//   doppler
//
// Authors
//   H. Nahrstaedt - Aug 2010
//   P. Goncalves - September 1995.

//	Copyright (c) 1995 Rice University
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="altes"
  
  if nargin==4 then
    if type(alpha)<>1|size(alpha,"*")>1|~isreal(alpha) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,4));
    end 
    if alpha<=1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A value > %d expected.\n"),fname,4,1));
    end
  else
    alpha=300 ;
  end

  //fmin
  if nargin>=2 then
    if type(fmin)<>1|size(fmin,"*")>1|~isreal(fmin) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,2));
    end 
    if fmin<=0|fmin>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"]0,0.5]"));
    end
  else
    fmin=0.05
  end
  
  //fmax
  if nargin>=3 then
    if type(fmax)<>1|size(fmax,"*")>1|~isreal(fmax) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
    end 
    if fmax<=0|fmax>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"]0,0.5]"));
    end
    if fmin>fmax then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be < %g.\n"),fname,3,fmax));
    end
  else
    fmax=0.5
  end

  
  //N
  if nargin>=1 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,1));
    end
  else
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,4));
  end
  

  g = exp((log(fmax/fmin))^2/(8*log(alpha))) ;
  nu0 = sqrt(fmin*fmax) ;
  beta0=sqrt(2*log(g)*log(alpha));
  t0 = N/(exp(beta0)-exp(-beta0)) ;
  t1 = t0*exp(-beta0); t2 = t0*exp(beta0) ;
  b = -t0*nu0*g*log(g) ;
  t = linspace(t1,t2,N+1) ; t = t(1:N) ;
  x = (exp(-(log(t./t0).^2)/(2*log(g)))).*cos(2*%pi*b*log(t./t0)/log(g)) ;
  x = x.'/norm(x) ;


endfunction
