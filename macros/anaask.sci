function [y,am]=anaask(N,Ncomp,f0);
//	Amplitude Shift Keying (ASK) signal.
// Calling Sequence
// 	[Y,AM]=anaask(N)  
// 	[Y,AM]=anaask(N,NCOMP) 
// 	[Y,AM]=anaask(N,NCOMP,F0) 
// Parameters
//	N     : number of points
//	NCOMP : number of points of each component (default: N/5)
// 	F0    : normalized frequency.              (default: 0.25)
// 	Y     : signal
// 	AM    : resulting amplitude modulation     (optional).
//  Description
//  anaask returns a complex amplitude modulated signal of normalized frequency F0, with a uniformly
//	distributed random amplitude. Such signal is only 'quasi'-analytic.
//  Examples
//    [signal,am]=anaask(512,64,0.05); 
//    clf; subplot(211); plot(real(signal)); subplot(212); plot(am);
//  See also 
//      anafsk
//      anabpsk
//      anaqpsk
//  Authors
//  H. Nahrstaedt - Aug 2010
//	O. Lemoine - October 1995
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="anaask";
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,3));
  end;
     
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,1,0))
  end
  
  if nargin==3 then
      if type(f0)<>1|size(f0,"*")>1|~isreal(f0) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
     end 
     if f0<0|f0>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"[0,0.5]"));
     end
  else
    f0=0.25;
  end
  
  if nargin>=2 then
     if type(Ncomp)<>1|size(Ncomp,"*")>1|~isreal(Ncomp)|int(Ncomp)<>Ncomp then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,2));
  end 
  if Ncomp<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,2,0))
  end
  else
    Ncomp=round(N/5);
  end
 
  m=ceil(N/Ncomp); jumps=rand(m,1);
  am=jumps .*. ones(Ncomp,1); am=am(1:N,1);
  y=am.*fmconst(N,f0,1);
endfunction
