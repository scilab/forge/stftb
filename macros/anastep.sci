function y=anastep(N,ti);
// Analytic projection of unit step signal.
// Calling Sequence
//	Y=anastep(N) 
//	Y=anastep(N,TI) 
// Parameters
//	N  : number of points.
//	TI : starting position of the unit step.
// Description
// anastep generates the analytic projection of a unit step signal.
// Examples
//  signal=anastep(256,128);plot(real(signal));
//  signal=-2.5*anastep(512,301);plot(real(signal));
//  See also 
//      anasing
//      anafsk
//      anabpsk
//      anaqpsk
//      anaask
//  Authors
//      H. Nahrstaedt - Aug 2010
//	O. Lemoine - June 1995, F. Auger, August 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="anastep"
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,2));
  end;
  
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,1,0))
  end
  
  //ti
  if nargin==2 then
    if type(ti)<>1|size(ti,"*")>1|~isreal(ti)|int(ti)<>ti then
      error(msprintf(_("%s: Wrong type for input argument #%d: An integer value expected.\n"),fname,2));
    end 
    if ti<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,2,1))
    end
  else
    ti=round(N/2);
  end
  
  
  t=(1:N)';
  y=hilbert(bool2s(t>=ti));

endfunction
