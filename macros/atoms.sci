function [sig,locatoms]= atoms(N,coord,display);
//  Linear combination of elementary Gaussian atoms.
// Calling Sequence
//	[SIG,LOCATOMS] = atoms(N,COORD,DISPLAY) 
// Parameters
//	N        : number of points of the signal
//	COORD    : matrix of time-frequency centers, of the form  [t1,f1,T1,A1;...;tM,fM,TM,AM]. (ti,fi) are the   time-frequency coordinates of atom i, Ti is its time duration and Ai its amplitude. Frequencies f1..fM should   be normalized (between 0 and 0.5).   If nargin==1, the location of the atoms will be defined   by clicking with the mouse, with the help of a menu. The   default value for Ti is N/4.
//       DISPLAY    display switch. if DISPLAY=1 a figure is displayed,   otherwise nothing is displayed. default value is 1
//	SIG      : output signal.
//	LOCATOMS : matrix of time-frequency coordinates and durations of the  atoms. 
//  Description
//	atoms generates a signal consisting in a linear combination of elementary
//	gaussian wave packets. The locations of the time-frequency centers 
// 	of the different atoms are either fixed by the input parameter COORD 
//	or successively defined by clicking with the mouse (if NARGIN==1).  
//  Examples
//	 sig=atoms(128);
//	 sig=atoms(128,[32,0.3,32,1;56,0.15,48,1.22;102,0.41,20,0.7]); 
//	 sig=atoms(128,[32,0.3,32,1;56,0.15,48,1.22;102,0.41,20,0.7],0); 
//  Authors
//      H. Nahrstaedt - Aug 2010
//	P. Flandrin, May 1995 - O. Lemoine, February 1996.
//	F. Auger - O. Lemoine, June 1996.
//       E. Chassande-Mottin, F. Auger, May 1998.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="atoms";
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,3));
  end;
  
  //display
  if nargin==3 then
    if type(display)<>1|size(display,"*")>1|~isreal(display) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,3));
    end 
  else
    display=0
  end

  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,1,0))
  end

  sig=(1+%i)*zeros(N,1);
  t=linspace(0,2*%pi,100); 
  locatoms=[];
  Natoms=0; 
  T=N/4; A=1;

  if display then
    clf; 
    axsig= gca();
    axsig.axes_bounds=[0.10 0.65 0.80 0.25];
    axsig.data_bounds=[1 -1; N 1];
    
    axtfr = newaxes();
    axtfr.axes_bounds=[0.10 0.12 0.80 0.45];
    axtfr.data_bounds=[ 1 0; N 0.5];
    sca(axtfr)
    xlabel(_('Time')); ylabel(_('Normalized frequency')); 
  end;

  if nargin==1 then
    mprintf(_(' Default value for the time-duration : %f\n'),T);
    mprintf(_(' Default value for the amplitude     : %f\n'),A);
    choice=1;
    while choice then
      MaxSig=max(abs(real(sig)));
      sca(axsig); 
      if size(get(gca(),'Children'))>0
        delete(get(gca(),'Children'));
      end;
      plot((1:N)', real(sig),'g');
      if MaxSig==0.0,
        axsig.data_bounds=[1 -1; N 1]; xgrid();
      else
        axsig.data_bounds=[1 -MaxSig; N MaxSig]; xgrid();
      end;
      title([string(Natoms)+' Gaussian atom(s)'])
      choice = x_choose([ 'Add a gaussian atom';'Delete the last atom';'Change the time-duration'; 'Change the amplitude'],['ATOMS MENU'],'Stop');

      if choice==1 then // add a gaussian atom
        sca(axtfr);             
        rep=xgetmouse([%f %t]);
        t0=rep(1);f0=rep(2);
        t0=round(max(min(t0,N),1)); f0=max(min(f0,0.5),0.0);
        locatoms=[locatoms; t0 f0 T A];
        sca(axtfr);//axes(axtfr); 
        plot(t0,f0,'x'); plot((t0+%i*f0)+(0.5*T*cos(t)+%i*(2/(T*%pi))*sin(t)))
        sig=sig + A*amgauss(N,t0,T) .* fmconst(N,f0,t0);
        Natoms=Natoms+1;
      elseif (choice==2 & Natoms>=1),              // delete last atom
        t0=locatoms(Natoms,1);
        f0=locatoms(Natoms,2);
        Told =locatoms(Natoms,3);
        Aold =locatoms(Natoms,4);
        sca(axtfr);
        AxtfrChildren=get(gca(),'Children');  delete(AxtfrChildren(1:2)); 
        if (Natoms==1)
          Natoms=0; locatoms=[]; sig=(1+%i)*zeros(N,1);
        else
          sig=sig - Aold*amgauss(N,t0,Told) .* fmconst(N,f0,t0);
          Natoms=Natoms-1;
          locatoms=locatoms(1:Natoms,:);
        end;
      elseif choice==3,
        printf(_(' Old time duration : %f\n'), T);
        Told=T; T=input(_(' New time duration : '));
        if isempty(T), T=Told; end;
      elseif choice==4,
        printf(_(' Old amplitude : %f\n'), A);
        Aold=A; A=input(_(' New amplitude : '));
        if isempty(A), A=Aold; end;
      end
    end;
  elseif (nargin>=2),
      if type(coord)<>1|~isreal(coord) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real array expected.\n"),fname,2));
      end 
      [Natoms,ccoord]=size(coord);
      if size(coord,2)<>4 then
        error(msprintf(_("%s: Wrong size for input argument #%d: Must have %d columns expected.\n"),fname,2,4));
      end
      Natoms=size(coord,1);
      
      //ti
      t0=coord(:,1)
      if or(int(t0)<>t0)| or(t0<1) then
         error(msprintf(_("%s: Wrong value for input argument #%d: the column #%d must contain integer values.\n"),fname,2,1));
      end
      if or(t0>N) then
        error(msprintf(_("%s: Wrong value for input argument #%d: the elements of the column #%d must be <=%d.\n"),fname,2,1,N));
      end
      //f0
      f0=coord(:,2)
      if or(f0<0)|or(f0>0.5) then
         error(msprintf(_("%s: Wrong value for input argument #%d: the column #%d must contain values in [%g %g].\n"),fname,2,2,0,0.5));
      end
      
      //T
      T=coord(:,3)
      if or(int(T)<>T)|or(T<0)
        error(msprintf(_("%s: Wrong value for input argument #%d: the column #%d must contain integer values >=%d.\n"),fname,2,3,0));
      end
      
      //A
      A=coord(:,4)
      if or(A<0)
        error(msprintf(_("%s: Wrong value for input argument #%d: the column #%d must contain values >=%d.\n"),fname,2,4,0));
      end

    for k=1:Natoms,
      sig=sig+A(k)*amgauss(N,t0(k),T(k)) .* fmconst(N,f0(k),t0(k)); 
        if display then
          sca(axtfr);
          plot(t0(k),f0(k),'x');
          plot((t0(k)+%i*f0(k))+(0.5*T(k)*cos(t)+%i*(2/(T(k)*%pi))*sin(t)))
          plot(amgauss(N,t0(k),T(k)) .* fmconst(N,f0(k),t0(k)),"r")
        end;
      end
    locatoms=coord;
  end

  if display then,
    // hold off
    MinSig=min(real(sig));
    MaxSig=max(real(sig));
    sca(axsig);
    if size(get(gca(),'Children'))>0
      delete(get(gca(),'Children'));
    end;
    plot(real(sig),'g');
    if MaxSig==0.0,
      axsig.data_bounds=[1 -1; N 1]; xgrid();
    else
      axsig.data_bounds=[1 -MaxSig; N MaxSig]; xgrid();
    end;
    title([string(Natoms)+' '+_("Gaussian atom(s)")])
  end;

endfunction
