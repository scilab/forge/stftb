function Rx=correlmx(x,p,Rxtype);
// correlation matrix of a signal
// Calling Sequence
// Rx=correlmx(x,p) 
// Rx=correlmx(x,p,Rxtype) 
// Parameters 
//        Rx : correlation matrix (p+1) x (p+1)
//         x : analyzed signal
//         p : last autocorrelation lag
//    Rxtype : computation algorithm (default : 'fbhermitian')    possible values : 'hermitian', 'fbhermitian', 'burg' or 'fbburg'
// Examples
// N=100; sig=real(fmconst(N,0.1))+0.4*randn(N,1); 
// Rx=correlmx(sig,2,'burg'); [v,d] = eig(Rx), acos(-0.5*v(2,1)/v(1,1))/(2*pi)
// Rx=correlmx(sig,2,'hermitian'); [v,d] = eig(Rx), acos(-0.5*v(2,1)/v(1,1))/(2*pi)
// Authors
// H. Nahrstaedt - Aug 2010
// F. Auger, july 1998.

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  

  fname="correlmx";
  
  if nargin <2 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,2,3));
  end;

  //x
  if type(x)<>1|and(size(x)>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real or complex vector expected.\n"),fname,1));
  end
  x=x(:)
  L=size(x,1);
  
  //p
  if type(p)<>1|~isreal(p)|size(p,"*")<>1|int(p)<>p then
    error(msprintf(_("%s: Wrong type for input argument #%d: An integer expected.\n"),fname,2));
  end
  if p<1|p>L then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be in [%d %d].\n"),fname,2,2,L));
  end
  
  //Rxtype
  if nargin==3 then
    if type(Rxtype)<>10|size(Rxtype,"*")<>1 then
        error(msprintf(_("%s: Wrong type for input argument #%d: A character string  or an empty matrix expected.\n"),fname,3));
    end 
    Rxtype=convstr(Rxtype,"l")
    if and(Rxtype<>["hermitian","fbhermitian","burg","fbburg"]) then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),fname,3,"""hermitian"",""fbhermitian"",""burg"",""fbburg"""));
    end
  else
    Rxtype='fbhermitian';
  end
  
 
  if (Rxtype=='hermitian')|(Rxtype=='fbhermitian'),
    vector=x(p+1-(0:p)); 
    Rx=conj(vector) * vector.';
    for t=p+2:L,
      vector=x(t-(0:p)); 
      Rx=Rx+conj(vector) * vector.';
    end;
    Rx=Rx/(L-p);
  elseif (Rxtype=='burg')|(Rxtype=='fbburg'),
    R0=sum(abs(x).^2)/L; // variance
    Rpos=zeros(1,p); Rneg=zeros(1,p);
    for n=1:p, 
      Rpos(n)=sum(x(n+1:L).*conj(x(1:L-n)))/(L-n); 
      Rneg(n)=sum(x(1:L-n).*conj(x(n+1:L)))/(L-n);
    end;
    Rx=toeplitz([R0 Rpos],[R0 Rneg]);
  end;

  if (strncpy(Rxtype,2)=='fb'),
    Rx=0.5*(Rx+Rx');
  end;
endfunction

