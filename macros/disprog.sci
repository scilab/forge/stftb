function disprog(i,N,steps);
// Display progression of a loop.
// Calling Sequence
//	disprog(i,N,steps) 
// Parameters
//	I     : loop variable
//	N     : final value of i
//	STEPS : number of displayed steps.
// Description
//       disprog displays the progression of a loop.
// Examples
//      N=16; for i=1:N, disprog(i,N,5); end;
// Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, August, December 1995.
//       from an idea of R. Settineri.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  fname="disprog";
  
  if nargin <3 then
    error(msprintf(_("%s: Wrong number of input argument: %d  expected.\n"),fname,3));
  end;
  //N 
  if type(N)<>1|~isreal(N)|size(N,"*")<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,2));
  end
  if N<1 then
     error(msprintf(_("%s: Wrong value for input argument #%d : Must be > %d.\n"),fname,2,1));
  end
  //i
  if type(i)<>1|~isreal(i)|size(i,"*")<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,1));
  end
  if i<1|i>N then
     if i<1|i>N then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be in [%d %d].\n"),fname,1,1,N));
  end
  
  end
  //steps
   if type(steps)<>1|~isreal(steps)|size(steps,"*")<>1|int(steps)<>steps then
    error(msprintf(_("%s: Wrong type for input argument #%d: An integer expected.\n"),fname,3));
  end
  if i==1 then
    tic();
  end;

  if i==N then
    printf('100 %% complete in %g seconds.\n', toc())
  elseif (floor(i*steps/N)~=floor((i-1)*steps/N)),
    printf('%g ', floor(i*steps/N)*ceil(100/steps));
  end;

endfunction
