function [y,iflaw]=dopnoise(N,Fs,f0,d,v,t0,c);
// Complex noisy doppler signal.
// Calling Sequence
//	[Y,IFLAW]=dopnoise(N,FS,F0,D,V,T0,C)
// Parameters
//	 N  : number of points.  
//	 FS : sampling frequency (in Hertz).  
//	 F0 : target frequency   (in Hertz).  
//	 D  : distance from the line to the observer (in meters).  
//	 V  : target velocity    (in m/s).  
//	 T0 : time center                  (default : N/2).  
//	 C  : wave velocity      (in m/s)  (default : 340).
//	 Y  : Output signal.
//	 IFLAW : Model used as instantaneous frequency law.
// Description
//      dopnoise generates a complex noisy doppler signal, normalized so as to be of unit energy. 
// Examples
//       [z,iflaw]=dopnoise(500,200,60,10,70,128);
//       subplot(211); plot(real(z)); 
//       subplot(212); plot(iflaw); 
//       ifl=instfreq(z,11:478,10);
//       plot(ifl,'g'); 
//       sum(abs(z).^2)
//       a=gca(); a.axes_bounds([0 468 -0.01 0.51]);
// Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, July 94, August 95.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="dopnoise";
  if nargin < 5 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,5,7));
  end;
  
  //N 
  if type(N)<>1|~isreal(N)|size(N,"*")<>1|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: An integer expected.\n"),fname,1));
  end
  if N<1 then
    error(msprintf(_("%s: Wrong value for input argument #%d : Must be > %d.\n"),fname,1,1));
  end
  
  //Fs
  if type(Fs)<>1|~isreal(Fs)|size(Fs,"*")<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,2));
  end
  if Fs<=0 then 
    error(msprintf(_("%s: Wrong value for input argument #%d : Must be > %d.\n"),fname,2,0));
  end
  
  //f0
  if type(f0)<>1|~isreal(f0)|size(f0,"*")<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,3));
  end
  if f0<0|f0>Fs/2 then 
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"[0,"+string(Fs/2)+"]"));
  end
  
  //d
  if type(d)<>1|~isreal(d)|size(d,"*")<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,4));
  end
  if d<=0 then 
    error(msprintf(_("%s: Wrong value for input argument #%d : Must be > %d.\n"),fname,4,0));
  end
  
  //v
  if type(v)<>1|~isreal(v)|size(v,"*")<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,5));
  end
  if v<0 then 
    error(msprintf(_("%s: Wrong value for input argument #%d : Must be >= %d.\n"),fname,5,0));
  end
  
  //t0
  if nargin>=6 then
    if type(t0)<>1|~isreal(t0)|size(t0,"*")<>1|int(t0)<>t0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: An integer expected.\n"),fname,6));
    end
    if t0<1|t0>N then 
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in [%d %d].\n"),fname,6,1,N));
    end
  else
    t0=round(N/2)
  end
  
  //c
  if nargin==7 then
    if type(c)<>1|~isreal(c)|size(c,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,6));
    end
    if c<=0 then 
     error(msprintf(_("%s: Wrong value for input argument #%d : Must be > %d.\n"),fname,6,0));
    end
  else
    c=340.0; 
  end
  
  
  r=0.9; rr=r*r; r2=r*2; vv=v*v;
  x=rand(2*N,1,'normal');
  tmt0=((1:2*N)'-t0-N)/Fs ; 
  dist=sqrt(d^2+(v*tmt0).^2);
  iflaw=(1-vv*tmt0./dist/c)*f0/Fs; 
  y=zeros(2*N-2,1);
  for t=3:2*N,
    y(t)=x(t)-rr*(x(t-2)+y(t-2))+r2*cos(2.0*%pi*iflaw(t))*y(t-1);
  end; 
  y=hilbert(y(N+1:2*N) ./ sqrt(dist(N+1:2*N))); 
  y=y/sqrt(sum(abs(y).^2)); 
  iflaw=iflaw(N+1:2*N);

endfunction
