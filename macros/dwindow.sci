function Dh=dwindow(varargin);
// Derive a window.
// Calling Sequence
//	DH=dwindow(H)
// Description
//      dwindow(H) derives a window H.
//   Examples
//       plot(dwindow(tftb_window(210,'hanning')))
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, August 1994, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  fname="dwindow";
  nargin=size(varargin)
  if nargin <1 then
    error(msprintf(_("%s: Wrong number of input argument: %d  expected.\n"),fname,1));
  end;
  if nargin==1 then //dwindow(h)
    h=varargin(1);
    if type(h)<>1|and(size(h)>1)|~isreal(h) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
    end
    sz=size(h);
    h=h(:);
    
    N=size(h,1);
    if N<2 then
      Dh=0
    else
      Lh=(N-1)/2;
      step=(h(1)+h($))/2;
      ramp=(h($)-h(1))/(N-1);
      //    h2=[0;h-step-ramp*(-Lh:Lh).';0]; 
      //    Dh=(h2(3:$)-h2(1:$-2))/2 + ramp; 
      Dh=[(h(2)+step+ramp*(Lh+1));
          (h(3:$)-h(1:$-2));
          (-h($-1)-step+ramp*(Lh+1))]/2

      Dh=matrix(Dh,sz)
    end
  else //dwindow(N,name,...)
    N=varargin(1);name=varargin(2);
    if N<=0 then error('N should be strictly positive.'); end;
    name=convstr(name,"u");
    T=N-1;t=(0:T)';
    if (name=='RECTANG') | (name=='RECT'), 
      Dh=[1;zeros(N-2,1);-1];
    elseif (name=='HAMMING'),
      Dh=.92*%pi*sin(2*%pi*t/T)/T;
    elseif (name=='HANNING') | (name=='HANN'),
      Dh=%pi*sin(2*%pi*t/T)/T;
    elseif (name=='KAISER'),
      if (nargin==3), beta0=param; else beta0=3*%pi; end;
      Dh=-2*besseli(1, beta0*sqrt(1-(2*t/T-1).^2))*beta0.*(2*t/T-1)./(sqrt(1-(2*t/T-1).^2)*T*real(besseli(0, beta0)));
    elseif (name=='NUTTALL'), //Blackman Nuttall
      ind=2*%pi*t/T;
      Dh=%pi/T*(0.9783550*sin(ind)-0.5455980*sin(2*ind)+0.0638466*sin(3*ind));
    elseif (name=='BLACKMAN'),
      alpha=0.16;
      ind=2*%pi*t/T;
      Dh= %pi/T*(sin(ind)-2*alpha*sin(2*ind));
    elseif (name=='HARRIS'),//Blackman Harris
      ind=2*%pi*t/T;
      Dh=%pi/T*(.97658*sin(ind)-.565120*sin(2*ind)+0.070080*sin(3*ind));
    elseif (name=='BARTLETT') | (name=='TRIANG'),
      Dh=-2*sign(2*t/T-1)/T;
    elseif (name=='BARTHANN'),
      ind=t/T-1/2;
      Dh=(-.48*sign(ind)-0.76*%pi*sin(2*%pi*ind))/T;
    elseif (name=='PAPOULIS')|name=='SINE' then
      Dh=%pi/T*cos(%pi*t/T);
    elseif (name=='GAUSS'),
      if (nargin==3), K=param; else K=0.005; end;
      t=linspace(-1,1,N)';
      Dh=4*log(K)*t.*exp(log(K)*t.^2)/T;
    elseif (name=='PARZEN')
      //https://en.wikipedia.org/wiki/Window_function#Parzen_window
     Ts2=T/2;
     t = (-Ts2:Ts2)';
     t1 = t(t<-T/4) ;
     t2 = t(abs(t)<=T/4);
     Dh=[-12*(1-2*abs(t1)/N).^2 .* sign(t1)/N;
         -48*t2.*(1-2*abs(t2)/N)/N^2+48*t2^2.*sign(t2)/N^3;
         12*(1-2*abs(t1($:-1:1))/N).^2 .* sign(t1($:-1:1))/N];
    elseif (name=='POWERSINE'),
      if (nargin==3), L=param; else L=1; end;
      Dh=(2*L*%pi/T)*sin(%pi*t/T)^(2*L-1).*cos(%pi*t/T);
    elseif (name=='DOLPH') | (name=='DOLF'),
      Dh=dwindow(tftb_window(varargin(:)));
    elseif (name=='NUTBESS'),
      if (nargin==3) then
        beta0=param; nu=0.5; 
      elseif (nargin==4) then
        beta0=param; nu=param2;
      else 
        beta0=3*%pi; nu=0.5;
      end;
      ind=(-T/2:T/2)' *2/N; 
      s=sqrt((-ind.^2+1));
      bj=besselj(nu,%i*beta0*s);
      bj1=besselj(nu+1,%i*beta0*s);
      Dh=real(2*(-ind.^2+1).^(nu/2-1).*ind.*(...
          %i*sign(bj).*bj1.*s.*bj*beta0...
          -sign(bj).*bj.^2*nu...
          -%i*sign(bj).*real(bj).*bj1.*s*beta0...
          +%i*sign(bj).*bj1.*s.*bj*beta0...
          -sign(bj).*bj.^2*nu...
          )./(bj.*sign(bj)*N*real(besselj(nu,%i*beta0))));
    elseif (name=='SPLINE'),
      if (nargin < 3),
        nfreq=1
        p=%pi*N*nfreq/10.0;
      elseif (nargin==3),
        nfreq=param; 
        p=%pi*N*nfreq/10.0;
      else 
        nfreq=param; 
        p=param2;
      end;
      ind=(-T/2:T/2)'; 
      t=(0.5*nfreq/p)*ind/%pi;
      Dh=0.5*nfreq/(p*%pi)*(sin(%pi*t)./(%pi*t)).^p*p.*(cos(%pi*t)./t-sin(%pi* t)./(%pi*t^2))*%pi.*t./sin(%pi*t);
      Dh(isnan(Dh))=0;
    elseif name=='FLATTOP'|name=='FLATTOP_NI',//National instrument (http://edoc.mpg.de/395068)
      ind=2*%pi/T*t;
      Dh=%pi/T*(1.0417944*sin(ind)-0.7921596*sin(2*ind));
    elseif (name=='FLATTOP_M'), //Matlab equivalent
      ind=2*%pi/T*t;
      Dh=%pi/T*(.83326316*sin(ind)-1.109052632*sin(2*ind)+0.501473682*sin(3*ind)-0.055578944*sin(4*ind));
    elseif (name=='FLATTOP_SRS'), //Standford Research (http://edoc.mpg.de/395068)
      ind=2*%pi/T*t;                            
      Dh=%pi/T*(3.86*sin(ind)-5.16*sin(2*ind)+2.328*sin(3*ind)-0.224*sin(4*ind));
    else 
      mess=[" unknown window name. choose one of:"
            "	''Hamming'',   ''Hanning'', ''Nuttall'',  ''Papoulis'',  ''Sine'', "
            "	''Harris'',    ''Rect'',    ''Triang'',   ''Bartlett'',  ''BartHann'', "
            "	''Blackman'',  ''Gauss'',   ''Parzen'',   ''Kaiser'',    ''Dolph'', "
            "	''Powersine'', ''Nutbess'', ''spline'',   ''Flattop'',   ''Flattop_ni'', "
            "	''Flattop_m'', ''Flattop_srs''."];
      error(mess);
    end;

  end
  
endfunction
