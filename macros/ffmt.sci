function [mellin,beta0]=ffmt(X,fmin,fmax,N);
//    Fast Fourier Mellin Transform.
// Calling Sequence
//       [MELLIN,BETA]=ffmt(X)
//       [MELLIN,BETA]=ffmt(X,FMIN,FMAX)
//       [MELLIN,BETA]=ffmt(X,FMIN,FMAX,N)
// Parameters
//       X : signal in time (Nx=length(X)).
//       FMIN,FMAX : respectively lower and upper frequency bounds of the analyzed signal. These parameters fix the equivalent  frequency bandwidth (expressed in Hz). When unspecified, you   have to enter them at the command line from the plot of the spectrum. FMIN and FMAX must be >0 and <=0.5.     
//       N : number of analyzed voices. N must be even (default : automatically determined).
//       MELLIN : the N-points Mellin transform of signal S.
//       BETA : the N-points Mellin variable.
// Description
//       ffmt computes the Fast Mellin  Transform of signal X.
// Examples
//        sig=altes(128,0.05,0.45); 
//        [MELLIN,BETA]=ffmt(sig,0.05,0.5,128);
//        plot(BETA,real(MELLIN));
//  See also 
//       iffmt
//   Authors
//      H. Nahrstaedt - Aug 2010
//       P. Goncalves 9-95 - O. Lemoine, June 1996. 
//       Copyright (c) 1995 Rice University - CNRS (France) 1996.

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="ffmt";
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,4));
  end;
  
  //X
  if type(X)<>1|and(size(X)<>1)|~isreal(X) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
  end
  X=X(:);
  Nx=size(X,1);
  
  //N
  if nargin>=4 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,4));
    end 
    if N<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,4,1));
    end
    if rem(N,2)~=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: An even number expected.\n"),fname,4));
    end
  else
    N=[];
  end
  
  //fmax
  if nargin>=3 then
    if type(fmax)<>1|size(fmax,"*")>1|~isreal(fmax) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
    end 
    if fmax<=0|fmax>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"]0,0.5]"));
    end
  else
    fmax=[]
  end
  
  //fmin
  if nargin>=2 then
    if type(fmin)<>1|size(fmin,"*")>1|~isreal(fmin) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,2));
    end 
    if fmin<=0|fmin>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"]0,0.5]"));
    end
    if fmin>fmax then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be < %g.\n"),fname,2,fmax));
    end
  else
    fmin=[]
  end

  Mt = length(X); 
  Z  = hilbert(X);
  M  = (Mt+rem(Mt,2))/2;

  if fmin==[]|fmax==[] then 
    STF = fft(fftshift(X),-1,1); Nstf=length(STF);
    sp = (abs(STF(1:Nstf/2))).^2; Maxsp=max(sp);
    f = linspace(0,0.5,Nstf/2+1) ; f = f(1:Nstf/2);
    plot(f,sp) ; xgrid;
    xlabel(_('Normalized frequency'));
    title(_("Analyzed signal energy spectrum"));
    if fmin==[] then
      indmin=min(find(sp>Maxsp/1000));
      fmindflt=max([0.001 0.05*fix(f(indmin)/0.05)]);
      txtmin=msprintf(_('Lower frequency bound [%s] :'),string(fmindflt));
      while  %t then
        fmin = input(txtmin); 
        if fmin==[] then 
          fmin=fmindflt; break
        elseif fmin>0&fmin<=0.5 then
          break
        else
          mprintf(_("%s: Wrong given value for lower frequency bound: Must be in the interval %s.\n"),fname,"]0,0.5]"); 
        end
      end
    end
    if fmax==[] then
      indmax=max(find(sp>Maxsp/1000));
      fmaxdflt=0.05*ceil(f(indmax)/0.05);
      txtmax=msprintf(_('Upper frequency bound [%s] :'),string(fmaxdflt));
      while  %t then
        fmax = input(txtmax); 
        if fmax==[] then 
          fmax=fmaxdflt; break
        elseif fmax>0&fmax<=0.5&fmax>fmin then
          break
        else
          mprintf(_("%s: Wrong given value for upper frequency bound: Must be in the interval %s and greater than fmin\n"),fname,"]0,0.5]"); 
        end
      end  
    end
  end
  
  

  B = fmax-fmin;       		// Bandwidth of the signal X
  R = B/((fmin+fmax)/2);		// Relative bandwidth of X

  Nq= ceil((B*Mt*(1+2/R)*log((1+R/2)/(1-R/2)))/2);
  Nmin = Nq-rem(Nq,2);
  Ndflt = 2^nextpow2(Nmin);
  if N==[] then
    Ndflt = 2^nextpow2(Nmin);
    Ntxt=msprintf(_('Number of frequency samples (>=%d) [%d] : '),Nmin,Ndflt);
    while  %t then
      N=input(Ntxt); 
      if N==[] then 
        N=Ndflt;break;
      elseif int(N)<>N|rem(N,2)~=0 then
        mprintf(_("%s: Wrong given value : an even  integer expected.\n"),fname);  
      else
        if N<Nmin then
          warning(msprintf(_("%s: Wrong given value : an even  integer >= %d expected.\n"),fname,Nmin));  
        end
        break
      end
    end

  else
    if N<Nmin then
      warning(msprintf(_("%s: Wrong value for input argument #%d: An integer >= %d expected.\n"),fname,4,Nmin));
    end      
  end


  // Geometric sampling of the analyzed spectrum
  No2 = N/2;
  k = (1:No2);
  q = (fmax/fmin)^(1/(No2-1));
  t = (1:Mt)-M-1;
  geo_f  = fmin*(exp((k-1).*log(q)));
  tfmatx = zeros(Mt,N);
  tfmatx = exp(-2*%i*%pi*t'*geo_f);
  ZS = Z.'*tfmatx; 
  ZS(No2+1:N) = zeros(1,N-No2);


  // Mellin transform computation of the analyzed signal
  p = 0:(N-1);
  L = log(fmin)/log(q);
  mellin = N*log(q)*fftshift(ifft(ZS)).*exp(%i*2*%pi*L*(p/N-1/2));
  beta0   = (p/N-1/2)./log(q);


  // Normalization
  SP = fft(Z); 
  indmin = 1+round(fmin*(Nx-2));
  indmax = 1+round(fmax*(Nx-2));
  SPana = SP(indmin:indmax);
  nu = (indmin:indmax)'/N; 
  SPp = SPana./nu;
  Normsig = sqrt(SPp'*SPana);

  mellin = mellin*Normsig/norm(mellin);
endfunction

