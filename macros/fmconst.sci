function [y,iflaw] = fmconst(N,fnorm,t0);
// Signal with constant frequency modulation.
// Calling Sequence
//	[Y,IFLAW] = fmconst(N,FNORM,T0) 
//     Parameters
//	N     : number of points.
//	FNORM : normalised frequency.       (default: 0.25)
//	T0    : time center.                (default: round(N/2))
//	Y     : signal.
//	IFLAW : instantaneous frequency law (optional).
//     Description
//      fmconst generates a frequency modulation  with a constant frequency fnorm.
//	The phase of this modulation is such that y(t0)=1.
//     Examples 
//        z=amgauss(128,50,30).*fmconst(128,0.05,50); plot(real(z));
//
//     See also 
//      fmlin
//      fmsin
//      fmodany
//      fmhyp
//      fmpar
//      fmpower
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="fmconst";
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,3));
  end;
  
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<1 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,1,1));
  end
  
  //t0
  if nargin==3 then
    if type(t0)<>1|size(t0,"*")>1|~isreal(t0)|int(t0)<>t0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if t0<1|t0>N then
      error(msprintf(_("%s: Wrong value for input argument #%d:  Must be in the interval %s.\n"),fname,3,"[1,"+string(N)+"]"));
    end
  else
    t0=round(N/2); 
  end
  
  //fnorm
  if nargin>=2 then
    if type(fnorm)<>1|~isreal(fnorm)|size(fnorm,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,2));
    end
    if abs(fnorm)>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"[-0.5,0.5]"));
    end
  else
    fnorm=0.25 ;
  end
  
  tmt0=(1:N)'-t0;
  y = exp(%i*2.0*%pi*fnorm*tmt0);
  y=y/y(t0);
  if (nargout==2), iflaw=fnorm*ones(N,1); end;

endfunction
