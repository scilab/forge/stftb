function [x,iflaw] = fmhyp(N,p1,p2)
//	Signal with hyperbolic frequency modulation.
// Calling Sequence
//	[X,IFLAW]=fmhyp(N,P1,P2)
// Parameters
//	N  : number of points in time
//	P1 : if the number of input arguments (NARGIN) is 2, P1 is a   vector containing the two coefficients [F0 C] for an  hyperbolic instantaneous frequency  (sampling frequency is set to 1).  If NARGIN=3, P1 (as P2) is a time-frequency point of the form [Ti Fi]. Ti is in seconds and Fi is a normalized frequency   (between 0 and 0.5). The coefficients F0 and C are then deduced    such that the frequency modulation law fits the points P1 and P2.
//	P2 : same as P1 is NARGIN=3         (optional)
//	X  : time row vector containing the modulated signal samples 
//	IFLAW : instantaneous frequency law
// Description
//      fmhyp generates a signal with hyperbolic frequency modulation : X(t) = exp(i.2.pi(F0.t + C/log|t|)) 
//  Examples
//       [X,IFLAW]=fmhyp(128,[1 .5],[32 0.1]);
//       subplot(211); plot(real(X));
//       subplot(212); plot(IFLAW);
//   See also 
//      fmlin
//      fmsin
//      fmpar
//      fmconst
//      fmodany
//      fmpower
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves - October 1995, O. Lemoine - November 1995
//	Copyright (c) 1995 Rice University

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="fmhyp";
  
  if nargin <2 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,2,3));
  end;
  
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<1 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,1,1));
  end
  
  //p1
  if nargin==2 then
    if type(p1)<>1|size(p1,"*")<>2|~isreal(p1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector of size %d expected.\n"),fname,2,2));
    end
    f0=p1(1);
    c=p1(2);
    if f0<=0|f0>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be in the interval %s.\n"),fname,2,1,"[-0.5,0.5]"));
    end 
  end
  if nargin==3 then
    if type(p1)<>1|size(p1,"*")<>2|~isreal(p1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector of size %d expected.\n"),fname,2,2));
    end
    if p1(1)>N | p1(1)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be in the interval %s.\n"),fname,2,1,"[1,"+string(N)+"]"));
    end
    if p1(2)<0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be >= %d.\n"),fname,2,2,0));
    end
      
    if type(p2)<>1|size(p2,"*")<>2|~isreal(p2) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector of size %d expected.\n"),fname,3,2));
    end
      
    if p2(1)>N | p2(1)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be in the interval %s.\n"),fname,3,1,"[1,"+string(N)+"]"));
    end
    if p2(2)<0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be >= %d.\n"),fname,3,2,0));
    end
      
    c = (p2(2) - p1(2))/(1/p2(1) - 1/p1(1)) ;
    f0 = p1(2) - c/p1(1) ;
  end  

  t = 1:N ;
  
  phi = 2*%pi*(f0*t + c*log(abs(t))); 
  iflaw = (f0 + c*abs(t).^(-1)).' ;
  
  aliasing = find(iflaw < 0 | iflaw > 0.5) ;
  if isempty(aliasing) == 0
    warning(msprintf(_("%s: signal is undersampled or has negative frequencies\n"),fname))
  end
  
  x = exp(%i*phi).';
endfunction
