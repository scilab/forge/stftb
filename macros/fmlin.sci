function [y,iflaw]=fmlin(N,fnormi,fnormf,t0);
//	Signal with linear frequency modulation.
// Calling Sequence
//	[Y,IFLAW]=fmlin(N)
//	[Y,IFLAW]=fmlin(N,FNORMI)
//	[Y,IFLAW]=fmlin(N,FNORMI,FNORMF)
//	[Y,IFLAW]=fmlin(N,FNORMI,FNORMF,T0)
//  Parameters
//	N       : number of points
//	FNORMI  : initial normalized frequency (default: 0.0)
//	FNORMF  : final   normalized frequency (default: 0.5)
//	T0      : time reference for the phase (default: N/2).
//	Y       : signal
//	IFLAW   : instantaneous frequency law  (optional).
//  Description
//      fmlin generates a linear frequency  modulation.
//	The phase of this modulation is such that Y(T0)=1.
//  Examples
//       z=amgauss(128,50,40).*fmlin(128,0.05,0.3,50); 
//       plot(real(z));
//   See also 
//      fmhyp
//      fmsin
//      fmpar
//      fmconst
//      fmodany
//      fmpower
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  [nargout,nargin]=argn(0);
  fname="fmlin";
  
  if nargin <1 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,4));
  end;
  
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<1 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,1,1));
  end
  
  //t0
  if nargin==4 then
    if type(t0)<>1|size(t0,"*")>1|~isreal(t0)|int(t0)<>t0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,4));
    end 
    if t0<1|t0>N then
      error(msprintf(_("%s: Wrong value for input argument #%d:  Must be in the interval %s.\n"),fname,4,"[1,"+string(N)+"]"));
    end    
  else
    t0 = round(N/2);
  end
  
  //fnormf
  if nargin>=3 then
    if type(fnormf)<>1|~isreal(fnormf)|size(fnormf,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,3));
    end
    if abs(fnormf)>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"[-0.5,0.5]"));
    end
  else
    fnormf=0.5;
  end
  
  //fnormi
  if nargin>=2 then
    if type(fnormi)<>1|~isreal(fnormi)|size(fnormi,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,2));
    end
    if abs(fnormi)>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"[-0.5,0.5]"));
    end   
  else
    fnormi=0.0;
  end
  y = (1:N)';
  y = fnormi*(y-t0) + ((fnormf-fnormi)/(2.0*(N-1))) * ((y-1).^2 - (t0-1).^2);
  y = exp(%i*2.0*%pi*y) ;
  y=y/y(t0);
  if (nargout==2), iflaw=linspace(fnormi,fnormf,N).'; end;
  
endfunction
