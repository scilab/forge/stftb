function [x,iflaw]=fmpar(N,p1,p2,p3)
//	Parabolic frequency modulated signal.
// Calling Sequence
//	[X,IFLAW]=fmpar(N,P1) 
//	[X,IFLAW]=fmpar(N,P1,P2) 
//	[X,IFLAW]=fmpar(N,P1,P2,P3) 
//    Parameters
//	N  : the number of points in time
//	P1 : if NARGIN=2, P1 is a vector containing the three coefficients [A0 A1 A2] of the polynomial instantaneous phase.  If NARGIN=4, P1 (as P2 and P3) is a time-frequency point of  the form [Ti Fi].  The coefficients (A0,A1,A2) are then deduced such that   the frequency modulation law fits these three points.
//	P2,P3 : same as P1 if NARGIN=4.       (optional)
//	X     : time row vector containing the modulated signal samples 
//	IFLAW : instantaneous frequency law
//   Description
//      fmpar generates a signal with parabolic frequency modulation law.
//	X(T) = exp(j*2*pi(A0.T + A1/2.T^2 +A2/3.T^3)) 
//   Examples
//       [X,IFLAW]=fmpar(128,[1 0.4],[64 0.05],[128 0.4]);
//       subplot(211);plot(real(X));subplot(212);plot(IFLAW);
//       [X,IFLAW]=fmpar(128,[0.4 -0.0112 8.6806e-05]);
//       subplot(211);plot(real(X));subplot(212);plot(IFLAW);
//   See also 
//      fmhyp
//      fmsin
//      fmodany
//      fmconst
//      fmlin
//      fmpower
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves - October 1995, O. Lemoine - November 1995
//	Copyright (c) 1995 Rice University

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="fmhyp";
  
  if nargin <2 then
    error(msprintf(_("%s: Wrong number of input argument: %d or %d expected.\n"),fname,2,4));
  end;
  
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<1 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,1,1));
  end
  
  //p1
  if nargin==2 then
    if type(p1)<>1|size(p1,"*")<>3|~isreal(p1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector of size %d expected.\n"),fname,2,3));
    end
    a0 = p1(1) ; a1 = p1(2) ; a2 = p1(3) ;
  else
     if type(p1)<>1|size(p1,"*")<>2|~isreal(p1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector of size %d expected.\n"),fname,2,2));
    end
    if p1(1)>N | p1(1)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be in the interval %s.\n"),fname,2,1,"[1,"+string(N)+"]"));
    end
    if p1(2)<0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be >= %d.\n"),fname,2,2,0));
    end

    if type(p2)<>1|size(p2,"*")<>2|~isreal(p2) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector of size %d expected.\n"),fname,3,2));
    end
    if p2(1)>N | p2(1)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be in the interval %s.\n"),fname,3,1,"[1,"+string(N)+"]"));
    end
    if p2(2)<0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be >= %d.\n"),fname,3,2,0));
    end
   
    if type(p3)<>1|size(p3,"*")<>2|~isreal(p3) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector of size %d expected.\n"),fname,4,2));
    end
    if p3(1)>N | p3(1)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be in the interval %s.\n"),fname,4,1,"[1,"+string(N)+"]"));
    end
    if p3(2)<0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be >= %d.\n"),fname,4,2,0));
    end
    Y = [p1(2) p2(2) p3(2)] ;
    X = [1 1 1;p1(1) p2(1) p3(1);p1(1)^2 p2(1)^2 p3(1)^2] ;
    coef = Y*inv(X) ; 
    a0 = coef(1) ;
    a1 = coef(2) ;
    a2 = coef(3) ;
  end



  t=1:N;

  phi = 2*%pi*(a0*t + a1/2*t.^2 + a2/3*t.^3) ;
  iflaw = (a0 + a1*t + a2*t.^2).' ;

  aliasing = find(iflaw<0 | iflaw>0.5) ;
  if isempty(aliasing) == 0
     warning(msprintf(_("%s: signal is undersampled or has negative frequencies\n"),fname))
  end

  x = exp(%i*phi).';

endfunction
