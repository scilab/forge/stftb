function [y,iflaw]=fmsin(N,fnormin,fnormax,period,t0,fnorm0,pm1);
//	Signal with sinusoidal frequency modulation.
// Calling Sequence
//	[Y,IFLAW]=fmsin(N)
//	[Y,IFLAW]=fmsin(N,FNORMIN)
//	[Y,IFLAW]=fmsin(N,FNORMIN,FNORMAX)
//	[Y,IFLAW]=fmsin(N,FNORMIN,FNORMAX,PERIOD)
//	[Y,IFLAW]=fmsin(N,FNORMIN,FNORMAX,PERIOD,T0)
//	[Y,IFLAW]=fmsin(N,FNORMIN,FNORMAX,PERIOD,T0,FNORM0)
//	[Y,IFLAW]=fmsin(N,FNORMIN,FNORMAX,PERIOD,T0,FNORM0,PM1)
// Parameters
//	N       : number of points.
//	FNORMIN : smallest normalized frequency          (default: 0.05) 
//	FNORMAX : highest normalized frequency           (default: 0.45)
//	PERIOD  : period of the sinusoidal fm            (default: N   )
//	T0      : time reference for the phase           (default: N/2 )
//	FNORM0  : normalized frequency at time T0        (default: 0.25)
//	PM1     : frequency direction at T0 (-1 or +1)	 (default: +1  )
//	Y       : signal
//	IFLAW   : its instantaneous frequency law (optional).
//  Description
//    fmsin generates a frequency modulation with a sinusoidal frequency.
//	This sinusoidal modulation is designed such that the instantaneous
//	frequency at time T0 is equal to FNORM0, and the ambiguity 
//	between increasing or decreasing frequency is solved by PM1.
//   Examples
//      z=fmsin(140,0.05,0.45,100,20,0.3,-1.0);plot(real(z));
//   See also 
//      fmhyp
//      fmpower
//      fmodany
//      fmconst
//      fmlin
//      fmpar
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="fmsin";
  
  if nargin <1 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,7));
  end;
  
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<1 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,1,1));
  end

  //pm1
  if nargin==7 then
    if type(pm1)<>1|size(pm1,"*")>1|~isreal(pm1)|int(pm1)<>pm1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,7));
    end 
    if abs(pm1)<>1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in %s.\n"),fname,7,"{-1,1}"));
    end
  else
    pm1=1
  end
  
  

  //t0
  if nargin>=5 then
    if type(t0)<>1|size(t0,"*")>1|~isreal(t0)|int(t0)<>t0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,5));
    end 
    if t0<1|t0>N then
      error(msprintf(_("%s: Wrong value for input argument #%d:  Must be in the interval %s.\n"),fname,5,"[1,"+string(N)+"]"));
    end    
  else
    t0= round(N/2)
  end
  
  //period
  if nargin>=4 then
    if type(period)<>1|size(period,"*")>1|~isreal(period) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar value expected.\n"),fname,4));
    end 
    if period<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,4,1));
    end
  else
    period=N
  end

  //fnormax
  if nargin>=3 then
    if type(fnormax)<>1|~isreal(fnormax)|size(fnormax,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,3));
    end
    if abs(fnormax)>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"[-0.5,0.5]"));
    end   
  else
    fnormax = 0.45
  end
  
  //fnormin
  if nargin>=2 then
    if type(fnormin)<>1|~isreal(fnormin)|size(fnormin,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,2));
    end
    if abs(fnormin)>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"[-0.5,0.5]"));
    end   
  else
    fnormin = 0.05
  end
  
  //fnorm0
  if nargin>=6 then
    if type(fnorm0)<>1|~isreal(fnorm0)|size(fnorm0,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,6));
    end
    if fnorm0<fnormin|fnorm0>fnormax then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,6,sci2exp([fnormin,fnormax])));
    end
  else
    fnorm0 = 0.5*(fnormin+fnormax)
  end
  
  fnormid=0.5*(fnormax+fnormin);
  delta  =0.5*(fnormax-fnormin);
  phi=-pm1*acos((fnorm0-fnormid)/delta);
  time=(1:N)-t0;
  phase=2*%pi*fnormid*time+delta*period*(sin(2*%pi*time/period+phi)-sin(phi));
  y = exp(%i*phase).';
  if (nargout==2)
    iflaw=fnormid+delta*cos(2*%pi*time'/period+phi);
  end
  
endfunction
