function [Image,NbDots]=imextrac(Image,ptrace);
// imextrac(Image) extract and isolate dots in a binary image
// Calling Sequence
// image2=imextrac(Image)
// Examples
// Image=[1 0 0 0 0 0 1 0 0 0 1 0 ;...
//        1 1 0 0 0 1 1 0 0 0 1 1 ;...
//        1 0 0 0 1 0 1 0 0 0 0 1 ;...
//        0 0 0 0 1 1 1 0 0 0 0 0 ;...
//        0 0 0 0 0 0 0 0 0 0 0 0 ];
//        image2=imextrac(Image)
//    See also 
//       tfrsurf
//   Authors
//      H. Nahrstaedt - Aug 2010
// 	F. Auger, oct 1999
//	Copyright (c) CNRS - France 1999. 

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="imextrac"
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,2));
  end;
  
  //Image
  if type(Image)<>4 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of booleans expected.\n"),fname,1));
  end
  Image=bool2s(Image);
  [Nbrow,Nbcol]=size(Image);
  
  if nargin==2 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,2));
    end
  else
    ptrace=%f;
  end


  NbDots=1 ; 
  if ptrace then printf('extracting dots in the image.\n'); end;

  if (Image(1,1)==1),
    NbDots=NbDots+1; Image(1,1)=NbDots;
  end;

  for i=2:Nbcol,
    if (Image(1,i)==1),
      if (Image(1,i-1)>1),
        Image(1,i)=Image(1,i-1);
      else
        NbDots=NbDots+1; Image(1,i)=NbDots;
      end;
    end; 
  end;
  if ptrace then disprog(1,Nbrow,10); end;

  for j=2:Nbrow,
    if (Image(j,1)==1),
      if (Image(j-1,1)>1),
        Image(j,1)=Image(j-1,1);
      else
        NbDots=NbDots+1;
        Image(j,1)=NbDots;
      end;
    end;

    for i=2:Nbcol,
      if (Image(j,i)==1) then
        if (Image(j-1,i)==0)&(Image(j,i-1)==0) then
          NbDots=NbDots+1
          Image(j,i)=NbDots;
        elseif (Image(j-1,i)==0)&(Image(j,i-1)>1) then
          Image(j,i)=Image(j,i-1);
        elseif (Image(j-1,i)>1)&(Image(j,i-1)==0) then
          Image(j,i)=Image(j-1,i);
        elseif (Image(j-1,i)>1)&(Image(j,i-1)>1) then
          MinDot=min(Image(j-1,i),Image(j,i-1));
          MaxDot=max(Image(j-1,i),Image(j,i-1));
          Indices=find(Image==MaxDot); 
          Image(Indices)=MinDot;
          Image(j,i)=MinDot;
        else 
          error(msprintf(_("%s,should never happen\n"),fname));
        end;
      end; 
    end;
    
    if ptrace then disprog(j,Nbrow,10); end;
  end;
endfunction
