function [fnormhat,t]=instfreq(x,t,L,ptrace);
// Instantaneous frequency estimation.
// Calling Sequence
//	[FNORMHAT,T]=instfreq(X)
//	[FNORMHAT,T]=instfreq(X,T)
//	[FNORMHAT,T]=instfreq(X,T,L)
//	[FNORMHAT,T]=instfreq(X,T,L,TRACE)
//  Parameters
//	X : Analytic signal to be analyzed.
//	T : Time instants	        (default : 2:length(X)-1).
//	L : If L=1, computes the (normalized) instantaneous frequency  of the signal X defined as angle(X(T+1)*conj(X(T-1)) ;   if L>1, computes a Maximum Likelihood estimation of the   instantaneous frequency of the deterministic part of the signal    blurried in a white gaussian noise.	    L must be an integer       	(default : 1).
//	TRACE : if nonzero, the progression of the algorithm is shown   (default : 0).
//	FNORMHAT : Output (normalized) instantaneous frequency.
//	T : Time instants.
//  Description
//      instfreq computes the instantaneous frequency of the analytic signal X at time instant(s) T, using the
//	trapezoidal integration rule.
//	The result FNORMHAT lies between 0.0 and 0.5.
//  Examples
//	 x=fmsin(70,0.05,0.35,25); [instf,t]=instfreq(x); plot(t,instf)
//	 N=64; SNR=10.0; L=4; t=L+1:N-L; x=fmsin(N,0.05,0.35,40);
//	 sig=sigmerge(x,hilbert(rand(N,1,'normal')),SNR);
//	 plotifl(t,[instfreq(sig,t,L),instfreq(x,t)]); xgrid;
//	 title ('theoretical and estimated instantaneous frequencies');
//   See also  
//      kaytth
//      sgrpdlay
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, March 1994, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="instfreq"
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,4));
  end;
  
  //x
  if type(x)<>1|and(size(x)<>1)|isreal(x) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A complex vector expected.\n"),fname,1));
  end
  x=x(:);
  xcol=size(x,1);
  
  //ptrace
  if nargin==4 then
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,4));
    end
  else
    ptrace=%f
  end
  
  //L
  if nargin>=3 then
    if type(L)<>1|size(L,"*")<>1|~isreal(L)|int(L)<>L then
      error(msprintf(_("%s: Wrong type for input argument #%d: An integer value expected.\n"),fname,3));
    end
    if L<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A value >= %d expected.\n"),fname,3,1));
    end
  else
    L = 1;
  end
  
  //t
  if nargin>=2 then
    if type(t)<>1|and(size(t)<>1)|~isreal(t)|or(int(t)<>t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A vector of integer values expected.\n"),fname,2)); 
    end
    if or(t<L|t>xcol-L) then
      error(msprintf(_("%s: Wrong value for input argument #%d: the elements must be in the interval %s.\n"),fname,2,"]"+string(L)+","+string(xcol-L)+"["));
    end
    t=matrix(t,1,-1);
  else
    t=2:xcol-1
  end
  tcol=size(t,2);
  

  if (L==1) then
    fnormhat=0.5*(angle(-x(t+1).*conj(x(t-1)))+%pi)/(2*%pi);
  else
    H=kaytth(L); 
    
    for icol=1:tcol,
      if ptrace, disprog(icol,tcol,10); end;
      ti = t(icol); tau = 0:L;
      R = x(ti+tau).*conj(x(ti-tau));
      M4 = R(2:L+1).*conj(R(1:L));
      d=2e-6;
      tetapred = H * (mtlb_unwrap(angle(-M4))+%pi);
      while tetapred<0.0 , tetapred=tetapred+(2*%pi); end;
      while tetapred>2*%pi, tetapred=tetapred-(2*%pi); end;
      iter = 1;
      while (d > 1e-6)&(iter<50),
        M4bis=M4 .* exp(-%i*2.0*tetapred);
        teta = H * (mtlb_unwrap(angle(M4bis))+2.0*tetapred);
        while teta<0.0 , teta=(2*%pi)+teta; end;
        while teta>2*%pi, teta=teta-(2*%pi); end;
        d=abs(teta-tetapred);
        tetapred=teta; iter=iter+1;
      end;
      fnormhat(icol,1)=teta/(2*%pi);
    end;
    
  end;

endfunction

function B=angle(A)
  B=atan(imag(A),real(A));
endfunction



function retval = mtlb_unwrap (a, tol, dim)
  [nargout,nargin]=argn(0);
  if (nargin < 1 | nargin > 3)
    error("wrong number of parameters!");
  end

  nd = ndims (a);
  sz = size (a);

  if (nargin == 3)
    if (~ (sum(length(dim))==1 & dim == round (dim)) & dim > 0 &   dim < (nd + 1))
      error ("unwrap: dim must be an integer and valid dimension");
    end
  else
    // Find the first non-singleton dimension
    dim  = 1;
    while (dim < nd + 1 & sz(dim) == 1)
      dim = dim + 1;
    end
    if (dim > nd)
      dim = 1;
    end
  end

  if (nargin < 2 | isempty (tol))
    tol = %pi;
  end

  // Don't let anyone use a negative value for TOL.
  tol = abs (tol);

  rng = 2*%pi;
  m = sz(dim);
  // Handle case where we are trying to unwrap a scalar, or only have
  // one sample in the specified dimension.
  if (m == 1)
    retval = a;
    return;
  end

  // Take first order difference to see so that wraps will show up
  // as large values, and the sign will show direction.
  //  idx = cell ();
  idx = list();
  for i = 1:nd
    idx(i) = 1:sz(i);
  end
  //  idx{dim} = [1,1:m-1];
  idx(dim) =  [1,1:m-1];
  
  //  d= a(idx{:})-a;
  d = a(idx(1),idx(2))-a;
  //d = a(:, [1,1:m-1])-a;


  // Find only the peaks, and multiply them by the range so that there
  // are kronecker deltas at each wrap point multiplied by the range
  // value.
  //p =  rng * (((d > tol) > 0) - ((d < -tol) > 0));
  p =  rng * ((bool2s(d > tol)) - (bool2s(d < -tol) ));
  // Now need to "integrate" this so that the deltas become steps.
  r = cumsum (p, dim);

  // Now add the "steps" to the original data and put output in the
  // same shape as originally.
  retval = a + r;

endfunction
