function som = integ(y,x) 
//	Approximate integral.
// Calling Sequence
//	SOM=INTEG(Y,X) 
// Parameters
//	Y   : N-row-vector (or MxN-matrix) to be integrated    (along each row).  
//	X   : N-row-vector containing the integration path of Y	(default : 1:N)
//	SOM : value (or Mx1 vector) of the integral
//  Description
//       integ approximates the integral of vector Y	according to X.
//  Examples  
//       Y = altes(256,0.1,0.45,10000)'; X = (0:255);
//       SOM = integ(Y,X)
//  See also
//      integ2d
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, October 95
//	Copyright (c) 1995 Rice University

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="integ";
  if nargin <1 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,2));
  end;
  
  //y
  if type(y)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(y)==1) then
    y=matrix(y,1,-1);
  end
  N=size(y,2);

  //x
  if nargin==2 then
    if type(x)<>1|and(size(x)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A vector of double expected.\n"),fname,2));
    end
    if size(x,"*")<>N then
      error(msprintf(_("%s: Wrong size for input argument %d: A vector of size %d expected.\n"),fname,2,N));
    end
    x=matrix(x,-1,1);
    som=(y(:,1:N-1) + y(:,2:N))*diff(x)/2;
  else
    som=sum(y(:,1:N-1) + y(:,2:N),2)/2;
  end
endfunction
