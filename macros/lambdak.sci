function y=lambdak(u,k);
// Evaluate lambda function for Affine Wigner distribution.
// Calling Sequence
//	Y=lambdak(U,K) 
// Parameters
//	U : real vector
//	Y : value of LAMBDAD at point(s) U
// Description
//      lambdak evaluates the parametrization lambda function
//	involved in the affine smoothed pseudo Bertrand distribution.
//	 lambdak(U,0) = -U/(exp(-U)-1) for K = 0
//	 lambdak(U,1) = exp(1+U exp(-U)/(exp(-U)-1)) for K = 1
//	 lambdak(U,K) = (K (exp(-U)-1)/(exp(-KU)-1))^(1/(K-1)) otherwise
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, October 95
//	Copyright (c) 1995 Rice University

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="lambdak"
  if nargin <>2 then
    error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),fname,2));
  end;
  
  //u
  if type(u)<>1|~isreal(u)|and(size(u)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
  end
  
  //k
  if type(k)<>1|size(k,"*")<>1|~isreal(k) then
    error(msprintf(_("%s: Wrong type for input argument #%d: An real value expected.\n"),fname,2));
  end

  y = ones(u) ;  
  ind = find(u ~= 0) ;
  if ind<>[] then
    if k==0 then
      y(ind) = -u(ind)./(exp(-u(ind))-1) ;
    elseif k==1 then
      y(ind) = exp(1+u(ind).*exp(-u(ind))./(exp(-u(ind))-1)) ;
    else
      y(ind) = (k*(exp(-u(ind))-1)./(exp(-k*u(ind))-1)).^(1/(k-1)) ;
    end; 
  end
endfunction
