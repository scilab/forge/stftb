function [tm,T]=loctime(sig);
// Time localization caracteristics.
// Calling Sequence
//	[TM,T]=loctime(SIG)
//  Parameters
//	SIG is the signal.
//	TM  is the averaged time center.
//	T   is the time spreading.
//  Description
//       loctime computes the time localization	caracteristics of signal SIG. 
//  Examples
//       z=amgauss(160,80,50); [tm,T]=loctime(z)
//   See also
//       locfreq
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="loctime"
  if nargin <>1 then
    error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),fname,1));
  end;
  
  //sig
  if type(sig)<>1|and(size(sig)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A complex vector expected.\n"),fname,1));
  end
  sig=matrix(sig,1,-1)
  N=size(sig,2)

  sig=abs(sig).^2; 
  sig=sig/mean(sig);
  t=(1:N);
  tm=mean(t.*sig);
  T=2*sqrt(%pi*mean((t-tm).^2 .* sig)); 

endfunction
