function noise=noisecu(N);
// Analytic complex uniform white noise.
// Calling Sequence
//	NOISE=noisecu(N)
// Description
//  noisecu computes an analytic complex white uniform noise of length N with mean 0.0 and variance 1.0. 
//  Examples
//       N=512;noise=noisecu(N);mean(noise),std(noise).^2
//       subplot(211); plot(real(noise)); a=gca();a.data_bounds=([1 N -1.5 1.5]);
//       subplot(212); f=linspace(-0.5,0.5,N); 
//       plot(f,abs(fftshift(fft(noise))).^2);
//   See also 
//     noisecg
//   Authors
//      H. Nahrstaedt - Aug 2010
//	O. Lemoine, June 95/May 96 - F. Auger, August 95.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="noisecu" 
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),fname,1));
  end;
  
 if type(N)<>1|or(size(N)<>1)|(int(N)<>N|N <= 0),
    error (msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,1));
  end;

  if N<=2,
    noise=(rand(N,1)-0.5+%i*(rand(N,1)-0.5))*sqrt(6); 
  else
    noise=rand(2^nextpow2(N),1)-0.5;
    noise=hilbert(noise)/stdev(noise)/sqrt(2);
    noise=noise($-(N-1:-1:0));
  end
 
endfunction
