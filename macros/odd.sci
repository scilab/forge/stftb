function y=odd(x);
//	Round towards nearest odd value.
// Calling Sequence
//	Y=odd(X) 
// Parameters
//	X : scalar, vector or matrix to be rounded
//	Y : output scalar, vector or matrix containing only odd values
//  Description
//      odd rounds each element of X towards the nearest odd
//	integer value. If an element of X is even, ODD adds +1 to 
// 	this value. X can be a scalar, a vector or a matrix.
//  Examples
//      X=[1.3 2.08 -3.4 90.43]; Y=odd(X)
//   Authors
//      H. Nahrstaedt - Aug 2010
//	O. Lemoine - August 1996.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="odd" 
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),fname,1));
  end;
  
  if type(x)<>1|~isreal(x) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real matrix expected.\n"),fname,1));
  end

  y=floor(x);
  l=find(modulo(y,2)==0);
  if l<>[] then
    y(l)=ceil(x(l));
    l1=find(modulo(y(l),2)==0);
    if l1<>[] then
      y(l(l1))=y(l(l1))+1;
    end
  end
 
endfunction
