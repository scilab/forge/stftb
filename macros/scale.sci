function S=scale(X,a,fmin,fmax,N,ptrace);
//	Scale a signal using the Mellin transform.
// Calling Sequence
//	S=scale(X)
//	S=scale(X,A)
//	S=scale(X,A,FMIN,FMAX)
//	S=scale(X,A,FMIN,FMAX,N)
//	S=scale(X,A,FMIN,FMAX,N,TRACE)
// Parameters
//	X : signal in time to be scaled (Nx=length(X)).
//	A : scale factor. A < 1 corresponds to a compression in the time  domain. A can be a vector.(default : 2)
//	FMIN,FMAX : respectively lower and upper frequency bounds of    the analyzed signal. These parameters fix the equivalent   frequency bandwidth (expressed in Hz). When unspecified, you   have to enter them at the command line from the plot of the   spectrum. FMIN and FMAX must be >0 and <=0.5.
//	N : number of analyzed voices (default : automatically determined).
//	TRACE : if nonzero, the progression of the algorithm is shown     	(default : 0).
//	S : the A-scaled version of signal X. Length of S can be larger       than length of X if A > 1. If A is a vector of length L, S is   a matrix with L columns. S has the same energy as X.
//  Description
//    scale computes the A-scaled version of signal X : A^(-1/2) X(T/A) using its Mellin transform.
//  Examples
//       sig=klauder(128); S=scale(sig,2,.05,.45,128);
//       subplot(211); plot(sig); subplot(212); plot(real(S(65:192)));
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, October 1995 - O. Lemoine, June 1996. 
//	Copyright (c) Rice University - CNRS (France)

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="scale";
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,6));
  end;
  
  //X
  if type(X)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(X)==1) then
    X=X(:);
  end
  if size(X,2)>2 then
    error(msprintf(_("%s: Wrong size for input argument #%d: A vector or a two columns array expected.\n"),fname,1));
  end
  [Mt,xcol] = size(X);
  
  
   //ptrace
  if nargin>=6 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,6));
    end
  else
    ptrace=%f;
  end

  //N
  if nargin>=5 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,5));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,5));
    end
  else
    N=[]
  end
  
   
    //fmax
  if nargin>=4 then
     if type(fmax)<>1|size(fmax,"*")>1|~isreal(fmax) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,4));
     end 
     if fmax<=0|fmax>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,4,"]0,0.5]"));
     end
  else
    fmax=[]
  end
  
  //fmin
  if nargin>=3 then
     if type(fmin)<>1|size(fmin,"*")>1|~isreal(fmin) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
     end 
     if fmin<=0|fmin>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"]0,0.5]"));
     end
     if fmin>fmax then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be < %g.\n"),fname,3,fmax));
     end
  else
    fmin=[]
  end
  
  // a
  if nargin>=2 then
    if type(a)<>1|~isreal(a)|and(size(a)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
  else
    a=2;
  end



  Z = hilbert(real(X));
  T = Mt;
  M = (Mt+rem(Mt,2))/2;

  if fmin==[]|fmax==[] then // fmin,fmax unspecified
  	        
    STF = fft(fftshift(Z)); Nstf=length(STF);
    sp = (abs(STF(1:Nstf/2))).^2; Maxsp=max(sp);
    f = linspace(0,0.5,Nstf/2+1) ; f = f(1:Nstf/2);
    plot(f,sp) ; xgrid;
    xlabel(_('Normalized frequency'));
    title(_('Analyzed signal energy spectrum'));
    if fmin==[] then
      indmin=min(find(sp>Maxsp/1000));
      fmindflt=max([0.01 0.05*fix(f(indmin)/0.05)]);
      txtmin=msprintf(_('Lower frequency bound [%s] :'),string(fmindflt));
      while  %t then
        fmin = input(txtmin); 
        if fmin==[] then 
          fmin=fmindflt; break
        elseif fmin>0&fmin<=0.5 then
          break
        else
          mprintf(_("%s: Wrong given value for lower frequency bound: Must be in the interval %s.\n"),fname,"]0,0.5]"); 
        end
      end
    end
    if fmax==[] then
      indmax=max(find(sp>Maxsp/1000));
      fmaxdflt=0.05*ceil(f(indmax)/0.05);
      txtmax=msprintf(_('Upper frequency bound [%s] :'),string(fmaxdflt));
      while  %t then
        fmax = input(txtmax); 
        if fmax==[] then 
          fmax=fmaxdflt; break
        elseif fmax>0&fmax<=0.5&fmax>fmin then
          break
        else
          mprintf(_("%s: Wrong given value for upper frequency bound: Must be in the interval %s and greater than fmin\n"),fname,"]0,0.5]"); 
        end
      end  
    end
  end


  B = fmax-fmin ; 
  R = B/((fmin+fmax)/2) ; 

  Nq= ceil((B*T*(1+2/R)*log((1+R/2)/(1-R/2)))/2);
  Nmin = Nq-rem(Nq,2);
  
 
  if N==[] then
    Ndflt = 2^nextpow2(Nmin);
    Ntxt=msprintf(_('Number of frequency samples (>=%d) [%d] : '),Nmin,Ndflt);
    while  %t then
      N=input(Ntxt); 
      if N==[] then 
        N=Ndflt;break;
      elseif N>=Nmin&int(N)==N then
        break
      else
        mprintf(_("%s: Wrong given value for number of frequency samples: a positive integer expected.\n"),fname);  
      end
    end

  else
    if (N<Nmin) then
      warning(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d expected.\n"),fname,5,Nmin));  
    end
  end


  // Geometric sampling of the analyzed spectrum
  k = 1:N;
  q = (fmax/fmin)^(1/(N-1));
  geo_f  = fmin*(exp((k-1).*log(q)));
  t = (1:Mt)-M-1;
  tfmatx = zeros(Mt,N);
  tfmatx = exp(-2*%i*t'*geo_f*%pi);
  ZS = Z.'*tfmatx; 
  ZS(N+1:2*N) = zeros(1,N);


  // Mellin transform computation of the analyzed signal
  p    = 0:(2*N-1);
  MS   = fftshift(ifft(ZS));
  beta0 = (p/N-1)./(2*log(q));


  // Inverse Mellin transform and inverse Fourier transform
  Mmax = max(ceil(Mt/2*a));
  S    = zeros(2*Mmax,length(a)); 
  ptr  = 1;
  for acurrent = a,
    if ptrace, disprog(ptr,length(a),10); end
    DMS = exp(-2*%i*%pi*beta0*log(acurrent)).*MS;	// Scaling in Mellin domain
    DS  = fft(fftshift(DMS));			// Inverse Mellin transform
    Mcurrent = ceil(acurrent*Mt/2);
    t = [-Mcurrent:Mcurrent-1]-1;
    itfmatx    = zeros(2*Mcurrent,N);
    itfmatx    = exp(2*%i*t'*geo_f*%pi);		
    dilate_sig = zeros(2*Mcurrent,1);
    for kk=1:2*Mcurrent,
      dilate_sig(kk) = integ(itfmatx(kk,:).*DS(1:N),geo_f) ;
    end;
    S(Mmax-Mcurrent+1:Mmax+Mcurrent,ptr) = dilate_sig;
    ptr=ptr+1;
  end

  S=S*norm(X)/norm(S);
endfunction
