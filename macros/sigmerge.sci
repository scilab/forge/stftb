function sig=sigmerge(x1,x2,ratio);
//   Add two signals with given energy ratio in dB.
// Calling Sequence
//	SIG=sigmerge(X1,X2) 
//	SIG=sigmerge(X1,X2,RATIO) 
//  Parameters
//	X1, X2 : input signals.
//	RATIO  : Energy ratio in deciBels	(default : 0 dB).
//	X      : output signal.
//	X= X1+H*X2, such that 10*log(Energy(X1)/Energy(H*X2))=RATIO
//  Description
//    sigmerge adds two signals so that a given energy ratio expressed in deciBels is satisfied.
//  Examples
//       sig=fmlin(64,0.01,0.05,1); noise=hilbert(randn(64,1));
//       SNR=15; x=sigmerge(sig,noise,SNR);
//       Esig=mean(abs(sig).^2); Enoise=mean(abs(x-sig).^2);
//       10*log10(Esig/Enoise)
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="sigmerge";
  if nargin <2 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,2,3));
  end;
 
  //x1
  if type(x1)<>1|and(size(x1)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real or complex vector expected.\n"),fname,1));
  end
  
  
  //x2
  if type(x2)<>1|and(size(x2)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real or complex vector expected.\n"),fname,2));
  end
  
  if or(size(x1)<>size(x2)) then
     error(msprintf(_("%s: Incompatible input arguments #%d and #%d: Same sizes expected.\n"),fname,1,2));
  end
  
  //ratio
  if nargin==3 then
    if type(ratio)<>1|~isreal(ratio)|or(size(ratio)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,3));
    end
  else
    ratio=0
  end

  if ratio==%inf then
    sig = x1;
  else
   Ex1=mean(abs(x1).^2);
   Ex2=mean(abs(x2).^2);
   h=sqrt(Ex1/(Ex2*10^(ratio/10)));
   sig=x1+h*x2;
  end

endfunction
