function y=mtlb_filter(b,a,x);

H = poly(b, 'z') / poly(a, 'z');
y = flts(x, H);

endfunction
