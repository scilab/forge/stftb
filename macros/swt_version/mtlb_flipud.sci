function y = mtlb_flipud(x)
//  Copyright Aldo I Maalouf

if ndims(x)~=2, 
disp('X must be a 2-D matrix!')
end
[m,n] = size(x);
y = x($:-1:1,:);
endfunction
