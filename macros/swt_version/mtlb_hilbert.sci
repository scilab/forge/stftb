function h=mtlb_hilbert(x);

[xrow,xcol]=size(x);
if (xrow~=1),
x = x';
end;

N=nextpow2(length(x));
if (2^N~=length(x)),
x=[x zeros(1,2^N-length(x))];
end;

n = 2^N;

y=mtlb_fft(x);
wi=[2*ones(1,n/2), zeros(1,n/2)];
wi(1) = 1;
wi(n/2+1) = 1;

y=y.*wi;
y=mtlb_ifft(y);
h=y(1:length(x));

endfunction
