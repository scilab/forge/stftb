function [x,t] = tfristft(tfr,t,h,ptrace);
// Inverse Short time Fourier transform.
// Calling Sequence
//	[X,T]=tfristft(tfr,T,H) 
//	[X,T]=tfristft(tfr,T,H,TRACE) 
//  Parameters
//	X     : signal.
//	T     : time instant(s)          (default : 1:length(X)).
//	H     : frequency smoothing window, H being normalized so as to    be  of unit energy.      (default : Hamming(N/4)). 
//	TRACE : if nonzero, the progression of the algorithm is shown      (default : 0).
//	TFR   : time-frequency decomposition (complex values). The    frequency axis is graduated from -0.5 to 0.5.
// Description
//      tfristft  computes the inverse short-time 	Fourier transform of a discrete-time signal X. This function
//	may be used for time-frequency synthesis of signals.
//    Examples 
//        t=200+(-128:127); sig=[fmconst(200,0.2);fmconst(200,0.4)]; 
//        h=hamming(57); tfr=tfrstft(sig,t,256,h,1);
//        sigsyn=tfristft(tfr,t,h,1);
//        plot(t,abs(sigsyn-sig(t)))
//     Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, November 1996.

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfristft";
  
  if nargin <3 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,3,4));
  end

  //tfr
  if type(tfr)<>1 then 
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [N,NbPoints]=size(tfr);
  
  //t
  if type(t)<>1|and(size(t)>1)|~isreal(t)|or(int(t)<>t) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A vector of integer values expected.\n"),fname,2));
  end
  t=matrix(t,1,-1)
  tcol=size(t,2)
  if tcol<>NbPoints then
    error(msprintf(_("%s: Wrong size for input argument #%d: %d expected.\n"),fname,2,NbPoints));
  end
  if or(diff(t)<>1) then
    error(msprintf(_("%s: Wrong value for input argument #%d: The increments between elements must be equal to 1.\n"),fname,2));
  end
  
  //h
  if type(h)<>1|and(size(h)>1)|~isreal(h) then 
    error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,3));
  end
  h=h(:)
  hrow=size(h,1);
  Lh=(hrow-1)/2; 
  if (modulo(hrow,2)==0),
    error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,3));
  end;
  h=h/norm(h);

  //ptrace
  if nargin>=4 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,4));
    end
  else
    ptrace=%f;
  end


  if ptrace, disp('Inverse Short-time Fourier transform'); end;

  tfr=fft(tfr,-1,1); 


  x=zeros(tcol,1);

  for icol=1:tcol,
    if ptrace, disprog(icol,tcol,10); end;
    valuestj=max([1,icol-N/2,icol-Lh]):min([tcol,icol+N/2,icol+Lh]);
    for tj=valuestj,
      tau=icol-tj; 
      indices= rem(N+tau,N)+1; 
      x(icol,1)=x(icol,1)+tfr(indices,tj)*h(Lh+1+tau);
    end;
    x(icol,1)=x(icol,1)/sum(abs(h(Lh+1+icol-valuestj)).^2);
  end;

  if ptrace, printf('\n'); end;
endfunction
