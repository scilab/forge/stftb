function [tfr,t,f] = tfrparam(x,t,N,p,L,ptrace,Rxtype,method,q,opt_plot);
//parametric time-frequency representation.
// Calling Sequence
// [tfr,t,f]=tfrparam(x,t,N,p,L,ptrace,Rxtype,method,q) 
// Parameters
// X      : signal
// T      : time instant(s)          (default : 1:length(X)).
// N      : number of frequency bins (default : max(256,length(X)) ).
// p      : last autocorrelation lag (default : 2 ).
// L      : length of the window around the analyzed time sample. Must be odd.         (default : max(51,round(xrow/10)) ).
// Rxtype : choice of the correlation matrix algorithm (default : 'fbhermitian')   possible values are 'hermitian', 'fbhermitian', 'burg' or 'fbburg'
// method : can be either 'ar', 'periodogram', 'capon', 'capnorm', 'lagunas',   or 'genlag'.
// q      : parameter for the generalized Lagunas method.
//      'plot':	if one input parameter is 'plot',  tfrparam runs tfrqview. and TFR will be plotted
// Examples
//          x=fmconst(256,0.1)+fmlin(256,0.15,0.30)+fmlin(256,0.2,0.45);
//          scf(1); tfrparam(x,1:256,256,3,21,1,'fbhermitian','ar','plot');
//          scf(2); tfrparam(x,1:256,256,15,61,1,'fbhermitian','periodogram','plot');
//          scf(3); tfrparam(x,1:256,256,3,21,1,'fbhermitian','capon','plot');
//          scf(4); tfrparam(x,1:256,256,3,21,1,'fbhermitian','lagunas','plot');
//     Authors
//      H. Nahrstaedt - Aug 2010
// F. Auger, july, november 1998, april 99.


//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  [nargout,nargin]=argn(0);
  fname="tfrparam";
 
  in_par=['x','t','N','p','L','ptrace','Rxtype','method','q','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;  
  clear in_par;
  
  if nargin <1 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,10));
  end
  
  //x
   if type(x)<>1|and(size(x)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A vector of double expected.\n"),fname,1));
  end
  x=x(:);
  [xrow] = size(x,1);
  
  //q
  if nargin>=9 then
    //? see parafrep GENLAG
  else
    q=0
  end
  
  //method
  if nargin>=8 then
    if type(method)<>10|size(method,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A character string expected.\n"),fname,8));
    end
    method=convstr(method,"u")
    M=["AR" "PERIODOGRAM" "CAPON" "CAPNORM" "LAGUNAS" "GENLAG"]
    if and(method<>M) then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set  {%s}.\n"),fname,8,"""AR"",""PERIODOGRAM"",""CAPON"",""CAPNORM"",""LAGUNAS"",""GENLAG"""));
    end
  else
    method='AR';
  end
  
  //Rxtype
  if nargin>=7 then
    if type(Rxtype)<>10|size(Rxtype,"*")<>1 then
        error(msprintf(_("%s: Wrong type for input argument #%d: A character string  or an empty matrix expected.\n"),fname,7));
    end 
    Rxtype=convstr(Rxtype,"l")
    if and(Rxtype<>["hermitian","fbhermitian","burg","fbburg"]) then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),fname,7,"""hermitian"",""fbhermitian"",""burg"",""fbburg"""));
    end
  else
     Rxtype='fbhermitian';
  end
  
  //ptrace
  if nargin>=6 then
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,6));
    end
  else
    ptrace=%f
  end
  
  //L
  if nargin>=5 then
    if type(L)<>1|size(L,"*")>1|~isreal(L)|int(L)<>L then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,5));
    end 
    if (modulo(L,2)==0) then 
       error(msprintf(_("%s: Wrong vale for argument #%d: A odd number expected.\n"),fname,5));
    end 
  else
    L=max(51,round(xrow/10))
    if modulo(L,2)==0 then L=L+1;end
  end
  Lhalf=(L-1)/2;
  
  //p
  if nargin>=4 then
    if type(p)<>1|~isreal(p)|size(p,"*")<>1|int(p)<>p then
      error(msprintf(_("%s: Wrong type for input argument #%d: An integer expected.\n"),fname,4));
    end
    if p<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d].\n"),fname,4,1));
    end;
  else
    p=2
  end
  
  //N
  if nargin >=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end 
  else
   N=max(256,xrow); 
  end
  
  //t
  if nargin>=2 then
    if type(t)<>1|and(size(t)<>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>xrow|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(xrow)+"]"));
    end
    t=matrix(t,1,-1)
  else
    t=1:xrow; 
  end
  tcol = size(t,2);
  
  tfr= zeros (N,tcol) ;  
  if ptrace, disp("tfrparam"); end;
  for icol=1:tcol,
    ti= t(icol);
    timin=max(ti-Lhalf,1)
    timax=min(ti+Lhalf,xrow)
    
    if ptrace, disprog(icol,tcol,10); end;
    Realp=min (p,timax-timin); 
    Rx=correlmx(x(timin:timax),Realp,Rxtype);
    [tfr(:,icol),f]=parafrep(Rx,N,method);
  end;
  if ptrace, printf('\n'); end;

  if (plotting),
    grayplot(t,f,10.0*log10(tfr)); //axis('xy'); 
    title("tfrparam "+method+", p="+string(p)+", L="+string(L)+", "+Rxtype+" autocorrelation matrix");
  end;

endfunction

