function tfrparams(method)
  if type(method)<>10|or(size(method)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A character string expected.\n"),fname,1));
  end
  method=convstr(method,"l");
  select method
  case "tfrbert"
    p=["t","fmin","fmax","n"]
    m=["time instant(s) on which the TFR is evaluated"
       "lower frequency bounds"
       "upper frequency bounds"
       "number of analyzed voices"];
  case "tfrbj"
    p=["t","n","g","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"];
  case "tfrbud"
    p=["t","n","g","h","sigma"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"
       "kernel width"];
  case "tfrcw"
    p=["t","n","g","h","sigma"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"
       "kernel width"];
  case "tfrdfla"
    p=["t","fmin","fmax","n"]
    m=["time instant(s) on which the TFR is evaluated"
       "lower frequency bounds"
       "upper frequency bounds"
       "number of analyzed voices"];
  case "tfrgabor"
    p=["n","q","h"]
    m=["number of Gabor coefficients in time"
       "degree of oversampling"
       "synthesis window"];
  case "tfrgrd"
    p=["t","n","g","h","rs","MoverN"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"
       "kernel width"
       "dissymmetry ratio"];
  case "tfrmh"
    p=["t","n"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"]
  case "tfrmhs"
    p=["t","n","g","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"]
  case "tfrmmce"
    p=["h","t","n"]
    m=[ "frequency smoothing window" 
        "time instant(s) on which the TFR is evaluated"
        "number of frequency bins"]
  case "tfrpage"
    p=["t","n"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"]
  case "tfrpmh"
    p=["t","n","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "frequency smoothing window"]
  case "tfrppage"
    p=["t","n","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "frequency smoothing window"]
  case "tfrpwv"
    p=["t","n","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "frequency smoothing window"]
    
  case "tfrrgab"
    p=["t","n","nh","k"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "frequency smoothing window"
       "length of the gaussian window"
       "value at both extremities"]
  case "tfrridbn"
    p=["t","n","g","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"];
  case "tfrridb"
    p=["t","n","g","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"];
  case "tfrridh"
    p=["t","n","g","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"];
  case "tfrridt"
    p=["t","n","g","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"];
  case "tfrmh"
    p=["t","n"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"]
    
  case "tfrrmsc"
    p=["t","n","f0t"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time-bandwidth product of the mother wavelet"]
  case "tfrpmh"
    p=["t","n","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "frequency smoothing window"]
  case "tfrrpmh"
    p=["t","n","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "frequency smoothing window"]
  case "tfrrpwv"
    p=["t","n","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "frequency smoothing window"]
  case "tfrsp"
    p=["t","n","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "frequency smoothing window"]
    
  case "tfrrsp"
    p=["t","n","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "frequency smoothing window"]
    
  case "tfrspwv"
    p=["t","n","g","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"];
    
  case "tfrrspwv"
    p=["t","n","g","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"];
  case "type1"
    p=[]

    
  case "tfrstft"
    p=["t","n","g","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"];
  case "tfrunter"
    p=["t","form","fmin","fmax","n"]
    
    m=["time instant(s) on which the TFR is evaluated"
       "active or passive unterberger distribution selector"
       "lower frequency bounds"
       "upper frequency bounds"
       "number of analyzed voices"];
  case "tfrwv"
    p=["t","n"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"]
  case "tfrzam"
    p=["t","n","g","h"]
    m=["time instant(s) on which the TFR is evaluated"
       "number of frequency bins"
       "time smoothing window"
       "frequency smoothing window"];

  else
    error(msprintf(_("%s: Wrong value for input argument #%d: Unknown method.\n"),fname,1));
  end
  mprintf("%s: %s\n",p',m)
endfunction
