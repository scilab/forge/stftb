function tfrqview(tfr,sig,t,method,p1,p2,p3,p4,p5);
// Quick visualization of time-frequency representations.
// Calling Sequence
//       tfrqview(TFR,SIG,T,METHOD,P1,P2,P3,P4,P5) 
// Parameters
//       TFR     : time-frequency representation (MxN).
//       SIG     : signal in time. If unavailable, put sig=[] as input parameter. (default : []).
//       T       : time instants                 (default : 1:N).
//       METHOD  : name of chosen representation (default : 'TYPE1'). 
//                 tfr*  :  See the help for authorized names. 
//                 TYPE1 : the representation TFR goes in normalized frequency from -0.5 to 0.5 ; 
//                 TYPE2 : the representation TFR goes in normalized frequency from 0 to 0.5. 
//       P1...P5 : optional parameters of the representation : run the file tfrparams(METHOD) to know the meaning of P1..P5 for your method. 
//  Description
//  tfrqview allows a quick visualization of a time-frequency representation.
//	When you use the 'save' option in the main menu, you save all your
//	variables as well as two strings, TfrQView and TfrView, in a mat 
//	file. If you load this file and do eval(TfrQView), you will restart
//	the display session under tfrqview ; if you do eval(TfrView), you
//	will obtain the exact layout of the screen you had when clicking on 
//	the 'save' button. 
//   
//       Examples 
//        sig=fmsin(128); tfr=tfrwv(sig);
//        tfrqview(tfr,sig,1:128,'tfrwv');
//       See also 
//        tfrview
//        tfrsave
//        tfrparam
//      Authors
//       H. Nahrstaedt - Aug 2010
//       F. Auger, September 1994, July 1995 
//       O. Lemoine, Oct 1995, May-July 1996.
//       F. Auger, May 1998.
//       Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 [nargout,nargin]=argn(0);
 fname="tfrqview"
 if nargin <1 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,9));
  end;

  //tfr
  if type(tfr)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [tfrrow,tfrcol] = size(tfr);
  
  
  //sig
  if nargin>=2 then
    if type(sig)<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
    end
    if or(size(sig)==1) then
      x=x(:);
    elseif size(x,2)>2 then
      error(msprintf(_("%s: Wrong size for input argument #%d: A  vector or a two columns array expected.\n"),fname,1));
    end
  else
    sig=[]
  end
  [Nsig,Ncol]=size(sig);
  
  //t
  if nargin>=3 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,3));
    end
    if size(t,"*")<>tfrcol then
      error(msprintf(_("%s: Wrong size for input argument #%d: %d expected.\n"),fname,3,tfrcol));
    end
    t=matrix(t,1,-1);
  else
    t=1:tfrcol;
  end
  [trow,tcol] = size(t);

  //method
  if nargin>=4 then
    method=convstr(method,"u");
  else
    method='TYPE1';// empty signal
  end
 

  // Computation of Nf2, the number of interresting points in frequency

  if istfr2(method),
    Nf2=tfrrow;
  elseif istfr1(method),
    Nf2=tfrrow/2; 
  else
    error('Unknown representation. Use type1 or type2');
  end;

  // Computation of freq (vector of frequency samples) 
  if istfraff(method),
    frq=evstr(['p'+string(nargin-4)]);   	// last input argument is freqs.
  else
    frq=(0.5*(0:Nf2-1)/Nf2);
  end

  // Initialization of the variables
  if isfile("tfrq_options.dat"),
    load "tfrq_options.dat";
    //colormap(SavedColorMap);
    f = gcf(); f.color_map = SavedColorMap;

  else
    threshold=5.0;  // visualization threshold
    linlogtfr=0;    // tfr visualization scale : 0 for linear 1 for logarithmic
    linlogspec=1;   // spectrum visualization scale
    sigenveloppe=0; // signal enveloppe visualization

    levelnumb=64;   // number of levels in the contour plot
    colmap=2;       // colormap index
    
    display=2;      // display index
    
    isgridsig=0;    // grid on signal
    isgridspec=0;   // grid on spectrum
    isgridtfr=0;    // grid on tfr
    
    issig=0;        // display signal
    isspec=0;       // display spectrum
    iscolorbar=0;   // display colorbar

    fs=1.0;         // sampling frequency (Hz)
    fmin=0.0;       // smallest displayed frequency 
    fmax=0.5*fs;    // highest displayed frequency
  end;

  f = gcf();f.color_map =jetcolormap((levelnumb));
  // Test of analycity
  if ~isempty(sig),
    for k=1:Ncol,
      // spec(:,k)=abs(fft(sig(min(t):max(t),k))).^2; Nsp=length(spec);
      // modifications :  F. Auger (fog), 30/11/97
      Lt_fog=max(t)-min(t)+1;   
      Nb_tranches_fog = floor(Lt_fog/tfrrow);
      // printf('%f \n',Nb_tranches_fog);
      spc(:,k)=zeros(tfrrow,1);
      for Num_tranche_fog=0:Nb_tranches_fog-1,
        // printf('%f \n',Num_tranche_fog);
        spc(:,k)=spc(:,k)+abs(fft(sig(min(t)+tfrrow*Num_tranche_fog+(0:tfrrow-1),k))).^2;
      end;

      if (Lt_fog>Nb_tranches_fog*tfrrow),
        spectre_fog=tftb_fft(sig(min(t)+tfrrow*Nb_tranches_fog:max(t),k),tfrrow);
        spectre_fog=spectre_fog(:);
        spc(:,k)=spc(:,k)+abs(spectre_fog).^2;
      end;

      
      // spec1=sum(spec(1:tfrrow/2,k));
      // spec2=sum(spec(tfrrow/2+1:Nsp,k));
      spec1=sum(spc(1:tfrrow/2,k));
      spec2=sum(spc(tfrrow/2+1:tfrrow,k));
      
      if spec2>spec1/10,
        disp('Be careful : the signal is not analytic!');
      end
    end
  end

  // Test of reality
  if (Ncol==2 & ~isreal(tfr)),
    disp('Cross distribution. As the result is complex, we display the real part.');
    tfr=real(tfr);
  end

  ChoiceDisplay     =  1; // All the possible values of the choice variable
  ChoiceLayout      =  2;
  ChoiceSampling    =  3;
  ChoiceFreqBounds  =  4;
  ChoiceThreshold   =  5;
  ChoiceLinlog      =  6;
  ChoiceRedraw      =  7;
  ChoiceNewFigure   =  8;
  ChoiceSaveResults =  9;
  ChoiceSaveOptions = 10;
  ChoicePrint       = 11;
  ChoiceClose       = 0;

  CallTfrView = 1; // 1 to call tfrview, 0 not to do it
  RefreshFigure=1; // 1 to refresh figure every time, 0 to freeze

  choice=ChoiceSampling;
  while choice~=ChoiceClose,                       // while not close
    if RefreshFigure & CallTfrView,                 // Call to tfrview
      linlog=linlogtfr+2*linlogspec+4*sigenveloppe;
      isgrid=isgridsig+2*isgridspec+4*isgridtfr;
      layout=issig+isspec*2+iscolorbar*4+1;
      param = [display, linlog, threshold, levelnumb, Nf2, layout,...
               fs, isgrid, fmin, fmax];
      if (nargin<=4),
        tfrview(tfr,sig,t,method,param);
      elseif (nargin==5),
        tfrview(tfr,sig,t,method,param,p1);
      elseif (nargin==6),
        tfrview(tfr,sig,t,method,param,p1,p2);
      elseif (nargin==7),
        tfrview(tfr,sig,t,method,param,p1,p2,p3);
      elseif (nargin==8),
        tfrview(tfr,sig,t,method,param,p1,p2,p3,p4);
      elseif (nargin==9),
        tfrview(tfr,sig,t,method,param,p1,p2,p3,p4,p5);
      end;
    end;

    if (linlogtfr==0),                              // Lin/log scale of the tfr
      linlogstr='Change to a logarithmic scale';
    else
      linlogstr='Change to a linear scale';
    end;

    if (RefreshFigure==1),
      redrawstr='Don''t redraw yet';
    else
      redrawstr='Redraw now';
    end;

    // Main menu

    choice=x_choose(['Change the display mode',...       // ChoiceDisplay
                     'Change the display layout',...     // ChoiceLayout
                     'Change the sampling frequency',... // ChoiceSampling
                     'Change the frequency bounds',...   // ChoiceFreqBounds
                     'Change the threshold',...          // ChoiceThreshold
                     linlogstr,...                       // ChoiceLinlog
                     redrawstr,...                       // ChoiceRedraw
                     'New figure',...                    // ChoiceNewFigure
                     'Save results',...                  // ChoiceSaveResults
                     'Save options',...                  // ChoiceSaveOptions
                     'Print'...                         // ChoicePrint
                    ],['TFRQVIEW MENU :'],'Close');                           // ChoiceClose
    
    if (choice==ChoiceDisplay),                      // Change the display mode

      OldDisplay=display;
      display=x_choose(    ['contour2d',...   // 1
                    'grayplot',...                      // 2
                    'Sgrayplot',...                     // 3
                    'surf',...                         // 4
                    'mesh',...                         // 5
                    'change the color map',...         // 6
                    'change the number of colors or levels'],...  // 7
                           ['DISPLAY MODE :'],'cancel');

      if (display>=1)&(display<=5),
        CallTfrView=1;
      elseif (display==6),
        colmap=x_choose(['hsv','jet','cool','bone','gray','hot','ocean',...
                         'pink','rainbow','autumn','winter','spring','summer',...
                         'white','copper','permute'],['COLOR MAP :'],'Return');
        f = gcf();
        if     colmap== 1,  f.color_map =hsvcolormap((levelnumb));
        elseif colmap== 2,  f.color_map =jetcolormap((levelnumb));
        elseif colmap== 3,  f.color_map =coolcolormap((levelnumb));
        elseif colmap== 4,  f.color_map =bonecolormap((levelnumb));
        elseif colmap== 5,  f.color_map =graycolormap((levelnumb));
        elseif colmap== 6,  f.color_map =hotcolormap((levelnumb));
        elseif colmap== 7,  f.color_map =oceancolormap((levelnumb));
        elseif colmap== 8,  f.color_map =pinkcolormap((levelnumb));
        elseif colmap== 9,  f.color_map =rainbowcolormap((levelnumb));
        elseif colmap==10,  f.color_map =autumncolormap((levelnumb));
        elseif colmap==11,  f.color_map =wintercolormap((levelnumb));
        elseif colmap==12,  f.color_map =springcolormap((levelnumb));
        elseif colmap==13,  f.color_map =summercolormap((levelnumb));
        elseif colmap==14,  f.color_map =whitecolormap((levelnumb));
        elseif colmap==15,  f.color_map =coppercolormap((levelnumb));
          //elseif colmap==14,  brighten(+0.20);
          //elseif colmap==15,  brighten(-0.10);
        elseif colmap==16,  MyMap = get(gcf(),"color_map"); f.color_map=((MyMap($:-1:1,:)));
          //elseif colmap==17,  spinmap; //'spin',
        end
        
        display=OldDisplay; CallTfrView=0;

      elseif (display==7),
        printf(' Old number of levels: %f\n',levelnumb); levelold=levelnumb;
        levelnumb=input(' New number of levels: ');
        if isempty(levelnumb),
          levelnumb=levelold; CallTfrView=0;
        else
          if levelnumb<levelold,
            CallTfrView=1;
            MyMap = get(gcf(),"color_map"); MyMap=MyMap(1:levelnumb,:); f = gcf(); f.color_map=(MyMap);
          elseif levelnumb>levelold,
            CallTfrView=1;
            MyMap = ones(levelnumb, 3); MyMap(1:levelold,:)=get(gcf(),"color_map"); 
            printf('warning : the colormap size has been increased by identical vectors\n');
            printf('You should redefine the colormap\n');
          else
            CallTfrView=0;
          end
        end
        display=OldDisplay; 

      elseif (display==0),
        display=OldDisplay; CallTfrView=0;
      end;

    elseif (choice==ChoiceLayout),                 // Change the display layout
      
      layout=1;
      if issig==0, 
        SignalStr= 'display signal';
      else
        SignalStr='remove signal';
      end;
      
      if isspec==0, 
        SpectrumStr= 'display spectrum';
      else
        SpectrumStr='remove spectrum';
      end;

      if ~issig & ~isspec,
        if isgridtfr,
          GridStr='Remove the grid';
        else
          GridStr='Add a grid';
        end;
      else
        GridStr='Grids';
      end;
      
      if iscolorbar==0,
        ColorbarStr='display colorbar';
      else
        ColorbarStr='remove colorbar';
      end;
      
      layout=x_choose(  [SignalStr,...
                         SpectrumStr,...
                         GridStr,...
                         ColorbarStr],...
                        'DISPLAY LAYOUT','Return');
      
      if layout==1,
        issig=sum(~issig);
        if issig==1, 
          if isempty(sig),
            disp('Impossible action : the signal is unavailable'); issig=0; CallTfrView=0;
          else
            sigenveloppe=x_choose(['signal only','signal with enveloppe'],'SIGNAL REPRESENTATION','Return')-1;
            CallTfrView=1;
          end;
        else
          isgridsig=0;
        end; 
      elseif layout==2,   
        isspec=sum(~isspec);
        if isspec==1,
          if isempty(sig),
            disp('Impossible action : the signal is unavailable'); isspec=0; CallTfrView=0;
          else    
            linlogspec=x_choose(['linear scale','log scale'],'FREQUENCY REPRESENTATION')-1;
            CallTfrView=1;
          end;
        else
          isgridspec=0;
        end;

      elseif layout==3,

        if ~issig & ~isspec,	                 // No signal and no spectrum
          isgridtfr=1-isgridtfr; 
          CallTfrView=1;  

        elseif issig & ~isspec,               // A signal, no spectrum 
          Grid=1;
          if ~isgridsig,
            gridsigstr='add a grid on the signal';
          else
            gridsigstr='remove the grid on the signal';
          end

          if ~isgridtfr,
            gridtfrstr='add a grid on the TFR';
          else
            gridtfrstr='remove the grid on the TFR';
          end

          Grid=x_choose([gridsigstr,gridtfrstr],'GRID MENU :');
          if Grid==1,
            isgridsig=1-isgridsig; CallTfrView=1;
          elseif Grid==2,
            isgridtfr=1-isgridtfr; CallTfrView=1;
          else
            CallTfrView=0;
          end

        elseif ~issig & isspec,               // No signal, a spectrum
          Grid=1;
          if ~isgridspec,
            gridspestr='add a grid on the spectrum';
          else
            gridspestr='remove the grid on the spectrum';
          end
          if ~isgridtfr,
            gridtfrstr='add a grid on the TFR';
          else
            gridtfrstr='remove the grid on the TFR';
          end
          Grid=x_choose([gridspestr,gridtfrstr],'GRID MENU :');
          if Grid==1,
            isgridspec=1-isgridspec; CallTfrView=1;
          elseif Grid==2,
            isgridtfr=1-isgridtfr; CallTfrView=1;
          else CallTfrView=0;
          end

        else                                  // A signal and a spectrum
          Grid=1;
          if ~isgridsig,
            gridsigstr='add a grid on the signal';
          else
            gridsigstr='remove the grid on the signal';
          end
          if ~isgridspec,
            gridspestr='add a grid on the spectrum';
          else
            gridspestr='remove the grid on the spectrum';
          end
          if ~isgridtfr,
            gridtfrstr='add a grid on the TFR';
          else
            gridtfrstr='remove the grid on the TFR';
          end
          Grid=x_choose([gridsigstr,gridspestr,gridtfrstr],'GRID MENU :');
          if Grid==1,
            isgridsig=1-isgridsig; CallTfrView=1;
          elseif Grid==2,
            isgridspec=1-isgridspec; CallTfrView=1;
          elseif Grid==3,
            isgridtfr=1-isgridtfr; CallTfrView=1;
          else CallTfrView=0;
          end
        end


      elseif layout==4,
        iscolorbar=sum(~iscolorbar); CallTfrView=1;
      elseif layout==0,
        CallTfrView=0;
      end;             
      
    elseif (choice==ChoiceSampling),                   // Change the sampling frequency 

      printf(' Old sampling frequency: %f\n',fs); 
      fsold=fs; fs=input(' New sampling frequency: ');
      if isempty(fs),
        fs=fsold; CallTfrView=0;
      else
        CallTfrView=1;
      end; 
    elseif (choice==ChoiceFreqBounds),                 // Change the frequency bounds
      CallTfrView=0;

      printf(' Old smallest normalized frequency : %f\n',fmin); fminold=fmin; 
      fmin=input(' New smallest normalized frequency : ');
      if isempty(fmin),
        fmin=fminold; 
      elseif fmin>0.5,
        printf('normalized frequency desired ! value unmodified\n');
        fmin=fminold; 
      else
        CallTfrView=1;
      end; 

      printf(' Old highest normalized frequency  : %f\n',fmax); fmaxold=fmax; 
      fmax=input(' New highest normalized frequency  : ');
      if isempty(fmax),
        fmax=fmaxold; 
      elseif fmax>0.5,
        printf('normalized frequency desired ! value unmodified\n');
        fmax=fmaxold;
      else
        CallTfrView=1;
      end; 
      
    elseif (choice==ChoiceThreshold),                  // Change the threshold

      printf(' Old threshold: %f\n', threshold); throld=threshold;
      threshold=input(' New threshold: ');
      if isempty(threshold),
        threshold=throld; CallTfrView=0;
      else
        CallTfrView=1;
      end

    elseif (choice==ChoiceLinlog),                     // Change the lin/log scale of tfr

      linlogtfr=1-linlogtfr;

    elseif (choice==ChoiceRedraw),                           // redraw ?
      RefreshFigure=1-RefreshFigure;
      if RefreshFigure==1, CallTfrView=1; end;

    elseif (choice==ChoiceNewFigure),                        // new figure
      figure; CallTfrView=1; 
      f = gcf();f.color_map =jetcolormap((levelnumb));

    elseif (choice==ChoiceSaveResults),                      // Save the results

      f=frq*fs;
      Nmethod=length(method);
      //if comp(1:2)=='PC',
      // DefaultName=[method(4:Nmethod),num2str(Nsig),'.mat'];
      // [name,PathWorkDir] = uiputfile(DefaultName, 'Save As');
      //else
      [name,PathWorkDir]=uiputfile(["*.dat"],pwd(),"Save As");
      DefaultName=part(method,(4:Nmethod))+string(Nsig)+".dat";
      //nameStr=[' Name of the MAT file ['+DefaultName+'] : '];
      //name=input(nameStr,'string'); 
      //    while (length(name)>8),
      //     disp(' The name must have less than 8 characters');
      //     name=input(nameStr,'s'); 
      //    end
      if isempty(name),
        name=DefaultName;
        PathWorkDir='';
      else
        name=name+".dat";
      end
      //end
      linlog=linlogtfr+2*linlogspec+4*sigenveloppe;
      isgrid=isgridsig+2*isgridspec+4*isgridtfr;
      param = [display,linlog,threshold,levelnumb,Nf2,layout,fs,isgrid,fmin,fmax];
      SavedColorMap=get(gcf(),"color_map");
      if (nargin<=4),
        TfrQView=['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrqview(tfr,sig,t,method)'];
        TfrView =['scf(); clf();f=gcf(); f.color_map=(SavedColorMap); tfrview(tfr,sig,t,method,param)'];
        save(PathWorkDir+filesep()+name,...
             tfr, sig, t, f, fs, method, param, SavedColorMap, TfrView, TfrQView);
      elseif (nargin==5),
        TfrQView=['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrqview(tfr,sig,t,method,p1)'];
        TfrView =['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrview(tfr,sig,t,method,param,p1)']; 
        save(PathWorkDir+filesep()+name,...
             tfr, sig, t, f, fs, method, param, p1, SavedColorMap, TfrView, TfrQView);
      elseif (nargin==6),
        TfrQView=['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrqview(tfr,sig,t,method,p1,p2)'];
        TfrView =['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrview(tfr,sig,t,method,param,p1,p2)'];
        save(PathWorkDir+filesep()+name,...
             tfr, sig, t, f, fs, method, param, p1, p2, SavedColorMap, TfrView, TfrQView);
      elseif (nargin==7),
        TfrQView=['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrqview(tfr,sig,t,method,p1,p2,p3)'];
        TfrView =['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrview(tfr,sig,t,method,param,p1,p2,p3)'];
        save(PathWorkDir+filesep()+name,...
             tfr, sig, t, f, fs, method, param, p1, p2, p3, SavedColorMap, TfrView, TfrQView);
      elseif (nargin==8),
        TfrQView=['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrqview(tfr,sig,t,method,p1,p2,p3,p4)'];
        TfrView =['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrview(tfr,sig,t,method,param,p1,p2,p3,p4)'];
        save(PathWorkDir+filesep()+name,...
             tfr, sig, t, f, fs, method, param, p1, p2, p3, p4, SavedColorMap, TfrView, TfrQView);
      elseif (nargin==9),
        TfrQView=['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrqview(tfr,sig,t,method,p1,p2,p3,p4,p5)'];
        TfrView =['scf(); clf(); f=gcf(); f.color_map=(SavedColorMap); tfrview(tfr,sig,t,method,param,p1,p2,p3,p4,p5)'];
        save(PathWorkDir+filesep()+name,...
             tfr, sig, t, f, fs, method, param, p1, p2, p3, p4, p5, SavedColorMap, TfrView, TfrQView);
      end;
      disp(' ');
      printf('The file is saved in the directory %s\n',PathWorkDir);
      printf('under the name %s\n',name);

      printf('If you want to find again the exact layout of this screen, do\n');
      printf('load %s/%s; execstr(TfrView);\n\n', PathWorkDir,name);
      printf('If you want to restart the display session under tfrqview, do\n');
      printf('load %s/%s; execstr(TfrQView);\n',PathWorkDir,name);
      CallTfrView=0;

    elseif (choice==ChoiceSaveOptions),                 // Save options
      SavedColorMap=get(gcf(),"color_map");
      save("tfrq_options.dat", fs, fmin, fmax, threshold, linlogtfr, linlogspec, levelnumb,...
           display, layout, colmap, SavedColorMap, iscolorbar,...
           isgridsig, isgridspec, isgridtfr, issig, sigenveloppe, isspec);
      printf('\n Options saved\n');
      CallTfrView=0;
      
    elseif (choice==ChoicePrint),	                     // Print the current figure

      Nmethod=length(method);
      TFTBStringList=['xs2pdf','xs2eps','xs2png','xs2bmp','xs2gif','xs2jpg','xs2ppm','xs2svg','xs2ps','xs2fig','xs2emf'];
      TFTBDevice = x_choose(TFTBStringList,'Choose a device');
      if TFTBDevice~=0,
        //if comp(1:2)=='PC',
        // DefaultName=[method(4:Nmethod),string(Nsig),TFTBExtension];
        // [name,PathWorkDir] = uiputfile(DefaultName, 'Save As');
        [name,PathWorkDir]=uiputfile(["*."+part(TFTBStringList(TFTBDevice),4:6)],pwd(),"Save As");
        //else
        DefaultName=part(method,(4:Nmethod))+string(Nsig);
        //nameStr=[' file name ['+DefaultName+'] : '];
        //name=input(nameStr,'string'); 
        //     while (length(name)>8),
        //      disp('The name must have less than 8 characters');
        //      name=input(nameStr,'s'); 
        //     end
        if isempty(name),
          name=DefaultName;
          PathWorkDir='.';
        end
        fullname=PathWorkDir+filesep()+name;
        if TFTBDevice==1,
          xs2pdf(gcf(),fullname);
        elseif TFTBDevice==2,
          xs2eps(gcf(),fullname);
        elseif TFTBDevice==3,
          xs2png(gcf(),fullname);
        elseif TFTBDevice==4,
          xs2bmp(gcf(),fullname);
        elseif TFTBDevice==5,
          xs2gif(gcf(),fullname);
        elseif TFTBDevice==6,
          xs2jpg(gcf(),fullname);
        elseif TFTBDevice==7,
          xs2ppm(gcf(),fullname);
        elseif TFTBDevice==8,
          xs2svg(gcf(),fullname);
        elseif TFTBDevice==9,
          xs2ps(gcf(),fullname);
        elseif TFTBDevice==10,
          xs2fig(gcf(),fullname);
        elseif TFTBDevice==11,
          xs2emf(gcf(),fullname);
        end;

        printf(' The file is saved in the directory %s\n',PathWorkDir);
        printf('under the name %s\n',name+"."+part(TFTBStringList(TFTBDevice),4:6));
        //end;
        CallTfrView=0;
      end;
    end;

  end;

  // good bye. I hope that everything happened fine.
endfunction

