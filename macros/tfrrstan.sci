function [stan,rtfr,rhat] = tfrrstan(x,t,N,g,h,ptrace,opt_plot);
// Reassigned Stankovic distribution.
// Calling Sequence
//	[TFR,RTFR,HAT] = tfrrstan(X) 
//	[TFR,RTFR,HAT] = tfrrstan(X,T) 
//	[TFR,RTFR,HAT] = tfrrstan(X,T,N) 
//	[TFR,RTFR,HAT] = tfrrstan(X,T,N,G) 
//	[TFR,RTFR,HAT] = tfrrstan(X,T,N,G,H) 
//	[TFR,RTFR,HAT] = tfrrstan(X,T,N,G,H,TRACE) 
//	[TFR,RTFR,HAT] = tfrrstan(...,'plot') 
//  Parameters
//	X     : analysed signal.
//	T     : the time instant(s)      (default : 1:length(X)).
//	N     : number of frequency bins (default : length(X)).
//      G     : frequency averaging window
//	h     : stft window              (default : Hamming(N/4)).
//	TRACE : if nonzero, the progression of the algorithm is shown     (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrrstan runs tfrqview. and TFR will be plotted
//	TFR,  : time-frequency representation and its reassigned
//	RTFR    version.
//	HAT   : Complex matrix of the reassignment vectors.
//     Description
//       tfrrstan computes the Stankovic distribution and its reassigned version.
//     Examples
//      sig=fmlin(128,0.1,0.4); t=1:2:128;
//      G=tftb_window(9,'hanning'); h=tftb_window(61,'hanning'); tfrrstan(sig,t,128,G,h,1,'plot');
//     Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, August, September 1997.

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrrstan"
 
  in_par=['x','t','N','G','h','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,7));
  end;

   //x
  if type(x)<>1|and(size(x)>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A vector of double expected.\n"),fname,1));
  end
  x=x(:);
  Nx=size(x,1);

   //ptrace
  if nargin==6 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,6));
    end
  else
    ptrace=%f;
  end
  
  //h
  if nargin>=5 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,5));
    end
    hlength=size(h,"*");
    if (modulo(hlength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,5));
    end;
  else
    hlength=floor(Nx/4);
    if rem(hlength,2)==0 then hlength=hlength+1;end;
    h = window("hm",hlength)
  end
  h=h(:);
  Lh=(hlength-1)/2; //h=h/h(Lh+1);
  
  //g
  if nargin>=4 then 
    if type(g)<>1|and(size(g)>1)|~isreal(g) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,4));
    end
    glength=size(g,"*");
    if (modulo(glength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,4));
    end;
  else
    g=[0.25; 0.5; 0.25]
    glength=3
  end
  g=g(:);
  Lg=(glength-1)/2; //g=g/g(Lg+1);
 
  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end

  else
    N=Nx
  end
  
  //T
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>Nx|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
      Deltat=diff(t);
    if or(Deltat<>Deltat(1)) then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be regularly spaced.\n"),fname,2));
    end
    Dt=Deltat(1);
  else
    t=1:Nx;
    Dt=1;
  end
  Nt=size(t,2);
  
 

  stan = zeros(N,Nt); 
  rtfr = zeros(N,Nt); 
  rhat  = zeros(N,Nt);

  if ptrace, disp('Stankovic distribution (with reassignement)'); end;
  Ex=mean(abs(x(min(t):max(t))).^2); Threshold=1.0e-3*Ex;
  Dh=dwindow(h); Th=h.*[-Lh:Lh]';
  for icol=1:Nt,
    if ptrace, disprog(icol,Nt,10); end;
    ti= t(icol); 
    tau=-min([round(N/2)-1,Lh,ti-1]):min([round(N/2)-1,Lh,Nx-ti]);
    indices= rem(N+tau,N)+1;
    tfr= zeros(N,3); 
    tfr(indices,1)=x(ti+tau).*conj(h(Lh+1-tau));
    tfr(indices,2)=x(ti+tau).*conj(Th(Lh+1-tau));
    tfr(indices,3)=x(ti+tau).*conj(Dh(Lh+1-tau));
    tfr=fft(tfr,-1,1); 
    stan(:,icol)=g(Lg+1) * abs(tfr(:,1)).^2;

    for jcol=1:N,
      stanTh=g(Lg+1)*tfr(jcol,1)*conj(tfr(jcol,2));
      stanDh=g(Lg+1)*tfr(jcol,1)*conj(tfr(jcol,3));
      for kstan=1:min(N/2-1,Lg),
        stanbefore=rem(rem(jcol-kstan-1,N)+N,N)+1;
        stanafter =rem(rem(jcol+kstan-1,N)+N,N)+1;
        stan(jcol,icol)= stan(jcol,icol) ...
            + g(Lg+1-kstan)*tfr(stanbefore,1)*conj(tfr(stanafter ,1)) ...
            + g(Lg+1+kstan)*tfr(stanafter ,1)*conj(tfr(stanbefore,1));
        stanTh= stanTh + g(Lg+1-kstan)*tfr(stanbefore,1)*conj(tfr(stanafter ,2)) ...
                + g(Lg+1+kstan)*tfr(stanafter ,1)*conj(tfr(stanbefore,2));
        stanDh= stanDh + g(Lg+1-kstan)*tfr(stanbefore,1)*conj(tfr(stanafter ,3)) ...
                + g(Lg+1+kstan)*tfr(stanafter ,1)*conj(tfr(stanbefore,3));
      end;
      stan(jcol,icol)=real(stan(jcol,icol));
      if abs(stan(jcol,icol))>Threshold,
        icolhat = round(icol - real(stanTh/stan(jcol,icol))/Dt);
        jcolhat = round(jcol - N*imag(stanDh/stan(jcol,icol)/(2.0*%pi)));
        
        jcolhat= real(rem(rem(jcolhat-1,N)+N,N)+1);
        icolhat= min(max(real(icolhat),1),Nt);
        
        rtfr(jcolhat,icolhat)=rtfr(jcolhat,icolhat) + stan(jcol,icol) ;
        rhat(jcol,icol)= jcolhat + %i * icolhat;
        //printf('%12.3f %12.3f , %12.3f %12.3f \n',jcol,icol,jcolhat,icolhat);
      else
        rtfr(jcol,icol)=rtfr(jcol,icol) + stan(jcol,icol);
        hat(jcol,icol)= jcol + %i * icol;
      end;
    end;

  end ;
  tfr=real(tfr)
  rtfr=real(tfr)
  if ptrace, printf('\n'); end;

  if (plotting),
    
    while %t
      choice=x_choose(['Stankovic distribution',...
                    'reassigned Stankovic distribution'],'Choose the representation:','stop');
      if (choice==0) then
        break
      elseif (choice==1), 
        tfrqview(stan,x,t,'type1');
      elseif (choice==2),
        tfrqview(rtfr,x,t,'type1');
      end;
    end;
  end;
endfunction

