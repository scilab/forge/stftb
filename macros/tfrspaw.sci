function [tfr,t,f]=tfrspaw(X,time,K,nh0,ng0,fmin,fmax,N,ptrace,opt_plot);
// Smoothed Pseudo Affine Wigner time-frequency distributions.
// Calling Sequence
//	[TFR,T,F]=tfrspaw(X)
//	[TFR,T,F]=tfrspaw(X,T)
//	[TFR,T,F]=tfrspaw(X,T,K)
//	[TFR,T,F]=tfrspaw(X,T,K,NH0)
//	[TFR,T,F]=tfrspaw(X,T,K,NH0,NG0)
//	[TFR,T,F]=tfrspaw(X,T,K,NH0,NG0,FMIN,FMAX)
//	[TFR,T,F]=tfrspaw(X,T,K,NH0,NG0,FMIN,FMAX,N)
//	[TFR,T,F]=tfrspaw(X,T,K,NH0,NG0,FMIN,FMAX,N,TRACE)
//	[TFR,T,F]=tfrspaw(...,'plot')
// Parameters
//	X : signal (in time) to be analyzed. If X=[X1 X2], TFRSPAW   computes the cross-Smoothed Pseudo Affine Wigner distribution. (Nx=length(X)).
//	T : time instant(s) on which the TFR is evaluated  (default is 1:Nx).
//	K : label of the K-Bertrand distribution. The distribution with parameterization function   lambdak(u,K) = (K (exp(-u)-1)/(exp(-Ku)-1))^(1/(K-1))  is computed (default is 0).
//	     K=-1 : Smoothed pseudo (active) Unterberger distribution 
//	     K=0  : Smoothed pseudo Bertrand distribution
//	     K=1/2: Smoothed pseudo D-Flandrin distribution
//	     K=2  : Affine smoothed pseudo Wigner-Ville distribution.
//	NH0 : half length of the analyzing wavelet at coarsest scale.   A Morlet wavelet is used. NH0 controles the frequency  smoothing of the smoothed pseudo Affine Wigner distribution. (default is sqrt(Nx)).
//	NG0 : half length of the time smoothing window. 
//	NG0 = 0 corresponds to the Pseudo Affine Wigner distribution.  (default is 0).
//	FMIN,FMAX : respectively lower and upper frequency bounds of   the analyzed signal. These parameters fix the equivalent  frequency bandwidth (expressed in Hz). When unspecified, you   have to enter them at the command line from the plot of the	spectrum. FMIN and FMAX must be >0 and <=0.5. 
//	N : number of analyzed voices (default : automatically determined).
//	TRACE : if nonzero, the progression of the algorithm is shown (default is 0).
//      'plot':	if one input parameter is 'plot',  tfrspwv runs tfrqview. and TFR will be plotted
//	TFR : time-frequency matrix containing the coefficients of the  decomposition (abscissa correspond to uniformly sampled time,  and ordinates correspond to a geometrically sampled	   frequency). First row of TFR corresponds to the lowest    frequency. 
//	F : vector of normalized frequencies (geometrically sampled  from FMIN to FMAX).
// Description
//	tfrspaw generates the auto- or cross- Smoothed Pseudo Affine Wigner distributions. 
//  Examples    
//         sig=altes(64,0.1,0.45); tfrspaw(sig,'plot');
// Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, October 95 
//      O. Lemoine, June 1996.
//	Copyright (c) 1995 Rice University - CNRS (France) 1996.

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrspaw";
 
  in_par=['X','time','K','nh0','ng0','fmin','fmax','N','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,10));
  end
  
  //X
  if type(X)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(X)==1) then
    X=X(:);
  end
  if size(X,2)>2 then
     error(msprintf(_("%s: Wrong size for input argument #%d: A  vector or a two columns array expected.\n"),fname,1));
  end
  [Nx,xcol]=size(X);
  
  //ptrace
  if nargin==9 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,9));
    end
  else
    ptrace=%f;
  end
  
   //N
  if nargin>=8 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,8));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,8));
    end
  else
    N=[]
  end
  
  //fmax
  if nargin>=7 then
     if type(fmax)<>1|size(fmax,"*")>1|~isreal(fmax) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,7));
     end 
     if fmax<=0|fmax>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,7,"]0,0.5]"));
     end
  else
    fmax=-1
  end
  
  //fmin
  if nargin>=6 then
     if type(fmin)<>1|size(fmin,"*")>1|~isreal(fmin) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,6));
     end 
     if fmin<=0|fmin>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,6,"]0,0.5]"));
     end
     if fmin>fmax then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be < %g.\n"),fname,6,fmax));
     end
  else
    fmin=-1
  end
  
  //ng0
  if nargin>=5 then
      if type(ng0)<>1|size(ng0,"*")>1|~isreal(ng0)|ng0<0 then
       error(msprintf(_("%s: Wrong type for input argument #%d: A non negative real expected.\n"),fname,5));
     end 
  else
    ng0=0
  end
  
  //nh0
   if nargin>=4 then
     if type(nh0)<>1|size(nh0,"*")>1|~isreal(nh0) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A positive real expected.\n"),fname,4));
     end 
  else
    nh0=sqrt(Nx)
  end
  
  //K
  if nargin>=3 then
     if type(K)<>1|size(K,"*")>1|~isreal(K) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
     end 
  else
    K=0;
  end
  
    
  //time
  if nargin>=2 then
    if type(time)<>1|and(size(time)>1)|~isreal(time) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(time)>Nx|min(time)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
  else
    time=1:Nx;
  end
  Nt=size(time,2);
 


  Mt=length(X); 

  if ptrace then
    if (K==-1),
      disp('Smoothed pseudo (active) Unterberger distribution');
    elseif K==0,
      disp('Smoothed pseudo Bertrand distribution');
    elseif K==1/2,
      disp('Smoothed pseudo D-Flandrin distribution');
    elseif K==2,
      disp('Affine smoothed pseudo Wigner-Ville distribution');
    else
      disp('Smoothed Pseudo Affine Wigner distribution');
    end;
  end;

  if xcol==1,
    X1=X;
    X2=X; 
  else
    X1=X(:,1);
    X2=X(:,2);
  end
  s1 = real(X1);
  s2 = real(X2);
  M  = (Mt+rem(Mt,2))/2;

  if fmin==-1|fmax==-1 then // fmin,fmax,N unspecified
    STF1 = fft(fftshift(s1(min(time):max(time))));
    Nstf=length(STF1);
    sp1 = (abs(STF1(1:Nstf/2))).^2; Maxsp1=max(sp1);
    STF2 = fft(fftshift(s2(min(time):max(time)))); 
    sp2 = (abs(STF2(1:Nstf/2))).^2; Maxsp2=max(sp2);
    f = linspace(0,0.5,Nstf/2+1) ; f=f(1:Nstf/2)';
    plot(f,sp1) ; xgrid;  plot(f,sp2) ; 
    xlabel(_('Normalized frequency'));
    title(_('Analyzed signal energy spectrum'));
    a=gca();a.data_bounds=[0 1/2 0 1.2*max(Maxsp1,Maxsp2)];
    if fmin==-1 then
      indmin=min(find(sp1>Maxsp1/100));
      fmindflt=max([0.01 0.05*fix(f(indmin)/0.05)]);
      txtmin=msprintf(_('Lower frequency bound [%s] :'),string(fmindflt));
      while  %t then
        fmin = input(txtmin); 
        if fmin==[] then 
          fmin=fmindflt; break
        elseif fmin>0&fmin<=0.5 then
          break
        else
          mprintf(_("%s: Wrong given value for lower frequency bound: Must be in the interval %s.\n"),fname,"]0,0.5]"); 
        end
      end
    end
    
    if fmax==-1 then
      indmax=max(find(sp1>Maxsp1/100));
      fmaxdflt=0.05*ceil(f(indmax)/0.05);
      txtmax=msprintf(_('Upper frequency bound [%s] :'),string(fmaxdflt));
      while  %t then
        fmax = input(txtmax); 
        if fmax==[] then 
          fmax=fmaxdflt; break
        elseif fmax>0&fmax<=0.5&fmax>fmin then
          break
        else
          mprintf(_("%s: Wrong given value for upper frequency bound: Must be in the interval %s and greater than fmin\n"),fname,"]0,0.5]"); 
        end
      end  
    end
  end
  
  B    = fmax-fmin ; 
  R    = B/((fmin+fmax)/2) ; 
  Qte  = fmax/fmin ;    
  umax = log(Qte); 
  Teq  = nh0/(fmax*umax);  
  if Teq<2*nh0,
    M0 = (2*nh0^2)/Teq-nh0+1;
  else
    M0 = 0;
  end;
  MU = round(nh0+M0);
  T  = 2*MU-1;

  Nq = ceil((B*T*(1+2/R)*log((1+R/2)/(1-R/2)))/2);
  Nmin = Nq-rem(Nq,2);
  if N==[] then
    Ndflt = 2^nextpow2(Nmin);
    Ntxt=msprintf(_('Number of frequency samples (>=%d) [%d] : '),Nmin,Ndflt);
    while  %t then
      N=input(Ntxt); 
      if N==[] then 
        N=Ndflt;break;
      elseif N>Nmin&int(N)==N then
        break
      else
        mprintf(_("%s: Wrong given value for number of frequency samples: a positive integer expected.\n"),fname);  
      end
    end

  else
    if N<=Nmin then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d expected.\n"),fname,8,Nmin));
    end      
  end
    
  

  fmin_s = string(fmin); 
  fmax_s = string(fmax); 
  N_s = string(N);
  if ptrace,
    disp(['Frequency runs from '+fmin_s+' to '+fmax_s+' with '+N_s+' points']);
  end

  k = 1:N;
  q = (fmax/fmin)^(1/(N-1));
  a = exp((k-1).*log(q));         // a is an increasing scale vector.
  geo_f = fmin*a;                 // geo_f is a geometrical increasing frequency vector.

  // Wavelet decomposition computation
  matxte1 = zeros(N,Nt);
  matxte2 = zeros(N,Nt);
  [p1,p2,p3,wt1] = tfrscalo(s1,time,nh0,fmin,fmax,N) ;
  [p1,p2,p3,wt2] = tfrscalo(s2,time,nh0,fmin,fmax,N) ;
  for ptr = 1:N, 
    matxte1(ptr,:) = wt1(ptr,:).*sqrt(a(N-ptr+1)) ; 
    matxte2(ptr,:) = wt2(ptr,:).*sqrt(a(N-ptr+1)) ; 
  end ;

  umin = -umax;
  u=linspace(umin,umax,2*MU+1);
  du = u(2)-u(1);
  u=u(1:2*MU);
  u(MU+1) = 0;
  p = 0:(2*N-1);
  Beta = (p/N-1)./(2*log(q));
  l1=zeros(2*MU,2*N);
  l2=zeros(2*MU,2*N);
  for m = 1:2*MU,
    l1(m,:) = exp(-2*%i*%pi*Beta*log(lambdak( u(m),K)));
    l2(m,:) = exp(-2*%i*%pi*Beta*log(lambdak(-u(m),K)));
  end 


  // Determination of the time smoothing window G
  if ng0==0,
    G = ones(2*MU,1);
  else
    a_t = 3 ;            // (attenuation of 10^(-a_t) at t = tmax)
    sigma_t = ng0*fmax/sqrt(2*a_t*log(10));
    a_u = 2 * %pi^2 * sigma_t^2 * umax^2 / log(10) ;
    sigma_u = 1/(2 * %pi * sigma_t) ;
    G = exp(-(a_u*log(10)/MU^2)*[-MU:MU-1].^2); 
    if sigma_u < du
      warning('Maximum time smoothing reached. Increase width of wavelet for effectiveness.') ;
    end
    G=G';
  end

  waf = zeros(2*MU,N);
  tfr = zeros(N,Nt);
  S1  = zeros(1,2*N);
  S2  = zeros(1,2*N);
  MX1 = zeros(2*N,2*MU);
  MX2 = zeros(2*N,2*MU);
  TX1 = zeros(2*MU,N);
  TX2 = zeros(2*MU,N);

  for ti = 1:Nt,
    if ptrace, disprog(ti,Nt,10); end

    S1(1:N) = matxte1(:,ti).';
    Mellin1 = fftshift(ifft(S1));
    MX1 = (l1.*Mellin1(ones(1,2*MU),:)).';
    MX1 = fft(MX1);
    TX1 = MX1(1:N,:).';

    S2(1:N) = matxte2(:,ti).';
    Mellin2 = fftshift(ifft(S2));     
    MX2 = (l2.*Mellin2(ones(1,2*MU),:)).';
    MX2 = fft(MX2);
    TX2 = MX2(1:N,:).';

    waf = real(TX1.*conj(TX2)).*G(:,ones(N,1));
    tfr(:,ti) = (sum(waf,1).*geo_f).';	// first row of tfr corresponds to
                                                // the lowest frequency.
  end;


  t = time; 
  f = geo_f'; 

  // Normalization
  SP1 = fft(hilbert(s1)); 
  SP2 = fft(hilbert(s2)); 
  indmin = 1+round(fmin*(Nx-2));
  indmax = 1+round(fmax*(Nx-2));
  SP1ana = SP1(indmin:indmax);
  SP2ana = SP2(indmin:indmax);
  tfr = real(tfr*(SP1ana'*SP2ana)/integ2d(tfr,t,f)/N);


  if (plotting),
    tfrqview(real(tfr),hilbert(real(X)),t,'tfrspaw',K,nh0,ng0,N,f);
  end;

endfunction
