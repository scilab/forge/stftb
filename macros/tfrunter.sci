function [tfr,t,f]=tfrunter(X,time,form,fmin,fmax,N,ptrace,opt_plot); 
// Unterberger time-frequency distribution.
// Calling Sequence
//	[TFR,T,F]=tfrunter(X,)
//	[TFR,T,F]=tfrunter(X,T)
//	[TFR,T,F]=tfrunter(X,T,FORM)
//	[TFR,T,F]=tfrunter(X,T,FORM,FMIN,FMAX)
//	[TFR,T,F]=tfrunter(X,T,FORM,FMIN,FMAX,N)
//	[TFR,T,F]=tfrunter(X,T,FORM,FMIN,FMAX,N,TRACE)
//	[TFR,T,F]=tfrunter(...,'plot')
//  Parameters
//	X : signal (in time) to be analyzed. If X=[X1 X2], tfrunter   computes the cross-Unterberger distribution (Nx=length(X)).
//	T : time instant(s) on which the TFR is evaluated  (default : 1:Nx).
//	FORM : 'A' for active, 'P' for passive Unterberger distribution.(default : 'A'). 
//	FMIN,FMAX : respectively lower and upper frequency bounds of   the analyzed signal. These parameters fix the equivalent   frequency bandwidth (expressed in Hz). When unspecified, you have to enter them at the command line from the plot of the   spectrum. FMIN and FMAX must be >0 and <=0.5. 
//	N : number of analyzed voices (default : automatically determined).
//	TRACE : if nonzero, the progression of the algorithm is shown (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrdfla runs tfrqview. and TFR will be plotted
//	TFR : time-frequency matrix containing the coefficients of the    decomposition (abscissa correspond to uniformly sampled  time, and ordinates correspond to a geometrically sampled    frequency). First row of TFR corresponds to the lowest  frequency. 
//	F : vector of normalized frequencies (geometrically sampled    from FMIN to FMAX).
//   Description
//        tfrunter generates the auto- or cross-Unterberger distribution (active or passive form).
//   Examples 
//        sig=altes(64,0.1,0.45); tfrunter(sig,'plot');
// Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, November 95 - O. Lemoine, June 1996.
//	Copyright (c) 1995 Rice University - CNRS (France) 1996.

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrunter";
  


  in_par=['X','time','form','fmin','fmax','N','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,8));
  end;
  
  //X
  if type(X)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(X)==1) then
    X=X(:);
  end
  if size(X,2)>2 then
    error(msprintf(_("%s: Wrong size for input argument #%d: A vector or a two columns array expected.\n"),fname,1));
  end
  [Nx,xcol]=size(X);
  
  //ptrace
  if nargin>=7 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,7));
    end
  else
    ptrace=%f;
  end

    //N
  if nargin>=6 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,6));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,6));
    end
  else
    N=[]
  end
  
    //fmax
  if nargin>=5 then
     if type(fmax)<>1|size(fmax,"*")>1|~isreal(fmax) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,5));
     end 
     if fmax<=0|fmax>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,5,"]0,0.5]"));
     end
  else
    fmax=[]
  end
  
  //fmin
  if nargin>=4 then
     if type(fmin)<>1|size(fmin,"*")>1|~isreal(fmin) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,4));
     end 
     if fmin<=0|fmin>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,4,"]0,0.5]"));
     end
     if fmin>fmax then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be < %g.\n"),fname,4,fmax));
     end
  else
    fmin=[]
  end
  
  //form
  if nargin>=3 then
    if type(form)<>10|size(form,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: String expected.\n"),fname,3));
    end
    form=convstr(form,"u")
    if and(form<>["A","P"]) then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set  {%s}.\n"),fname,3,"""A"",""P"""));
    end
  else
    form='A'; 
  end
  
 //time
  if nargin>=2 then
    if type(time)<>1|and(size(time)>1)|~isreal(time) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(time)>Nx|min(time)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
  else
    time=1:Nx;
  end
  Nt=size(time,2);
 

  Mt = length(X); 

  if ptrace, disp('Unterberger distribution'); end;

  if xcol==1,
    X1=X;
    X2=X; 
  else
    X1=X(:,1);
    X2=X(:,2);
  end
  s1 = real(X1);
  s2 = real(X2);
  M  = (Mt+rem(Mt,2))/2;

  t = (1:Mt)-M-1;
  T = Nx;

  if fmin==[]|fmax==[] then
    STF1 = fft(fftshift(s1(min(time):max(time))));
    Nstf=length(STF1);
    sp1 = (abs(STF1(1:Nstf/2))).^2; Maxsp1=max(sp1);
    STF2 = fft(fftshift(s2(min(time):max(time)))); 
    sp2 = (abs(STF2(1:Nstf/2))).^2; Maxsp2=max(sp2);
    f = linspace(0,0.5,Nstf/2+1) ; f=f(1:Nstf/2)';
    plot(f,sp1) ; xgrid;  plot(f,sp2) ; 
    xlabel(_('Normalized frequency'));
    title(_('Analyzed signal energy spectrum'));
    a=gca();a.data_bounds=[0 1/2 0 1.2*max(Maxsp1,Maxsp2)];
    if fmin==[] then
      indmin=min(find(sp1>Maxsp1/100));
      fmindflt=max([0.01 0.05*fix(f(indmin)/0.05)]);
      txtmin=msprintf(_('Lower frequency bound [%s] :'),string(fmindflt));
      while  %t then
        fmin = input(txtmin); 
        if fmin==[] then 
          fmin=fmindflt; break
        elseif fmin>0&fmin<=0.5 then
          break
        else
          mprintf(_("%s: Wrong given value for lower frequency bound: Must be in the interval %s.\n"),fname,"]0,0.5]"); 
        end
      end
    end
    if fmax==[] then
      indmax=max(find(sp1>Maxsp1/100));
      fmaxdflt=0.05*ceil(f(indmax)/0.05);
      txtmax=msprintf(_('Upper frequency bound [%s] :'),string(fmaxdflt));
      while  %t then
        fmax = input(txtmax); 
        if fmax==[] then 
          fmax=fmaxdflt; break
        elseif fmax>0&fmax<=0.5&fmax>fmin then
          break
        else
          mprintf(_("%s: Wrong given value for upper frequency bound: Must be in the interval %s and greater than fmin\n"),fname,"]0,0.5]"); 
        end
      end  
    end
  end
  
  B = fmax-fmin ; 
  R = B/((fmin+fmax)/2) ; 
  ratio_f = fmax/fmin ;
  //global ratio_f
  //umax = fzero('umaxunte',0); 
  //clearglobal ratio_f
  umax=(ratio_f-1)/sqrt(ratio_f)
  
  Teq = M/(fmax*umax);  
  if Teq<2*M,
    M0 = round((2*M^2)/Teq-M)+1;
    M1 = M+M0;
    T  = 2*M1-1;
  elseif Teq>=2*M
    M0 = 0;
    M1 = M;
  end;

  Nq= ceil((B*T*(1+2/R)*log((1+R/2)/(1-R/2)))/2);
  Nmin = Nq-rem(Nq,2);
  if N==[] then
    Ndflt = 2^nextpow2(Nmin);
    Ntxt=msprintf(_('Number of frequency samples (>=%d) [%d] : '),Nmin,Ndflt);
    while  %t then
      N=input(Ntxt); 
      if N==[] then 
        N=Ndflt;break;
      elseif N>=Nmin&int(N)==N then
        break
      else
        mprintf(_("%s: Wrong given value for number of frequency samples: a positive integer expected.\n"),fname);  
      end
    end

  else
    if N<Nmin then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d expected.\n"),fname,8,Nmin));
    end      
  end
    

  fmin_s = string(fmin) ; fmax_s = string(fmax) ; N_s = string(N) ;
  if ptrace,
    disp(['frequency runs from '+fmin_s+' to '+fmax_s+' with '+N_s+' points']);
  end


  // Geometric sampling of the analyzed spectrum
  k = 1:N;
  q = (fmax/fmin)^(1/(N-1));
  t = (1:Mt)-M-1;
  geo_f  = fmin*(exp((k-1).*log(q)));
  tfmatx = zeros(Mt,N);
  tfmatx = exp(-2*%i*t'*geo_f*%pi);
  S1 = s1'*tfmatx; 
  S2 = s2'*tfmatx; 
  clear tfmatx
  S1(N+1:2*N) = zeros(1,N);
  S2(N+1:2*N) = zeros(1,N);


  // Mellin transform computation of the analyzed signal
  p = 0:(2*N-1);
  Mellin1 = fftshift(ifft(S1));
  Mellin2 = fftshift(ifft(S2));
  umin = -umax;
  du = abs(umax-umin)/(2*M1);
  u(1:2*M1) = umin:du:umax-du;
  u(M1+1) = 0;
  Beta = (p/N-1)./(2*log(q));


  // Computation of the Lambda(+/- u) dilations/compressions 
  // of the analyzed signal
  waf = zeros(2*M1,N);
  for n = 1:2*M1,
    if ptrace, disprog(n,4*M1,10); end
    MX1 = exp(-(2*%i*%pi*Beta+0.5)*log(sqrt(1+(u(n)/2)^2)-u(n)/2)).*Mellin1 ;
    MX2 = exp(-(2*%i*%pi*Beta+0.5)*log(sqrt(1+(u(n)/2)^2)+u(n)/2)).*Mellin2 ;
    FX1 = fft(fftshift(MX1)) ;
    FX1 = FX1(1:N) ;
    FX2 = fft(fftshift(MX2)) ;
    FX2 = FX2(1:N) ;
    if form=='A',
      waf(n,:) = FX1.*conj(FX2); 
    else
      waf(n,:) = FX1.*conj(FX2)*(1/sqrt(1+(u(n)/2)^2)); 
    end
  end
  waf  = [waf(M1+1:2*M1,:) ; waf(1:M1,:)].*geo_f(ones(2*M1,1),:);
  tffr = fft(waf,1,1);  
  tffr = real(rot90([tffr(M1+1:2*M1,:) ; tffr(1:M1,:)],-1));


  // Conversion from [t.f,f] to [t,f] using a 1-D interpolation
  tfr  = zeros(N,Nt);
  Ts2  = (Mt-1)/2 ;
  gamma0 = linspace(-geo_f(N)*Ts2,geo_f(N)*Ts2,2*M1) ;
  alpha = (0.6*N-1)/0.4;
  for n = 1:N,
    if ptrace, disprog(n+alpha,N+alpha,10); end
    ind = find(gamma0>=-geo_f(n)*Ts2 & gamma0<=geo_f(n)*Ts2);
    x = gamma0(ind);
    y = tffr(n,ind);
    xi = (time-Ts2-1)*geo_f(n);
    v=interp1(x,y,xi,'spline');
    [l,r]=size(v);
    if (r==1),
      v=v';
    end;
    tfr(n,:)=v;
    clear v
  end 


  t = time;
  f = geo_f(1:N)';

  // Normalization
  SP1 = fft(hilbert(s1)); 
  SP2 = fft(hilbert(s2)); 
  indmin = 1+round(fmin*(Nx-2));
  indmax = 1+round(fmax*(Nx-2));
  SP1ana=SP1(indmin:indmax);
  SP2ana=SP2(indmin:indmax);
  tfr=real(tfr*(SP1ana'*SP2ana)/integ2d(tfr,t,f)/N);

  if (plotting),
    tfrqview(real(tfr),hilbert(real(X1)),t,'TFRUNTER',N,f);
  end;
endfunction

