function builder_gw_c()
  CURRENT_PATH = strsubst(get_absolute_file_path("builder_gateway_c.sce"), "\", "/");
  INCLUDES_PATHS = "-I" + CURRENT_PATH +" -Dkiss_fft_scalar=double "
  FILES_GATEWAY = ["kiss_fft.c","af2tfr.c","create_window.c","distance.c","Dwindow.c","mhs.c","ppage.c","ri.c",...
                   "ridt.c","af.c","divers.c","hough.c","mmce.c","pwv.c","ridb.c","sp.c","bj.c",...
                   "cw.c","gradient.c","kernel.c","page.c","reassign.c","ridbn.c","spwv.c","wv.c",...
                   "bud.c","grd.c","mh.c","pmh.c","reas_spectro.c","ridh.c","stft.c","zam.c",..
                   "Caf2tfr.c","Cambifunb.c","Cwindow.c","Ctfrsp.c","Ctfrrsp.c","Ctfrcw.c","Ctfrbj.c",..
                   "Ctfrbud.c","Ctfrgrd.c","Ctfrker.c","Ctfrmh.c","Ctfrwv.c","Ctfrpwv.c","Ctfrzam.c",...
                   "Ctfrstft.c","Ctfrri.c","Ctfrspwv.c","Ctfrppage.c","Ctfrpmh.c","Ctfrdist.c",...
                   "Ctfrridt.c","Ctfrpage.c","Ctfrmmce.c","Ctfrmhs.c","Chtl.c","Ctfrridh.c","Ctfrridbn.c",...
                   "Ctfrridb.c","Ctfrreas.c"];
  if newest(CURRENT_PATH+["date_build","builder_gateway.sce", ...
                    FILES_GATEWAY,"kiss_fft.h" "_kiss_fft_guts.h"])<>1 then 
    curdir=pwd();
    try
    FUNCTIONS_GATEWAY = ["Caf2tfr","int_Caf2tfr","csci6";
                    "Cambifunb","int_Cambifunb","csci6";
                    "Cwindow","int_Cwindow","csci6";
                    "Ctfrsp","int_Ctfrsp","csci6";
                    "Ctfrrsp","int_Ctfrrsp","csci6";
                    "Ctfrcw","int_Ctfrcw","csci6";
                    "Ctfrbj","int_Ctfrbj","csci6";
                    "Ctfrbud","int_Ctfrbud","csci6";
                    "Ctfrgrd","int_Ctfrgrd","csci6";
                    "Ctfrker","int_Ctfrker","csci6";
                    "Ctfrmh","int_Ctfrmh","csci6";
                    "Ctfrwv","int_Ctfrwv","csci6";
                    "Ctfrpwv","int_Ctfrpwv","csci6";
                    "Ctfrzam","int_Ctfrzam","csci6";
                    "Ctfrstft","int_Ctfrstft","csci6";
                    "Ctfrri","int_Ctfrri","csci6";
                    "Ctfrspwv","int_Ctfrspwv","csci6";
                    "Ctfrppage","int_Ctfrppage","csci6";
                    "Ctfrpmh","int_Ctfrpmh","csci6";
                    "Ctfrdist","int_Ctfrdist","csci6";
                    "Ctfrridt","int_Ctfrridt","csci6";
                    "Ctfrpage","int_Ctfrpage","csci6";
                    "Ctfrmmce","int_Ctfrmmce","csci6";
                    "Ctfrmhs","int_Ctfrmhs","csci6";
                    "Chtl","int_Chtl","csci6";
                    "Ctfrridh","int_Ctfrridh","csci6";
                    "Ctfrridbn","int_Ctfrridbn","csci6";
                    "Ctfrridb","int_Ctfrridb","csci6";
                    "Ctfrreas","int_Ctfrreas","csci6"];
    if getos()=="Windows" then
      LIBS="../../src/fortran/libslatec"
    else
      LIBS=""
    end
    tbx_build_gateway("stftb_c", FUNCTIONS_GATEWAY, FILES_GATEWAY, CURRENT_PATH, LIBS,"",INCLUDES_PATHS);
    mputl(sci2exp(getdate()),CURRENT_PATH+"date_build")
    catch
      cd(curdir);
      mprintf("%s\n",lasterror());
    end
  end
end
builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack
