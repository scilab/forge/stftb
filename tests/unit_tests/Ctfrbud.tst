mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function tfrbudt
//TFRBUDT Unit test for the function TFRBUD.

//       F. Auger, Dec. 1995 - O. Lemoine, March 1996. 

// We test each property of the corresponding TFR :
//
rtol=100*%eps;
atol=1000*%eps;

// First test the optional arguments
N=32;
sig=ones(1,N);
tfr=Ctfrbud(sig);
//test the algorithm against the tfrbud one
assert_checkalmostequal(tfr,tfrbud(sig),rtol,atol);
//test the optional arguments
assert_checkequal(Ctfrbud(sig,1:N),tfr);
assert_checkequal(Ctfrbud(sig,1:N,32),tfr);
assert_checkequal(Ctfrbud(sig,1:N,32,tftb_window(int(N/10),"hamming")),tfr);
assert_checkequal(Ctfrbud(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming")),tfr);
assert_checkequal(Ctfrbud(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"),1),tfr);

[t,T]=Ctfrbud(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrbud(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrbud(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrbud(sig,1:N,32,tftb_window(int(N/10),"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrbud(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrbud(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"),1);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);

[t,T,F]=Ctfrbud(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T,f]=Ctfrbud(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrbud(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrbud(sig,1:N,32,tftb_window(int(N/10),"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrbud(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrbud(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"),1);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);



N=128;
t1=55;  f=0.3;
sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrbud(sig),tfrbud(sig));  
assert_checkalmostequal(Ctfrbud(sig,1:N,N,1,ones(N-1,1)),...
                        tfrbud(sig,1:N,N,1,ones(N-1,1)),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrbud(sig,1:N,N/2,window("re",11),window("re",N/2+1)),...
                        tfrbud(sig,1:N,N/2,window("re",11),window("re",N/2+1)),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrbud(sig,round(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N+1,'rect')),...
                        tfrbud(sig,round(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N+1,'rect')),...
                        rtol,atol);  



// Reality of the TFR
sig=noisecg(N);
tfr=Ctfrbud(sig);
assert_checktrue(isreal(tfr));



N=127; 
sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrbud(sig),tfrbud(sig));  
assert_checkalmostequal(Ctfrbud(sig,1:N,N,1,ones(N,1)),...
                        tfrbud(sig,1:N,N,1,ones(N,1)),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrbud(sig,1:N,round(N/2),window("re",11),window("re",round(N/2)+1)),...
                        tfrbud(sig,1:N,round(N/2),window("re",11),window("re",round(N/2)+1)),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrbud(sig,ceil(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N,'rect')),...
                         tfrbud(sig,ceil(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N,'rect')),...
                        rtol,atol);
