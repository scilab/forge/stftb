mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//       F. Auger, Dec. 1995 - O. Lemoine, March 1996. 
rtol=100*%eps;
atol=1000*%eps;


// First test the optional arguments
N=32;
sig=ones(1,N);
tfr=Ctfrcw(sig);
assert_checkalmostequal(tfr,tfrcw(sig),rtol,atol);
assert_checkequal(size(tfr),[32,32]);
assert_checkequal(Ctfrcw(sig,1:N),tfr);
assert_checkequal(Ctfrcw(sig,1:N,32),tfr);
assert_checkequal(Ctfrcw(sig,1:N,32,tftb_window(int(N/10),"hamming")),tfr);
assert_checkequal(Ctfrcw(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming")),tfr);
assert_checkequal(Ctfrcw(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"),1),tfr);

[t,T]=Ctfrcw(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrcw(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrcw(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrcw(sig,1:N,32,tftb_window(int(N/10),"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrcw(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrcw(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"),1);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);

[t,T,F]=Ctfrcw(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T,f]=Ctfrcw(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrcw(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrcw(sig,1:N,32,tftb_window(int(N/10),"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrcw(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrcw(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"),1);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);

// We test each property of the corresponding TFR :

N=128; 

t1=55; f=0.3;
sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrcw(sig),tfrcw(sig));  
assert_checkalmostequal(Ctfrcw(sig,1:N,N,1,ones(N-1,1)),...
                        tfrcw(sig,1:N,N,1,ones(N-1,1)),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrcw(sig,1:N,N/2,window("re",11),window("re",N/2+1)),...
                        tfrcw(sig,1:N,N/2,window("re",11),window("re",N/2+1)),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrcw(sig,round(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N+1,'rect')),...
                        tfrcw(sig,round(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N+1,'rect')),...
                        rtol,atol);  


N=127; 

// Covariance by translation in time 
sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrcw(sig),tfrcw(sig));  
assert_checkalmostequal(Ctfrcw(sig,1:N,N,1,ones(N,1)),...
                        tfrcw(sig,1:N,N,1,ones(N,1)),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrcw(sig,1:N,round(N/2),window("re",11),window("re",round(N/2)+1)),...
                        tfrcw(sig,1:N,round(N/2),window("re",11),window("re",round(N/2)+1)),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrcw(sig,ceil(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N,'rect')),...
                         tfrcw(sig,ceil(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N,'rect')),...
                        rtol,atol);
