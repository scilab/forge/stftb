mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

//       O. Lemoine - March 1996.
rtol=100*%eps;
atol=1000*%eps;


// First test the optional arguments
N=32;
sig=ones(1,N);
tfr=Ctfrmh(sig);
assert_checkequal(size(tfr),[32,32]);
assert_checkequal(Ctfrmh(sig,1:N),tfr);
assert_checkequal(Ctfrmh(sig,1:N,32),tfr);

[t,T]=Ctfrmh(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrmh(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrmh(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);

[t,T,F]=Ctfrmh(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T,f]=Ctfrmh(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrmh(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);





t1=55; f=0.3;
N=128; 

sig=amgauss(N,t1).*fmconst(N,f,t1); 
Ctfr=Ctfrmh(sig,1:N,N);
tfr=tfrmh(sig,1:N,N);
assert_checkalmostequal(tfr,Ctfr,rtol,atol); 
assert_checkalmostequal(Ctfrmh(sig),tfrmh(sig),rtol,atol);  
assert_checkalmostequal(Ctfrmh(sig,1:N,N),...
                        tfrmh(sig,1:N,N),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrmh(sig,1:N,N/2),...
                        tfrmh(sig,1:N,N/2),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrmh(sig,round(N/2)+2,N),...
                        tfrmh(sig,round(N/2)+2,N),...
                        rtol,atol);  



N=123 

// Covariance by translation in time 
sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrmh(sig),tfrmh(sig),[],1e-10);  
assert_checkalmostequal(Ctfrmh(sig,1:N,N),...
                        tfrmh(sig,1:N,N),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrmh(sig,1:N,round(N/2)),...
                        tfrmh(sig,1:N,round(N/2)),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrmh(sig,ceil(N/2)+2,N),...
                         tfrmh(sig,ceil(N/2)+2,N),...
                        rtol,atol);


