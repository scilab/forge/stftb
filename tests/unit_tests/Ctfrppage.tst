mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function tfrppage
//Unit test for the time frequency representation TFRPPAGE.

//       O. Lemoine - March 1996. 


//Check otionnal arguments

N=32;
sig=ones(1,N);
tfr=Ctfrppage(sig);
assert_checkequal(size(tfr),[32,32]);
assert_checkequal(Ctfrppage(sig,1:N),tfr);
assert_checkequal(Ctfrppage(sig,1:N,32),tfr);
assert_checkequal(Ctfrppage(sig,1:N,32,tftb_window(int(N/4+1),"hamming")),tfr);

[t,T]=Ctfrppage(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrppage(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrppage(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrppage(sig,1:N,32,tftb_window(int(N/4+1),"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);

[t,T,F]=Ctfrppage(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T,f]=Ctfrppage(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrppage(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrppage(sig,1:N,32,tftb_window(int(N/4+1),"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);

// Check against the tfrppage Scilab function
rtol=100*%eps;
atol=1000*%eps;

N=128;
t1=55;  f=0.3;
sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrppage(sig),tfrppage(sig),[],atol);
assert_checkalmostequal(Ctfrppage(sig,1:2,1),tfrppage(sig,1:2,1),[],atol);


assert_checkalmostequal(Ctfrppage(sig,1:N,N,ones(N-1,1)),...
                        tfrppage(sig,1:N,N,ones(N-1,1)),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrppage(sig,1:N,N/2,window("re",N/2+1)),...
                        tfrppage(sig,1:N,N/2,window("re",N/2+1)),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrppage(sig,round(N/2)+2,N,tftb_window(N+1,'rect')),...
                        tfrppage(sig,round(N/2)+2,N,tftb_window(N+1,'rect')),...
                        rtol,atol);  




N=128;

// Covariance by translation in time 
t1=55; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=Ctfrppage(sig1);  
tfr2=Ctfrppage(sig2);
assert_checkalmostequal(tfr1,tfr2(:,modulo((0:N-1)-t1+t2,N)+1),sqrt(%eps),[],"matrix");


N=129;

t1=55;  f=0.3;
sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrppage(sig),tfrppage(sig),[],atol);
assert_checkalmostequal(Ctfrppage(sig,1:2,1),tfrppage(sig,1:2,1),[],atol);


assert_checkalmostequal(Ctfrppage(sig,1:N,N,ones(N,1)),...
                        tfrppage(sig,1:N,N,ones(N,1)),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrppage(sig,1:N,round(N/2),window("re",round(N/2))),...
                        tfrppage(sig,1:N,round(N/2),window("re",round(N/2))),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrppage(sig,round(N/2)+2,N,tftb_window(N,'rect')),...
                        tfrppage(sig,round(N/2)+2,N,tftb_window(N,'rect')),...
                        rtol,atol);  

