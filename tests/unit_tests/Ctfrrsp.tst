mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

//	F. Auger - December 1995, O. Lemoine - April 1996. 


//Check optional arguments

N=32;
sig=ones(1,N);
tfr=Ctfrrsp(sig);
assert_checkequal(size(tfr),[32,32]);
assert_checkequal(Ctfrrsp(sig,1:N),tfr);
assert_checkequal(Ctfrrsp(sig,1:N,32),tfr);
assert_checkequal(Ctfrrsp(sig,1:N,32,tftb_window(int(N/4)+1,"hamming")),tfr);

[tfr1,rtfr]=Ctfrrsp(sig);
assert_checkequal(size(rtfr),[32,32]);
assert_checkequal(tfr1,tfr);
[tfr1,rtfr1]=Ctfrrsp(sig,1:N);
assert_checkequal(tfr1,tfr);assert_checkequal(rtfr1,rtfr)
[tfr1,rtfr1]=Ctfrrsp(sig,1:N,32);
assert_checkequal(tfr1,tfr);assert_checkequal(rtfr1,rtfr)
[tfr1,rtfr1]=Ctfrrsp(sig,1:N,32,tftb_window(int(N/4)+1,"hamming"));
assert_checkequal(tfr1,tfr);assert_checkequal(rtfr1,rtfr)

[tfr1,rtfr1,rhat]=Ctfrrsp(sig);
assert_checkequal(tfr1,tfr);assert_checkequal(rtfr1,rtfr)
[tfr1,rtfr1,rhat1]=Ctfrrsp(sig,1:N);
assert_checkequal(tfr1,tfr);assert_checkequal(rtfr1,rtfr);assert_checkequal(rhat1,rhat)
[tfr1,rtfr1,rhat1]=Ctfrrsp(sig,1:N,32);
assert_checkequal(tfr1,tfr);assert_checkequal(rtfr1,rtfr);assert_checkequal(rhat1,rhat)
[tfr1,rtfr1,rhat1]=Ctfrrsp(sig,1:N,32,tftb_window(int(N/4)+1,"hamming"));
assert_checkequal(tfr1,tfr);assert_checkequal(rtfr1,rtfr);assert_checkequal(rhat1,rhat)

//The Ctfrsp code seems to be wrong
// Check against the tfrrsp Scilab function
rtol=100*%eps;
atol=1000*%eps;
N=64;
t1=45;  f=0.3;
sig=amgauss(N,t1).*fmconst(N,f,t1);
[Ctfr,Crtfr,Crhat]=Ctfrrsp(sig);
[tfr,rtfr,rhat]=tfrrsp(sig);
assert_checkalmostequal(Ctfr,tfr,rtol,atol);
assert_checkalmostequal(Crtfr,rtfr,rtol,atol);
assert_checkalmostequal(Crhat,rhat,rtol,atol);

[Ctfr,Crtfr,Crhat]=Ctfrrsp(sig,1:N,N,ones(1,5));
[tfr,rtfr,rhat]=tfrrsp(sig,1:N,N,ones(1,5));
assert_checkalmostequal(Ctfr,tfr,rtol,atol);
assert_checkalmostequal(Crtfr,rtfr,rtol,atol);
assert_checkalmostequal(Crhat,rhat,rtol,atol);

[Ctfr,Crtfr,Crhat]=Ctfrrsp(sig,1:N,N,ones(1,N-1));
[tfr,rtfr,rhat]=tfrrsp(sig,1:N,N,ones(1,N-1));
assert_checkalmostequal(Ctfr,tfr,rtol,atol);
assert_checkalmostequal(Crtfr,rtfr,rtol,atol);
assert_checkalmostequal(Crhat,rhat,rtol,atol);

[Ctfr,Crtfr,Crhat]=Ctfrrsp(sig,1:N,N,window("re",N/2+1));
[tfr,rtfr,rhat]=tfrrsp(sig,1:N,N,window("re",N/2+1));
assert_checkalmostequal(Ctfr,tfr,rtol,atol);
assert_checkalmostequal(Crtfr,rtfr,rtol,atol);
assert_checkalmostequal(Crhat,rhat,rtol,atol);

f0=10;
sig=fmconst(N+6,f0/N);
[Ctfr,Crtfr,Crhat]=Ctfrrsp(sig,round(N/2)+2,N,tftb_window(N+1,'rect'));
[tfr,rtfr,rhat]=tfrrsp(sig,round(N/2)+2,N,tftb_window(N+1,'rect'));
assert_checkalmostequal(Ctfr,tfr,rtol,atol);
assert_checkalmostequal(Crtfr,rtfr,rtol,atol);
assert_checkalmostequal(Crhat,rhat,rtol,atol);


N=63;

t1=45;  f=0.3;
sig=amgauss(N,t1).*fmconst(N,f,t1);
[Ctfr,Crtfr,Crhat]=Ctfrrsp(sig);
[tfr,rtfr,rhat]=tfrrsp(sig);
assert_checkalmostequal(Ctfr,tfr,rtol,atol);
assert_checkalmostequal(Crtfr,rtfr,rtol,atol);
assert_checkalmostequal(Crhat,rhat,rtol,atol);

[Ctfr,Crtfr,Crhat]=Ctfrrsp(sig,1:N,N,ones(1,5));
[tfr,rtfr,rhat]=tfrrsp(sig,1:N,N,ones(1,5));
assert_checkalmostequal(Ctfr,tfr,rtol,atol);
assert_checkalmostequal(Crtfr,rtfr,rtol,atol);
assert_checkalmostequal(Crhat,rhat,rtol,atol);

[Ctfr,Crtfr,Crhat]=Ctfrrsp(sig,1:N,N,ones(1,N));
[tfr,rtfr,rhat]=tfrrsp(sig,1:N,N,ones(1,N));
assert_checkalmostequal(Ctfr,tfr,rtol,atol);
assert_checkalmostequal(Crtfr,rtfr,rtol,atol);
assert_checkalmostequal(Crhat,rhat,rtol,atol);

[Ctfr,Crtfr,Crhat]=Ctfrrsp(sig,1:N,N,window("re",floor(N/2)));
[tfr,rtfr,rhat]=tfrrsp(sig,1:N,N,window("re",floor(N/2)));
assert_checkalmostequal(Ctfr,tfr,rtol,atol);
assert_checkalmostequal(Crtfr,rtfr,rtol,atol);
assert_checkalmostequal(Crhat,rhat,rtol,atol);

f0=10;
sig=fmconst(N+6,f0/N);
[Ctfr,Crtfr,Crhat]=Ctfrrsp(sig,round(N/2)+2,N,tftb_window(N,'rect'));
[tfr,rtfr,rhat]=tfrrsp(sig,round(N/2)+2,N,tftb_window(N,'rect'));
assert_checkalmostequal(Ctfr,tfr,rtol,atol);
assert_checkalmostequal(Crtfr,rtfr,rtol,atol);
assert_checkalmostequal(Crhat,rhat,rtol,atol);




