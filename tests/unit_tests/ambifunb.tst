mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function ambifunt
//AMBIFUNT Unit test for the function ambifunb.

//	O. Lemoine - December 1995.
//	F. Auger - February 1996.
// H. Nahrstaedt - 2012

N=128; 

// Ambiguity function of a pulse
sig=sum(((1:N)'==40),'c'); 
amb=ambifunb(sig);
[ik,jk]=find(amb~=0.0);
assert_checkfalse(or(jk~=N/2)|or(ik'-(1:N)'));


// Ambiguity function of a sine wave
sig=fmconst(N,0.2);
amb=ambifunb(sig);
[a b]=mtlb_max(amb);
assert_checkfalse(or(b-odd(N/2)*ones(1,N-1)))


// Energy 
sig=noisecg(N);
amb=ambifunb(sig);
assert_checkalmostequal(abs(amb(odd(N/2),N/2)),norm(sig)^2,%eps,sqrt(%eps));


// Link with the Wigner-Ville distribution
sig=fmlin(N);
amb=ambifunb(sig);
amb=amb([(N+modulo(N,2))/2+1:N 1:(N+modulo(N,2))/2],:);
ambi=mtlb_ifft(amb).';
tdr=zeros(N,N); 		// Time-delay representation
tdr(1:N/2,:)=ambi(N/2:N-1,:);
tdr(N:-1:N/2+2,:)=ambi(N/2-1:-1:1,:);
wvd1=real(mtlb_fft(tdr));
wvd2=tfrwv(sig);
errors=max(abs(wvd1-wvd2));

assert_checkalmostequal(errors,0,%eps,sqrt(%eps));

				     

N=111;

// Ambiguity function of a pulse
sig=sum(((1:N)'==40),'c'); 
amb=ambifunb(sig);
[ik,jk]=find(amb~=0.0);
assert_checkfalse(or(jk~=(N+1)/2)|or(ik'-(1:N)'))


// Ambiguity function of a sine wave
sig=fmconst(N,0.2);
amb=ambifunb(sig);
[a b]=mtlb_max(amb(:,2:N-1));
assert_checkfalse(or(b-((N+1)/2)*ones(1,N-2)))


// Energy 
sig=noisecg(N);
amb=ambifunb(sig);
assert_checkalmostequal(abs(amb((N+1)/2,(N+1)/2)),norm(sig)^2,%eps,sqrt(%eps));


// Link with the Wigner-Ville distribution
sig=fmlin(N);
amb=ambifunb(sig);
amb=amb([(N+1)/2:N 1:(N-1)/2],:);
ambi=mtlb_ifft(amb).';
tdr=zeros(N,N); 		// Time-delay representation
tdr(1:ceil((N+1)/2),:)      = ambi((N+1)/2:N,:);
tdr(N:-1:(N+1)/2+1,:) = ambi((N-1)/2:-1:1,:);
wvd1=real(mtlb_fft(tdr));
wvd2=tfrwv(sig,1:N,N);
errors=max(abs(wvd1-wvd2));
assert_checkalmostequal(errors,0,%eps,sqrt(%eps));
