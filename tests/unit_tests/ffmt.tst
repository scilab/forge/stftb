mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function ffmtt
// 	Unit test for the function ffmt.

//	O. Lemoine - May 1996.

N=128; 

// Perfect reconstruction with iffmt
fmin=0.1; fmax=0.5;
sig=real(amgauss(N).*fmconst(N,.3)); 
[MELLIN,BETA]=ffmt(sig,fmin,fmax,N);
X=real(iffmt(MELLIN,BETA,N)); 
assert_checkalmostequal(X,sig,[],1e-7);

// Energy conservation
x=fmconst(N);
fmin=0.1; fmax=0.4;
FMT=ffmt(real(x),fmin,fmax,N);
SP=fft(x); 
indmin = 1+round(fmin*(N-2));
indmax = 1+round(fmax*(N-2));
SPana=SP(indmin:indmax);
nu=(indmin:indmax)'/N; 
SPp=SPana./nu;
assert_checkalmostequal(SPp'*SPana,norm(FMT)^2);

// Unitarity of the MT
x1=amgauss(N).*fmlin(N,.15,.35);
x2=amgauss(N).*fmconst(N);
fmin=0.01; fmax=0.49;
FMT1=ffmt(real(x1),fmin,fmax,2*N);
FMT2=ffmt(real(x2),fmin,fmax,2*N);
indmin = 1+round(fmin*(2*N-2));
indmax = 1+round(fmax*(2*N-2));
SP1=fft(x1); SP2=fft(x2);
nu=(indmin:indmax)'/N/2; 
SP1p=SP1(indmin:indmax)./nu;
cor1=SP1p'*SP2(indmin:indmax);
cor2=conj(FMT1*FMT2');
assert_checkalmostequal(cor1,cor2,N*1e-3);


// Covariance by dilation 
// Property of the Mellin transform used in scale.
// So as scale works, this property is verified.


// MT of a product = convolution of the MT 
x1=amgauss(N).*fmlin(N,.15,.35);
x2=amgauss(N).*fmsin(N,.15,.35);
FMT1=ffmt(real(x1),fmin,fmax,2*N);
FMT2=ffmt(real(x2),fmin,fmax,2*N);
FMT=convol(FMT1,FMT2);
FMT=FMT/max(real(FMT));
X1=fft(x1); X2=fft(x2);
X=X1.*X2; 
x=fftshift(ifft(X)); 
FMTp=ffmt(real(x),fmin,fmax,2*N);
FMTp=FMTp/max(real(FMTp));
assert_checkalmostequal(FMTp,FMT(N+1:3*N),[],1e-4);


N=121; 

// Perfect reconstruction with iffmt
fmin=0.1; fmax=0.5;
sig=real(amgauss(N).*fmconst(N,.3)); 
[MELLIN,BETA]=ffmt(sig,fmin,fmax,N+1);
X=real(iffmt(MELLIN,BETA,N)); 
assert_checkalmostequal(X,sig,[],1e-2);

// Energy conservation
x=fmconst(N);
fmin=0.1; fmax=0.4;
FMT=ffmt(real(x),fmin,fmax,N+1);
SP=fft(hilbert(real(x))); 
indmin = 1+round(fmin*(N-2));
indmax = 1+round(fmax*(N-2));
SPana=SP(indmin:indmax);
nu=(indmin:indmax)'/(N+1); 
SPp=SPana./nu;
assert_checkalmostequal(SPp'*SPana,norm(FMT)^2);

// Unitarity of the MT
x1=amgauss(N).*fmlin(N,.15,.35);
x2=amgauss(N).*fmconst(N);
fmin=0.01; fmax=0.49;
FMT1=ffmt(real(x1),fmin,fmax,2*N);
FMT2=ffmt(real(x2),fmin,fmax,2*N);
indmin = 1+round(fmin*(2*N-2));
indmax = 1+round(fmax*(2*N-2));
SP1=fft(x1); SP2=fft(x2);
nu=(indmin:indmax)'/N/2; 
SP1p=SP1(indmin:indmax)./nu;
cor1=SP1p'*SP2(indmin:indmax);
cor2=conj(FMT1*FMT2');
assert_checkalmostequal(cor1,cor2,N*1e-3);

// MT of a product = convolution of the MT 
x1=amgauss(N).*fmlin(N,.15,.35);
x2=amgauss(N).*fmsin(N,.15,.35);
FMT1=ffmt(real(x1),fmin,fmax,2*N);
FMT2=ffmt(real(x2),fmin,fmax,2*N);
FMT=convol(FMT1,FMT2);
FMT=FMT/max(real(FMT));
X1=fft(x1); X2=fft(x2);
X=X1.*X2; 
x=fftshift(ifft(X)); 
FMTp=ffmt(real(x),fmin,fmax,2*N);
FMTp=FMTp/max(real(FMTp));
assert_checkalmostequal(FMTp,FMT(N+1:3*N),[],1e-4);
