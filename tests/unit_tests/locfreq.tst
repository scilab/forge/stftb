mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function locfreqt
//LOCFREQT Unit test for the function LOCFREQ.

//	O. Lemoine - January 1996.

N=256;

// Test for a complex sinusoid
sig1=fmconst(N);
[fm1,B1]=locfreq(sig1);
assert_checkalmostequal(fm1,0.25);
assert_checkalmostequal(B1,0,[],sqrt(%eps)); 



// Test for a Gaussian window : lower bound of the Heisenber-Gabor
// inequality 
sig3=amgauss(N);
[fm3,B3]=locfreq(sig3);
[tm3,T3]=loctime(sig3); 
assert_checkalmostequal(T3*B3,1);


