mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

//	F. Auger, Dec. 1995 - O. Lemoine, March 1996.


// We test each property of the corresponding TFR :

N=128;

// Covariance by translation in time 
t1=55; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrbj(sig1);  
tfr2=tfrbj(sig2);        
[tr,tc]=size(tfr1);
assert_checkalmostequal(tfr1,tfr2(:,vecmodulo((1:tc)-t1+t2,tc)),1e-10,1e-8);

// Reality of the TFR
sig=noisecg(N);
tfr=tfrbj(sig);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=noisecg(N);
[tfr,t,f]=tfrbj(sig);
Es=norm(sig)^2;
Etfr=sum(mean(tfr,1));
assert_checkalmostequal(Es,Etfr);

// Time-marginal
sig=noisecg(N);
tfr=tfrbj(sig);
ip1=abs(sig).^2;
ip2=mean(tfr,1);
assert_checkalmostequal(ip1,ip2');

// Frequency-marginal
sig=noisecg(N);
tfr=tfrbj(sig,1:N,N,[1],ones(N-1,1));
FFT=fft(sig);
psd1=abs(FFT(2:N/2)).^2/(2*N);
psd2=mean(tfr(1:2:N,:),2);
assert_checkalmostequal(psd1,psd2(2:N/2));


// Conservation of the time support (wide-sense)
sig=[zeros(N/4,1);fmlin(N/2);zeros(N/4,1)];
tfr=tfrbj(sig);
assert_checktrue(or(abs(tfr(:,1:N/4-1))<sqrt(%eps)));
assert_checktrue(or(abs(tfr(:,(3*N/4+1):N))<sqrt(%eps)));

// time localization
t0=30; sig=((1:N)'==t0)+0;
tfr=tfrbj(sig);
[ik,jk]=find(tfr~=0.0);
assert_checkequal(jk,t0*ones(jk));
assert_checkequal(ik,1:N);

// frequency localization
f0=10;
sig=fmconst(N+6,f0/N);
tfr=tfrbj(sig,N/2+2,N,tftb_window(11,'rect'),tftb_window(N+1,'rect'));
assert_checkequal(find(tfr>1/N),2*f0+1);
assert_checkalmostequal(mean(tfr,1),1);

sig=fmconst(N,0.1);
tfr=tfrbj(sig,1:N,N/2,tftb_window(11,'rect'),tftb_window(N/2+1,'rect'));
assert_checkequal(sum(mean(tfr,1)),sum(abs(sig).^2));

N=111;

// Reality of the TFR
sig=noisecg(N);
tfr=tfrbj(sig);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=noisecg(N);
tfr=tfrbj(sig);
Es=norm(sig)^2;
Etfr=sum(mean(tfr,1));
assert_checkalmostequal(Es,Etfr);

// Time-marginal
sig=noisecg(N);
tfr=tfrbj(sig);
ip1=abs(sig).^2;
ip2=mean(tfr,1);
assert_checkalmostequal(ip1,ip2');

// Frequency-marginal
sig=noisecg(N);
tfr=tfrbj(sig,1:N,N,[1],ones(N,1));
FFT=fft(sig);
psd1=abs(FFT(2:round(N/2))).^2/(2*N);
psd2=mean(tfr(1:2:N,:),2);
assert_checkalmostequal(psd1,psd2(2:N/2+1),5e-1);

// Conservation of the time support (wide-sense)
sig=[zeros(round(N/4),1);fmlin(round(N/2));zeros(round(N/4),1)];
tfr=tfrbj(sig);
assert_checktrue(or(abs(tfr(:,1:N/4-1))<sqrt(%eps)));
assert_checktrue(or(abs(tfr(:,(3*N/4+1):N))<sqrt(%eps)));

// time localization
t0=31; sig=((1:N)'==t0)+0;
tfr=tfrbj(sig);
[ik,jk]=find(tfr~=0.0);
assert_checkequal(jk,t0*ones(jk));
assert_checkequal(ik,1:N);

// frequency localization
f0=10;
sig=fmconst(N+6,f0/N);
tfr=tfrbj(sig,round(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N,'rect'));
assert_checkequal(find(tfr>1/N),2*f0+1);
assert_checkalmostequal(mean(tfr,1),1);

sig=fmconst(N,0.1);
tfr=tfrbj(sig,1:N,round(N/2),tftb_window(11,'rect'),tftb_window(round(N/2)+1,'rect'));
assert_checkequal(sum(mean(tfr,1)),sum(abs(sig).^2));
