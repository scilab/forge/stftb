mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

//       O. Lemoine, March 1996.

N=128; 

// Covariance by translation in time 
t1=60; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrri(sig1);  
tfr2=tfrri(sig2);        
[tr,tc]=size(tfr1);
assert_checkalmostequal(tfr1,tfr2(:,vecmodulo((1:tc)-t1+t2,tc)),1e-10,1e-7);

// Energy conservation
sig=noisecg(N);
tfr=tfrri(sig);
Es=norm(sig)^2;
Etfr=sum(mean(tfr,1));
assert_checkalmostequal(Es,real(Etfr));
assert_checktrue(abs(imag(Etfr))<1e-10);

// Time-marginal
sig=noisecg(N);
tfr=tfrri(sig);
ip1=abs(sig).^2;
ip2=mean(tfr,1);
assert_checkalmostequal(ip1,real(ip2'));
assert_checktrue(max(abs(imag(ip2)))<1e-10);

// Frequency-marginal
sig=noisecg(N);
tfr=tfrri(sig);
FFT=fft(sig);
psd1=abs(FFT(2:N/2)).^2/N;
psd2=real(mean(tfr(2:N/2,:),2));
assert_checkalmostequal(psd1,psd2);

// Unitarity
x1=amgauss(N).*fmlin(N);
x2=amexpo2s(N);
tfr1=tfrri(x1);
tfr2=tfrri(x2);
cor1=abs(x1'*x2)^2;
cor2=real(sum(sum(tfr1.*conj(tfr2)))/N);
assert_checkalmostequal(cor1,cor2);

// Conservation of the time support (wide-sense)
sig=[zeros(N/4,1);fmlin(N/2);zeros(N/4,1)];
tfr=tfrri(sig);
assert_checktrue(or(abs(tfr(:,1:N/4-1))<sqrt(%eps)));
assert_checktrue(or(abs(tfr(:,(3*N/4+1):N))<sqrt(%eps)));

// time localization
t0=30; sig= ((1:N)'==t0)+0;
tfr=tfrri(sig);
[ik,jk]=find(tfr~=0.0);
assert_checkequal(jk,t0*ones(jk));
assert_checkequal(ik,1:N);

N=127; 

// Covariance by translation in time 
t1=61; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrri(sig1);  
tfr2=tfrri(sig2);        
[tr,tc]=size(tfr1);
assert_checkalmostequal(tfr1,tfr2(:,vecmodulo((1:tc)-t1+t2,tc)),1e-10,1e-7);

// Energy conservation
sig=noisecg(N);
tfr=tfrri(sig);
Es=norm(sig)^2;
Etfr=sum(mean(tfr,1));
assert_checkalmostequal(Es,real(Etfr));
assert_checktrue(abs(imag(Etfr))<1e-10);

// Time-marginal
sig=noisecg(N);
tfr=tfrri(sig);
ip1=abs(sig).^2;
ip2=mean(tfr,1);
assert_checkalmostequal(ip1,real(ip2)');
assert_checktrue(max(abs(imag(ip2)))<1e-10);
// Frequency-marginal
sig=noisecg(N);
tfr=tfrri(sig);
FFT=fft(sig);
psd1=abs(FFT(2:fix(N/2))).^2/N;
psd2=real(mtlb_mean(tfr(2:fix(N/2),:)'))';
psd2=real(mean(tfr(2:N/2,:),2));
assert_checkalmostequal(psd1,psd2);

// Unitarity
x1=amgauss(N).*fmlin(N);
x2=amexpo2s(N);
tfr1=tfrri(x1);
tfr2=tfrri(x2);
cor1=abs(x1'*x2)^2;
cor2=real(sum(sum(tfr1.*conj(tfr2)))/N);
assert_checkalmostequal(cor1,cor2);

// Conservation of the time support (wide-sense)
sig=[zeros(N/4,1);fmlin(round(N/2));zeros(N/4,1)];
tfr=tfrri(sig);
assert_checktrue(or(abs(tfr(:,1:N/4-1))<sqrt(%eps)));
assert_checktrue(or(abs(tfr(:,(3*N/4+1):N))<sqrt(%eps)));

// time localization
t0=30; sig= ((1:N)'==t0)+0;
tfr=tfrri(sig);
[ik,jk]=find(tfr~=0.0);
assert_checkequal(jk,t0*ones(jk));
assert_checkequal(ik,1:N);

