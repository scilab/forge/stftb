mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function tfrrspt
//TFRRSPT Unit test for the function TFRRSP.

//	F. Auger - December 1995, O. Lemoine - April 1996. 


N=128;

// Reality of the TFR
sig=noisecg(N);
[tfr,rtfr]=tfrrsp(sig);
assert_checktrue(isreal(rtfr));

// Energy conservation
sig=fmlin(N);
[tfr,rtfr]=tfrrsp(sig);
Es=norm(sig)^2;
Etfr=sum(mean(rtfr,1));
assert_checkalmostequal(Es,Etfr); 


// Positivity
assert_checkequal(find(rtfr<0),[]);

// time localization
t0=30; sig=((1:N)'==t0)+0;
[tfr,rtfr]=tfrrsp(sig);
[ik,jk]=find(abs(rtfr)>sqrt(%eps));
assert_checktrue(and(jk==t0));
assert_checkequal(ik,1:N);

// frequency localization
f0=30;
sig=fmconst(N+6,f0/N);
[tfr rtfr]=tfrrsp(sig,N/2+2,N,tftb_window(N+1,'rect'));
assert_checkfalse(or(find(rtfr>max(rtfr)/N)~=f0+1));
assert_checkalmostequal(mean(rtfr),1,1e-2);


N=123;

// Reality of the TFR
sig=noisecg(N);
[tfr,rtfr]=tfrrsp(sig);
assert_checktrue(isreal(rtfr));

// Energy conservation
sig=fmlin(N);
[tfr,rtfr]=tfrrsp(sig);
Es=norm(sig)^2;
Etfr=sum(mean(rtfr,1));
assert_checkalmostequal(Es,Etfr); 

// Positivity
assert_checkequal(find(rtfr<0),[]);

// time localization
t0=30; sig=((1:N)'==t0)+0;
[tfr,rtfr]=tfrrsp(sig);
[ik,jk]=find(abs(rtfr)>sqrt(%eps));
assert_checktrue(and(jk==t0));
assert_checkequal(ik,1:N);

// frequency localization
f0=30;
sig=fmconst(N+6,f0/N);
[tfr rtfr]=tfrrsp(sig,round(N/2)+2,N,tftb_window(N,'rect'));
assert_checkfalse(or(find(rtfr>max(rtfr)/N)~=f0+1));
assert_checkalmostequal(mean(rtfr),1,1e-2);


